#!/bin/bash 

# Description =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-= #
# This script finds out on which OS type you want to
# use FATE-HD, and compile the FATE-HD sources with
# the adapted makefile accordingly.

# Run the script =-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- #
# To run this script, make sure you have the right
# of execute this file :
#	chmod +x FateHD_compilation.sh
# The command to type in your shell is smth like :
#	./FateHD_compilation.sh

set -e

if [[ "$OSTYPE" == "linux-gnu" ]]; then
	echo "linux-gnu system"
	make -f MAKEFILES/MakefileLinux
#	make -f MAKEFILES/MakefileLinux_GTK

elif [[ "$OSTYPE" == "darwin"* ]]; then
	echo "darwin system"
	make -f MAKEFILES/MakefileMac
	make -f MAKEFILES/MakefileMac_GTK

elif [[ "$OSTYPE" == "cygwin" ]]; then
	echo "windows system"
	mingw32-make -f MAKEFILES/MakefileWindows
	mingw32-make -f MAKEFILES/MakefileWindows_GTK

elif [[ "$OSTYPE" == "msys" ]]; then
	echo "windows system"
	mingw32-make -f MAKEFILES/MakefileWindows
	mingw32-make -f MAKEFILES/MakefileWindows_GTK

fi




