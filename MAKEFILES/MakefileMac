CC            = gcc
CXX           = g++
DEFINES       = -DUNICODE -DQT_LARGEFILE_SUPPORT -DQT_DLL -DQT_NO_DEBUG -DQT_CORE_LIB -DQT_THREAD_SUPPORT
CFLAGS        = -O2 -Wall $(DEFINES)
CXXFLAGS      = -O2 -frtti -fexceptions -Wall -fopenmp -std=c++11 $(DEFINES)

$(info Version CXX used is $(CXX))
$(info )

#######################################################################################
### Define directories path
#######################################################################################

CURRENT_DIR= $(shell pwd)
MAKEFILE_PATH= $(abspath $(lastword $(MAKEFILE_LIST)))
MAKEFILE_DIR= $(patsubst %/,%,$(dir $(MAKEFILE_PATH)))
DEVEL_DIR= $(patsubst %/,%,$(dir $(MAKEFILE_DIR)))
SOURCES_DIR= $(DEVEL_DIR)/SOURCES/_FATE/
SUPPMAT_DIR= $(DEVEL_DIR)/SUPP_MAT/

$(info CURRENT_DIR is $(CURRENT_DIR))
$(info MAKEFILE_PATH is $(MAKEFILE_PATH))
$(info MAKEFILE_DIR is $(MAKEFILE_DIR))
$(info SOURCES_DIR is $(SOURCES_DIR))
$(info SUPPMAT_DIR is $(SUPPMAT_DIR))
$(info )

GDAL_VERSION= $(shell gdalinfo --version | cut -c 6-9)

#######################################################################################
### Define file name
#######################################################################################

VERSION= $(shell grep "Version_FATE" $(DEVEL_DIR)/DESCRIPTION | cut -c 16-)
EXEC= $(DEVEL_DIR)/BINARIES/FATEHDD_$(VERSION)_MAC

$(info File that will be created is $(EXEC))
$(info )

#######################################################################################
### Define INCLUDE directories
#######################################################################################

BOOST_DIR= $(SUPPMAT_DIR)BOOST_include_unix/
GDAL_DIR= /Library/Frameworks/GDAL.framework/Versions/$(GDAL_VERSION)/Unix/include/
GDAL_DIR= $(SUPPMAT_DIR)GDAL_include_unix/
INCLUDEDIRS= -I$(BOOST_DIR) -I$(GDAL_DIR)

$(info INCLUDE_DIR : $(INCLUDEDIRS))
$(info )

#######################################################################################
### Define LIBRARY directories
#######################################################################################

BOOST_LIB= $(SUPPMAT_DIR)BOOST_lib_unix/
LIB_DIR= -L/Library/Frameworks/GDAL.framework/Versions/$(GDAL_VERSION)/Unix/lib/
LIBS= -lboost_serialization -lboost_filesystem -lboost_system -lgomp -lgdal
LDFLAGS_new= -L$(BOOST_LIB) $(LIB_DIR) $(LIBS)

$(info LIB_DIR : $(LDFLAGS_new))
$(info )

#######################################################################################
### MAKE FILES
#######################################################################################




all: $(EXEC) clean

$(EXEC):  main_CROSSPLATFORM.o Cohort.o Community.o Disp.o FG.o FGresponse.o FGUtils.o FilesOfParamsList.o FuncGroup.o GlobalSimulParameters.o Legion.o LightResources.o  Params.o PropPool.o SimulMap.o SuFate.o SuFateH.o
	$(CXX) -o $@ $^ $(LDFLAGS_new) $(INCLUDEDIRS)

main_CROSSPLATFORM.o: $(SOURCES_DIR)main_CROSSPLATFORM.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Cohort.o: $(SOURCES_DIR)Cohort.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS) 

Community.o: $(SOURCES_DIR)Community.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Disp.o: $(SOURCES_DIR)Disp.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FG.o: $(SOURCES_DIR)FG.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FGresponse.o: $(SOURCES_DIR)FGresponse.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FGUtils.o: $(SOURCES_DIR)FGUtils.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FilesOfParamsList.o: $(SOURCES_DIR)FilesOfParamsList.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

FuncGroup.o: $(SOURCES_DIR)FuncGroup.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

GlobalSimulParameters.o: $(SOURCES_DIR)GlobalSimulParameters.cpp
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Legion.o: $(SOURCES_DIR)Legion.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

LightResources.o: $(SOURCES_DIR)LightResources.cpp  
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

Params.o: $(SOURCES_DIR)Params.cpp  
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS) 

PropPool.o: $(SOURCES_DIR)PropPool.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

SimulMap.o: $(SOURCES_DIR)SimulMap.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

SuFate.o: $(SOURCES_DIR)SuFate.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

SuFateH.o: $(SOURCES_DIR)SuFateH.cpp 
	$(CXX) -o $@ -c $< $(CXXFLAGS) $(INCLUDEDIRS)

%:

.PHONY: clean mrproper

clean:
	rm -rf *.o

mrproper: clean
	rm -rf $(EXEC)
