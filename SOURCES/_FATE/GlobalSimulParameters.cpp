#include <cmath>
#include <iostream>
#include <cmath>
#include <cstring>
#include <fstream>
#include <sstream>
#include <cstdio>
#include <vector>
#include <numeric>
#include <sys/stat.h>
#include <sys/types.h>
#include <cstdlib>
#include <ctime>
#include <omp.h>

#include "GlobalSimulParameters.h"

using namespace std;

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Constructors                                                                                    */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GSP::GSP() : m_NbCpus(1), m_NbFG(0), m_NbStrata(0), m_SimulDuration(0),
m_SeedingDuration(0), m_SeedingTimeStep(0), m_SeedingInput(100),
m_MaxByCohort(7000000), m_MaxAbundLow(3000000), m_MaxAbundMedium(7000000), m_MaxAbundHigh(10000000),
m_DoLightCompetition(false), m_LightThreshLow(9000000), m_LightThreshMedium(6000000),
m_DoHabSuitability(false), m_HabSuitOption(1),
m_DoDispersal(false), m_ModeDispersed(1),
m_DoDisturbances(false), m_NbDisturbances(0), m_NbDistSub(0), m_FreqDisturbances(0,0),
m_DoSoilCompetition(false), 
m_DoFireDisturbances(false), m_NbFireDisturbances(0), m_NbFireSub(0), m_FreqFires(0,0), m_InitOption(1),
m_NeighOption(1), m_PropOption(1), m_QuotaOption(1), m_NbFires(0,0), m_PrevData(0),
m_CCextent(0,0), m_FireProb(0,0), m_FlammMax(0), m_LogisInit(0,0), m_LogisSpread(0,0), m_NbClimVar(0),
m_DoDroughtDisturbances(false), m_NbDroughtSub(0), m_ChronoPost("prev"), m_ChronoCurr("post"),
m_DoHabStabilityCheck(false), m_NbHabitats(0),
m_DoAliensDisturbance(false), m_FreqAliens(0,0)
{
	/* Nothing to do */
}

GSP::GSP(const int& nbCpus, const int& nbFG, const int& nbStrata, const int& simulDuration,
const int& seedingDuration, const int& seedingTimeStep, const int& seedingInput,
const int& maxByCohort, const int& maxAbundLow, const int& maxAbundMedium, const int& maxAbundHigh,
const bool& doLightCompetition, const int& lightThreshLow, const int& lightThreshMedium,
const bool& doHabSuitability, const int& habSuitOption,
const bool& doDispersal, const int& modeDispersed,
const bool& doDisturbances, const int& nbDisturbances, const int& nbDistSub, const vector<int>& freqDisturbances,
const bool& doSoilCompetition,
const bool& doFireDisturbances, const int& nbFireDisturbances, const int& nbFireSub, const vector<int>& freqFires,
const int& initOption, const int& neighOption, const int& propOption, const int& quotaOption,
const vector<int>& nbFires, const vector<int>& prevData, const vector<int>& ccExtent, const vector<double>& fireProb,
const int& flammMax, const vector<double>& logisInit, const vector<double>& logisSpread, const int& nbClimVar,
const bool& doDroughtDisturbances, const int& nbDroughtSub, const string& chronoPost, const string& chronoCurr,
const bool& doHabStabilityCheck, const int& nbHabitats,
const bool& doAliensDisturbance, const vector<int>& freqAliens) : m_NbFG(nbFG), m_NbStrata(nbStrata),
m_SimulDuration(simulDuration),
m_SeedingDuration(seedingDuration), m_SeedingTimeStep(seedingTimeStep), m_MaxByCohort(maxByCohort),
m_MaxAbundLow(maxAbundLow), m_MaxAbundMedium(maxAbundMedium), m_MaxAbundHigh(maxAbundHigh),
m_DoLightCompetition(doLightCompetition), m_LightThreshLow(lightThreshLow), m_LightThreshMedium(lightThreshMedium),
m_DoHabSuitability(doHabSuitability), m_HabSuitOption(habSuitOption),
m_DoDispersal(doDispersal), m_ModeDispersed(modeDispersed),
m_DoDisturbances(doDisturbances), m_NbDisturbances(nbDisturbances), m_NbDistSub(nbDistSub), m_FreqDisturbances(freqDisturbances),
m_DoSoilCompetition(doSoilCompetition),
m_DoFireDisturbances(doFireDisturbances), m_NbFireDisturbances(nbFireDisturbances), m_NbFireSub(nbFireSub), m_FreqFires(freqFires),
m_InitOption(initOption), m_NeighOption(neighOption), m_PropOption(propOption), m_QuotaOption(quotaOption), m_NbFires(nbFires), m_PrevData(prevData),
m_CCextent(ccExtent), m_FireProb(fireProb), m_FlammMax(flammMax), m_LogisInit(logisInit), m_LogisSpread(logisSpread), m_NbClimVar(nbClimVar),
m_DoDroughtDisturbances(doDroughtDisturbances), m_NbDroughtSub(nbDroughtSub), m_ChronoPost(chronoPost), m_ChronoCurr(chronoCurr),
m_DoHabStabilityCheck(doHabStabilityCheck), m_NbHabitats(nbHabitats),
m_DoAliensDisturbance(doAliensDisturbance), m_FreqAliens(freqAliens)
{
	/* check if the number of cores required is compatible with computer arch */
	if (nbCpus > 1)
	{
		/* get OMP_NUM_THREADS environment variable :
		   maximum number of threads that can be used to form a new team
		   if a parallel region without a num_threads clause is encountered */
		int threads_number = omp_get_max_threads();
		if (nbCpus <= threads_number)
		{
			m_NbCpus = nbCpus;
		} else
		{
			m_NbCpus = threads_number;
			cout << "\nInitially required number of cores is too high. It was automatically set-up to " << m_NbCpus << endl;
		}
	} else
	{
		m_NbCpus = 1;
	}
}

GSP::GSP(const string globalParamsFile)
{
	/* 1. check parameter file existence */
	testFileExist("--GLOBAL_PARAMS--", globalParamsFile, false);
	
	/* 2. read global parameter file */
	par::Params GlobParms(globalParamsFile.c_str(), " = \"", "#");
	
	/* 3. fill global simulation attributes given parameters */
	
	/* GET OPTIONAL number of cores */
	vector<int> v_int = GlobParms.get_val<int>("NB_CPUS", true);
	if (v_int.size()) m_NbCpus = v_int[0]; else m_NbCpus = 1;
	
	/* check if the number of cores required is compatible with computer arch */
	if (m_NbCpus > 1)
	{
		/* get OMP_NUM_THREADS environment variable :
		   maximum number of threads that can be used to form a new team
		   if a parallel region without a num_threads clause is encountered */
		int threads_number = omp_get_max_threads();
		if (m_NbCpus > threads_number)
		{
			m_NbCpus = threads_number;
			cout << "\nInitially required number of cores is too high. It was automatically set-up to " << m_NbCpus << endl;
		}
	} else
	{
		m_NbCpus = 1;
	}
	
	/* GET BASIC REQUIRED parameters*/
	m_NbFG = GlobParms.get_val<int>("NB_FG", false, "!!! Parameter NB_FG : must be equal to the number of Plant Functional Groups!")[0];
	if (m_NbFG <= 0)
	{
		cerr << "!!! Parameter NB_FG : must be superior to 0!" << endl;
		terminate();
	}
	m_NbStrata = GlobParms.get_val<int>("NB_STRATUM", false, "!!! Parameter NB_STRATUM : must be equal to the number of height strata!")[0];
	if (m_NbStrata <= 0)
	{
		cerr << "!!! Parameter NB_STRATUM : must be superior to 0!" << endl;
		terminate();
	}
	m_SimulDuration = GlobParms.get_val<int>("SIMULATION_DURATION", false, "!!! Parameter SIMULATION_DURATION : must be superior to 0!")[0];
	if (m_SimulDuration <= 0)
	{
		cerr << "!!! Parameter SIMULATION_DURATION : must be superior to 0!" << endl;
		terminate();
	}
	m_MaxByCohort = GlobParms.get_val<int>("MAX_BY_COHORT")[0];
	m_MaxAbundLow = GlobParms.get_val<int>("MAX_ABUND_LOW")[0];
	m_MaxAbundMedium = GlobParms.get_val<int>("MAX_ABUND_MEDIUM")[0];
	m_MaxAbundHigh = GlobParms.get_val<int>("MAX_ABUND_HIGH")[0];
	if (m_MaxAbundLow > m_MaxAbundMedium)
	{
		cerr << "!!! Parameter MAX_ABUND_LOW : must be inferior to MAX_ABUND_MEDIUM!" << endl;
		terminate();
	}
	if (m_MaxAbundMedium > m_MaxAbundHigh)
	{
		cerr << "!!! Parameter MAX_ABUND_MEDIUM : must be inferior to MAX_ABUND_HIGH!" << endl;
		terminate();
	}
	
	/* GET OPTIONAL parameters : seeding */
	v_int = GlobParms.get_val<int>("SEEDING_DURATION", true);
	if (v_int.size()) m_SeedingDuration = v_int[0]; else m_SeedingDuration = 0;
	v_int = GlobParms.get_val<int>("SEEDING_TIMESTEP", true);
	if (v_int.size()) m_SeedingTimeStep = v_int[0]; else m_SeedingTimeStep = 0;
	if (m_SeedingDuration > 0 && m_SeedingTimeStep <= 0)
	{
		cerr << "!!! Parameter SEEDING_TIMESTEP : must be superior to 0!" << endl;
		terminate();
	}
	v_int = GlobParms.get_val<int>("SEEDING_INPUT", true);
	if (v_int.size()) m_SeedingInput = v_int[0]; else m_SeedingInput = 100;
	if (m_SeedingDuration > 0 && m_SeedingInput <= 0)
	{
		cerr << "!!! Parameter SEEDING_INPUT : must be superior to 0!" << endl;
		terminate();
	}

	/* GET OPTIONAL parameters : light competition */
	v_int = GlobParms.get_val<int>("DO_LIGHT_COMPETITION", true);
	if (v_int.size()) m_DoLightCompetition = bool(v_int[0]); else m_DoLightCompetition = false;
	if (m_DoLightCompetition)
	{
		m_LightThreshLow = GlobParms.get_val<int>("LIGHT_THRESH_LOW")[0];
		m_LightThreshMedium = GlobParms.get_val<int>("LIGHT_THRESH_MEDIUM")[0];
		if (m_LightThreshLow < m_LightThreshMedium)
		{
			cerr << "!!! Parameter LIGHT_THRESH_LOW : must be superior to LIGHT_THRESH_MEDIUM!" << endl;
			terminate();
		}
	} else
	{
		m_LightThreshLow = 0;
		m_LightThreshMedium = 0;
	}
	
	/* GET OPTIONAL parameters : habitat suitability */
	v_int = GlobParms.get_val<int>("DO_HAB_SUITABILITY", true);
	if (v_int.size()) m_DoHabSuitability = bool(v_int[0]); else m_DoHabSuitability= false;
	if (m_DoHabSuitability)
	{
		m_HabSuitOption = GlobParms.get_val<int>("HABSUIT_OPTION")[0];
		if (m_HabSuitOption != 1 && m_HabSuitOption != 2)
		{
			cerr << "!!! Parameter HABSUIT_OPTION : must be either equal to 1 (one random number per pixel) or 2 (one distribution per PFG)!" << endl;
			terminate();
		}
	} else
	{
		m_HabSuitOption = 1;
	}
	
	/* GET OPTIONAL parameters : dispersal */
	v_int = GlobParms.get_val<int>("DO_DISPERSAL", true);
	if (v_int.size()) m_DoDispersal = bool(v_int[0]); else m_DoDispersal= false;
	v_int = GlobParms.get_val<int>("DISPERSAL_MODE", true);
	if (v_int.size()) m_ModeDispersed = v_int[0]; else m_ModeDispersed = 1;
	if (m_ModeDispersed != 1 && m_ModeDispersed != 2 && m_ModeDispersed != 3)
	{
		cerr << "!!! DISPERSAL MODE should be either 1 (uniform), 2 (exponential kernel) or 3 (exponential kernel + probability). Please check!" << endl;
		terminate();
	}
	
	/* GET OPTIONAL parameters : disturbances */
	v_int = GlobParms.get_val<int>("DO_DISTURBANCES", true);
	if (v_int.size()) m_DoDisturbances = bool(v_int[0]); else m_DoDisturbances= false;
	if (m_DoDisturbances)
	{
		m_NbDisturbances = GlobParms.get_val<int>("NB_DISTURBANCES")[0];
		m_NbDistSub = GlobParms.get_val<int>("NB_SUBDISTURBANCES")[0];
		if (m_NbDisturbances <= 0 || m_NbDistSub <= 0)
		{
			cerr << "!!! Parameter NB_DISTURBANCES and NB_SUBDISTURBANCES : must be superior to 0!" << endl;
			terminate();
		}
		
		m_FreqDisturbances = GlobParms.get_val<int>("FREQ_DISTURBANCES");
		if (m_NbDisturbances != m_FreqDisturbances.size())
		{
			cerr << "!!! Parameter FREQ_DISTURBANCES : number of frequencies must be equal to the number of disturbances (NB_DISTURBANCES)!" << endl;
			terminate();
		}
	} else
	{
		m_NbDisturbances = 0;
		m_NbDistSub = 0;
		m_FreqDisturbances = vector<int>(1,0);
	}
	
	/* GET OPTIONAL parameters : soil competition */
	v_int = GlobParms.get_val<int>("DO_SOIL_COMPETITION", true);
	if (v_int.size()) m_DoSoilCompetition = bool(v_int[0]); else m_DoSoilCompetition = false;
	
	/* GET OPTIONAL parameters : habitat stability check */
	v_int = GlobParms.get_val<int>("DO_HAB_STABILITY", true);
	if (v_int.size()) m_DoHabStabilityCheck = bool(v_int[0]); else m_DoHabStabilityCheck = false;
	if (m_DoHabStabilityCheck)
	{
		m_NbHabitats = GlobParms.get_val<int>("NB_HABITATS")[0];
		if (m_NbHabitats <= 0)
		{
			cerr << "!!! Parameter NB_HABITATS : must be superior to 0!" << endl;
			terminate();
		}
	} else
	{
		m_NbHabitats = 0;
	}
	
	/* GET OPTIONAL parameters : fire disturbances */
	v_int = GlobParms.get_val<int>("DO_FIRE_DISTURBANCES", true);
	if (v_int.size()) m_DoFireDisturbances = bool(v_int[0]); else m_DoFireDisturbances= false;
	if (m_DoFireDisturbances)
	{
		v_int = GlobParms.get_val<int>("NB_FIRE_DISTURBANCES",true);
		if (v_int.size()) m_NbFireDisturbances = v_int[0]; else m_NbFireDisturbances = 0;
		v_int = GlobParms.get_val<int>("NB_SUBFIRES",true);
		if (v_int.size()) m_NbFireSub = v_int[0]; else m_NbFireSub = 0;
		v_int = GlobParms.get_val<int>("FREQ_FIRES",true);
		if (v_int.size()) m_FreqFires = v_int; else m_FreqFires = vector<int>(1,0);
		if (m_NbFireDisturbances != m_FreqFires.size())
		{
			cerr << "!!! Parameter FREQ_FIRES : number of frequencies must be equal to the number of fire disturbances (NB_FIRE_DISTURBANCES)!" << endl;
			terminate();
		}
		v_int = GlobParms.get_val<int>("INIT_OPTION", true);
		if (v_int.size()) m_InitOption = v_int[0]; else m_InitOption = 0;
		v_int = GlobParms.get_val<int>("NEIGH_OPTION", true);
		if (v_int.size()) m_NeighOption = v_int[0]; else m_NeighOption = 0;
		v_int = GlobParms.get_val<int>("PROP_OPTION", true);
		if (v_int.size()) m_PropOption = v_int[0]; else m_PropOption = 0;
		v_int = GlobParms.get_val<int>("QUOTA_OPTION", true);
		if (v_int.size()) m_QuotaOption = v_int[0]; else m_QuotaOption = 0;
		if ((m_NeighOption == 2 || m_NeighOption == 3) && (m_InitOption == 4 || m_InitOption == 5))
		{
			cerr << "!!! FIRE options : Cookie-cutter module : ignition option is wrong!" << endl;
			terminate();
		}
		if (m_InitOption == 5 && m_PropOption != 4)
		{
			cerr << "!!! FIRE options : Chao Li module : propagation option is wrong!" << endl;
			terminate();
		}
		if (m_QuotaOption == 2 && m_PropOption != 2)
		{
			cerr << "!!! FIRE options : probability module (based on current cell) : propagation option is wrong!" << endl;
			terminate();
		}
		v_int = GlobParms.get_val<int>("NB_FIRES",true);
		if (v_int.size()) m_NbFires = v_int; else m_NbFires = vector<int>(1,0);
		v_int = GlobParms.get_val<int>("PREV_DATA",true);
		if (v_int.size()) m_PrevData = v_int; else m_PrevData = vector<int>(1,0);
		v_int = GlobParms.get_val<int>("COOKIE_CUTTER_EXTENT",true);
		if (v_int.size()) m_CCextent = v_int; else m_CCextent = vector<int>(1,0);
		if ((m_NeighOption == 2 || m_NeighOption == 3) && m_CCextent.size() == 0)
		{
			cerr << "!!! FIRE options : Cookie-cutter module : must give a Cookie-Cutter extent!" << endl;
			terminate();
		}
		vector<double> v_double = GlobParms.get_val<double>("FIRE_PROB",true);
		if (v_double.size()) m_FireProb = v_double; else m_FireProb = vector<double>(1.0,0);
		v_int = GlobParms.get_val<int>("FLAMM_MAX", true);
		if (v_int.size()) m_FlammMax = v_int[0]; else m_FlammMax = 0;
		v_double = GlobParms.get_val<double>("LOGIS_INIT",true);
		if (v_int.size()) m_LogisInit = v_double; else m_LogisInit = vector<double>(1.0,0);
		v_double = GlobParms.get_val<double>("LOGIS_SPREAD",true);
		if (v_int.size()) m_LogisSpread = v_double; else m_LogisSpread = vector<double>(1.0,0);
		v_int = GlobParms.get_val<int>("NB_CLIM_VARIABLES", true);
		if (v_int.size()) m_NbClimVar = v_int[0]; else m_NbClimVar = 0;
	} else
	{
		m_NbFireDisturbances = 0;
		m_NbFireSub = 0;
		m_FreqFires = vector<int>(1,0);
		m_InitOption = 0;
		m_NeighOption = 0;
		m_PropOption = 0;
		m_QuotaOption = 0;
		m_NbFires = vector<int>(1,0);
		m_PrevData = vector<int>(1,0);
		m_CCextent = vector<int>(1,0);
		m_FireProb = vector<double>(1.0,0);
		m_FlammMax = 0;
		m_LogisInit = vector<double>(1.0,0);
		m_LogisSpread = vector<double>(1.0,0);
		m_NbClimVar = 0;
	}
	
	/* GET OPTIONAL parameters : drought disturbances */
	v_int = GlobParms.get_val<int>("DO_DROUGHT_DISTURBANCES", true);
	if (v_int.size()) m_DoDroughtDisturbances = bool(v_int[0]); else m_DoDroughtDisturbances= false;
	if (m_DoDroughtDisturbances)
	{
		m_NbDroughtSub = GlobParms.get_val<int>("NB_SUBDROUGHT")[0];
		v_int = GlobParms.get_val<int>("CHRONO_POST_DROUGHT",true);
		if (v_int.size())
		{
			if(v_int[0]==0) m_ChronoPost = "prev"; else m_ChronoPost = "post";
		} else
		{
			m_ChronoPost = "prev";
		}
		v_int = GlobParms.get_val<int>("CHRONO_CURR_DROUGHT",true);
		if (v_int.size())
		{
			if (v_int[0]==0) m_ChronoCurr = "prev"; else m_ChronoCurr = "post";
		} else
		{
			m_ChronoCurr = "post";
		}
	} else
	{
		m_NbDroughtSub = 0;
		m_ChronoPost = "prev";
		m_ChronoCurr = "post";
	}
	
	/* GET OPTIONAL parameters : aliens introduction */
	v_int = GlobParms.get_val<int>("DO_ALIENS_DISTURBANCE", true);
	if (v_int.size()) m_DoAliensDisturbance = bool(v_int[0]); else m_DoAliensDisturbance= false;
	if (m_DoAliensDisturbance)
	{
		m_FreqAliens = GlobParms.get_val<int>("FREQ_ALIENS");
		if (m_NbFG != m_FreqAliens.size())
		{
			cerr << "!!! Parameter FREQ_ALIENS : number of frequencies must be equal to the number of PFG (NB_FG)!" << endl;
			terminate();
		}
	} else
	{
		m_FreqAliens = vector<int>(1,0);
	}
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Destructor                                                                                      */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

GSP::~GSP()
{
	/* Nothing to do */
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Getters & Setters                                                                               */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

const int& GSP::getNbCpus() const{ return m_NbCpus; }
const int& GSP::getNbFG() const{ return m_NbFG; }
const int& GSP::getNbStrata() const{ return m_NbStrata; }
const int& GSP::getSimulDuration() const{ return m_SimulDuration; }
const int& GSP::getSeedingDuration() const{ return m_SeedingDuration; }
const int& GSP::getSeedingTimeStep() const{ return m_SeedingTimeStep; }
const int& GSP::getSeedingInput() const{ return m_SeedingInput; }
const int& GSP::getMaxByCohort() const{ return m_MaxByCohort; }
const int& GSP::getMaxAbundLow() const{ return m_MaxAbundLow; }
const int& GSP::getMaxAbundMedium() const{ return m_MaxAbundMedium; }
const int& GSP::getMaxAbundHigh() const{ return m_MaxAbundHigh; }
const bool& GSP::getDoLightCompetition() const{ return m_DoLightCompetition; }
const int& GSP::getLightThreshLow() const{ return m_LightThreshLow; }
const int& GSP::getLightThreshMedium() const{ return m_LightThreshMedium; }
const bool& GSP::getDoHabSuitability() const{ return m_DoHabSuitability; }
const int& GSP::getHabSuitOption() const{ return m_HabSuitOption; }
const bool& GSP::getDoDispersal() const{ return m_DoDispersal; }
const int& GSP::getModeDispersed() const{ return m_ModeDispersed; }
const bool& GSP::getDoDisturbances() const{ return m_DoDisturbances; }
const int& GSP::getNbDisturbances() const{ return m_NbDisturbances; }
const int& GSP::getNbDistSub() const{ return m_NbDistSub; }
const vector<int>& GSP::getFreqDisturbances() const{ return m_FreqDisturbances; }
const bool& GSP::getDoSoilCompetition() const{ return m_DoSoilCompetition; }
const bool& GSP::getDoFireDisturbances() const{ return m_DoFireDisturbances; }
const int& GSP::getNbFireDisturbances() const{ return m_NbFireDisturbances; }
const int& GSP::getNbFireSub() const{ return m_NbFireSub; }
const vector<int>& GSP::getFreqFires() const{ return m_FreqFires; }
const int& GSP::getInitOption() const{ return m_InitOption; }
const int& GSP::getNeighOption() const{ return m_NeighOption; }
const int& GSP::getPropOption() const{ return m_PropOption; }
const int& GSP::getQuotaOption() const{ return m_QuotaOption; }
const vector<int>& GSP::getNbFires() const{ return m_NbFires; }
const vector<int>& GSP::getPrevData() const{ return m_PrevData; }
const vector<int>& GSP::getCCextent() const{ return m_CCextent; }
const vector<double>& GSP::getFireProb() const{ return m_FireProb; }
const int& GSP::getFlammMax() const{ return m_FlammMax; }
const vector<double>& GSP::getLogisInit() const{ return m_LogisInit; }
const vector<double>& GSP::getLogisSpread() const{ return m_LogisSpread; }
const int& GSP::getNbClimVar() const{ return m_NbClimVar; }
const bool& GSP::getDoDroughtDisturbances() const{ return m_DoDroughtDisturbances; }
const int& GSP::getNbDroughtSub() const{ return m_NbDroughtSub; }
const string& GSP::getChronoPost() const{ return m_ChronoPost; }
const string& GSP::getChronoCurr() const{ return m_ChronoCurr; }
const bool& GSP::getDoHabStabilityCheck() const{ return m_DoHabStabilityCheck; }
const int& GSP::getNbHabitats() const{ return m_NbHabitats; }
const bool& GSP::getDoAliensDisturbance() const{ return m_DoAliensDisturbance; }
const vector<int>& GSP::getFreqAliens() const{ return m_FreqAliens; }

void GSP::setNbCpus(const int& nbCpus){ m_NbCpus = nbCpus; }
void GSP::setNbFG(const int& nbFG){ m_NbFG = nbFG; }
void GSP::setNbStrata(const int& nbStrata){ m_NbStrata = nbStrata; }
void GSP::setSimulDuration(const int& simulDuration){ m_SimulDuration = simulDuration; }
void GSP::setSeedingDuration(const int& seedingDuration){ m_SeedingDuration = seedingDuration; }
void GSP::setSeedingTimeStep(const int& seedingTimeStep){ m_SeedingTimeStep = seedingTimeStep; }
void GSP::setSeedingInput(const int& seedingInput){ m_SeedingInput = seedingInput; }
void GSP::setMaxByCohort(const int& maxByCohort){ m_MaxByCohort = maxByCohort; }
void GSP::setMaxAbundLow(const int& maxAbundLow){ m_MaxAbundLow = maxAbundLow; }
void GSP::setMaxAbundMedium(const int& maxAbundMedium){ m_MaxAbundMedium = maxAbundMedium; }
void GSP::setMaxAbundHigh(const int& maxAbundHigh){ m_MaxAbundHigh = maxAbundHigh; }
void GSP::setDoLightCompetition(const bool& doLightCompetition){ m_DoLightCompetition = doLightCompetition; }
void GSP::setLightThreshLow(const int& lightThreshLow){ m_LightThreshLow = lightThreshLow; }
void GSP::setLightThreshMedium(const int& lightThreshMedium){ m_LightThreshMedium = lightThreshMedium; }
void GSP::setDoHabSuitability(const bool& doHabSuitability){ m_DoHabSuitability = doHabSuitability; }
void GSP::setHabSuitOption(const int& habSuitOption){ m_HabSuitOption = habSuitOption; }
void GSP::setDoDispersal(const bool& doDispersal){ m_DoDispersal = doDispersal; }
void GSP::setModeDispersed(const int& modeDispersed){ m_ModeDispersed = modeDispersed; }
void GSP::setDoDisturbances(const bool& doDisturbances){ m_DoDisturbances = doDisturbances; }
void GSP::setNbDisturbances(const int& nbDisturbances){ m_NbDisturbances = nbDisturbances; }
void GSP::setNbDistSub(const int& nbDistSub){ m_NbDistSub = nbDistSub; }
void GSP::setFreqDisturbances(const vector<int>& freqDisturbances){ m_FreqDisturbances = freqDisturbances; }
void GSP::setDoSoilCompetition(const bool& doSoilCompetition){ m_DoSoilCompetition = doSoilCompetition; }
void GSP::setDoFireDisturbances(const bool& doFireDisturbances){ m_DoFireDisturbances = doFireDisturbances; }
void GSP::setNbFireDisturbances(const int& nbFireDisturbances){ m_NbFireDisturbances = nbFireDisturbances; }
void GSP::setNbFireSub(const int& nbFireSub){ m_NbFireSub = nbFireSub; }
void GSP::setFreqFires(const vector<int>& freqFires){ m_FreqFires = freqFires; }
void GSP::setInitOption(const int& initOption){ m_InitOption = initOption; }
void GSP::setNeighOption(const int& neighOption){ m_NeighOption = neighOption; }
void GSP::setPropOption(const int& propOption){ m_PropOption = propOption; }
void GSP::setQuotaOption(const int& quotaOption){ m_QuotaOption = quotaOption; }
void GSP::setNbFires(const vector<int>& nbFires){ m_NbFires = nbFires; }
void GSP::setPrevData(const vector<int>& prevData){ m_PrevData = prevData; }
void GSP::setCCextent(const vector<int>& ccExtent){ m_CCextent = ccExtent; }
void GSP::setFireProb(const vector<double>& fireProb){ m_FireProb = fireProb; }
void GSP::setFlammMax(const int& flammMax){ m_FlammMax = flammMax; }
void GSP::setLogisInit(const vector<double>& logisInit){ m_LogisInit = logisInit; }
void GSP::setLogisSpread(const vector<double>& logisSpread){ m_LogisSpread = logisSpread; }
void GSP::setNbClimVar(const int& nbClimVar){ m_NbClimVar = nbClimVar; }
void GSP::setDoDroughtDisturbances(const bool& doDroughtDisturbances){ m_DoDroughtDisturbances = doDroughtDisturbances; }
void GSP::setNbDroughtSub(const int& nbDroughtSub){ m_NbDroughtSub = nbDroughtSub; }
void GSP::setChronoPost(const string& chronoPost){ m_ChronoPost = chronoPost; }
void GSP::setChronoCurr(const string& chronoCurr){ m_ChronoCurr = chronoCurr; }
void GSP::setDoHabStabilityCheck(const bool& doHabStabilityCheck){ m_DoHabStabilityCheck = doHabStabilityCheck; }
void GSP::setNbHabitats(const int& nbHabitats){ m_NbHabitats = nbHabitats; }
void GSP::setDoAliensDisturbance(const bool& doAliensDisturbance){ m_DoAliensDisturbance = doAliensDisturbance; }
void GSP::setFreqAliens(const vector<int>& freqAliens){ m_FreqAliens = freqAliens; }

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
/* Other functions                                                                                 */
/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

void GSP::show()
{
	cout << endl;
	cout << "Global Simulation Parameters:" << endl;
	cout << endl;
	cout << "m_NbCpus = " << m_NbCpus << endl;
	cout << "m_NbFG = " << m_NbFG << endl;
	cout << "m_NbStrata = " << m_NbStrata << endl;
	cout << "m_SimulDuration = " << m_SimulDuration << endl;
	cout << "m_SeedingDuration = " << m_SeedingDuration << endl;
	cout << "m_SeedingTimeStep = " << m_SeedingTimeStep << endl;
	cout << "m_SeedingInput = " << m_SeedingInput << endl;
	cout << "m_MaxByCohort = " << m_MaxByCohort << endl;
	cout << "m_MaxAbundLow = " << m_MaxAbundLow << endl;
	cout << "m_MaxAbundMedium = " << m_MaxAbundMedium << endl;
	cout << "m_MaxAbundHigh = " << m_MaxAbundHigh << endl;
	cout << "m_DoLightCompetition = " << m_DoLightCompetition << endl;
	cout << "m_LightThreshLow = " << m_LightThreshLow << endl;
	cout << "m_LightThreshMedium = " << m_LightThreshMedium << endl;
	cout << "m_DoHabSuitability = " << m_DoHabSuitability << endl;
	cout << "m_HabSuitOption = " << m_HabSuitOption << endl;
	cout << "m_DoDispersal = " << m_DoDispersal << endl;
	cout << "m_ModeDispersed = " << m_ModeDispersed << endl;
	cout << "m_DoDisturbances = " << m_DoDisturbances << endl;
	cout << "m_NbDisturbances = " << m_NbDisturbances << endl;
	cout << "m_NbDistSub = " << m_NbDistSub << endl;
	cout << "m_FreqDisturbances = ";
	copy(m_FreqDisturbances.begin(), m_FreqDisturbances.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << "m_DoSoilCompetition = " << m_DoSoilCompetition << endl;
	cout << "m_DoFireDisturbances = " << m_DoFireDisturbances << endl;
	cout << "m_NbFireDisturbances = " << m_NbFireDisturbances << endl;
	cout << "m_NbFireSub = " << m_NbFireSub << endl;
	cout << "m_FreqFires = ";
	copy(m_FreqFires.begin(), m_FreqFires.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << "m_InitOption = " << m_InitOption << endl;
	cout << "m_NeighOption = " << m_NeighOption << endl;
	cout << "m_PropOption = " << m_PropOption << endl;
	cout << "m_QuotaOption = " << m_QuotaOption << endl;
	cout << "m_NbFires = ";
	copy(m_NbFires.begin(), m_NbFires.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << "m_PrevData = ";
	copy(m_PrevData.begin(), m_PrevData.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << "m_CCextent = ";
	copy(m_CCextent.begin(), m_CCextent.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << "m_FireProb = ";
	copy(m_FireProb.begin(), m_FireProb.end(), ostream_iterator<double>(cout, " "));
	cout << endl;
	cout << "m_FlammMax = " << m_FlammMax << endl;
	cout << "m_LogisInit = ";
	copy(m_LogisInit.begin(), m_LogisInit.end(), ostream_iterator<double>(cout, " "));
	cout << endl;
	cout << "m_LogisSpread = ";
	copy(m_LogisSpread.begin(), m_LogisSpread.end(), ostream_iterator<double>(cout, " "));
	cout << endl;
	cout << "m_NbClimVar = " << m_NbClimVar << endl;
	cout << "m_DoDroughtDisturbances = " << m_DoDroughtDisturbances << endl;
	cout << "m_NbDroughtSub = " << m_NbDroughtSub << endl;
	cout << "m_ChronoPost = " << m_ChronoPost << endl;
	cout << "m_ChronoCurr = " << m_ChronoCurr << endl;
	cout << "m_DoHabStabilityCheck = " << m_DoHabStabilityCheck << endl;
	cout << "m_NbHabitats = " << m_NbHabitats << endl;
	cout << "m_DoAliensDisturbance = " << m_DoAliensDisturbance << endl;
	cout << "m_FreqAliens = ";
	copy(m_FreqAliens.begin(), m_FreqAliens.end(), ostream_iterator<int>(cout, " "));
	cout << endl;
	cout << endl;
}

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/

int GSP::AbundToInt(Abund abund)
{
	/* Convert Abundance classes to integer (defined by user) */
	int res = 0;
	switch (abund)
	{
		case ANone: case Acount:
			res =  0;
			break;
		case ALow:
			res = m_MaxAbundLow;
			break;
		case AMedium:
			res = m_MaxAbundMedium;
			break;
		case AHigh:
			res = m_MaxAbundHigh;
			break;
	}
	return res;
}
