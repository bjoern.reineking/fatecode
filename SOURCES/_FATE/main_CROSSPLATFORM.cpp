/*=============================================================================*/
/*               FUNCTIONAL ATTRIBUTES IN TERRESTRIAL ECOSYSTEMS               */
/*                                 Version 1.1                                 */
/*                                                                             */
/* Biological model, including succession, disturbance and environmental       */
/* response sections.                                                          */
/*                                                                             */
/*=============================================================================*/

#include <iostream>
#include <memory>
#include <cstring>
#include <sstream>
#include <fstream>
#include <cstdio>
#include <vector>
#include <cmath>
#include <ctime>

#include "stdlib.h"
#include "stdio.h"
#include "string.h"

#include <assert.h> // to check objects equivalences

#if defined(__unix__) || defined(__linux__) || defined(linux) || defined(LINUX)
	#include "gdal.h"
	#include <ogr_spatialref.h>
#endif

#include "gdal_priv.h" // to read raster files
#include "cpl_conv.h"

/* header files */
#include "FilesOfParamsList.h"
#include "GlobalSimulParameters.h"
#include "FGUtils.h"
#include "FG.h"
#include "Cohort.h"
#include "Legion.h"
#include "FuncGroup.h"
#include "PropPool.h"
#include "SuFate.h"
#include "SuFateH.h"
#include "SimulMap.h"
#include "Disp.h"
#include "Spatial.hpp"

/* to save and load simulation objects */
#include <boost/archive/text_oarchive.hpp> // to create archive
#include <boost/archive/text_iarchive.hpp> // to read archive
#include <boost/lexical_cast.hpp> // to transform int into string
#include <boost/filesystem.hpp>   // for file manipulation
#include <boost/serialization/export.hpp> // for children class serialisation
#include <boost/serialization/vector.hpp>
#include <boost/serialization/nvp.hpp>
#include <boost/serialization/version.hpp>

BOOST_CLASS_EXPORT_GUID(SuFateH, "SuFateH")

/*=============================================================================*/

#if defined(__unix__) || defined(__linux__) || defined(linux) || defined(LINUX)
	/* to get virtual and physical memory information */
	#include "sys/types.h" // Unix-Mac
	#include "sys/sysinfo.h" // Unix

	struct sysinfo memInfo; // Unix
	
	/* MEMORY CURRENTLY USED BY CURRENT PROCESS : Unix */
	int parseLine(char* line)
	{
		int i = strlen(line);
		while (*line < '0' || *line > '9') line++;
		line[i-3] = '\0';
		i = atoi(line);
		return i;
	}
	
	int getMemUsed(string typeMEM)
	{ //Note: this value is in KB!
		FILE* file = fopen("/proc/self/status", "r");
		int result = -1;
		char line[128];
		while (fgets(line, 128, file) != NULL)
		{
			if (strcmp(typeMEM.c_str(),"virtual") == 0)
			{
				if (strncmp(line, "VmSize:", 7)==0)
				{ // FOR VIRTUAL MEMORY
					result = parseLine(line);
					break;
				}
			}
			if (strcmp(typeMEM.c_str(),"physical") == 0)
			{
				if (strncmp(line, "VmRSS:", 6) == 0)
				{ // FOR PHYSICAL MEMORY (RAM)
					result = parseLine(line);
					break;
				}
			}
		}
		fclose(file);
		return result;
	}
#elif defined(__APPLE__)
	/* to get virtual and physical memory information */
	#include <mach/mach.h>  // Mac
	#include <mach/vm_statistics.h> // Mac
	#include <mach/mach_types.h> // Mac
	#include <mach/mach_init.h> // Mac
	#include <mach/mach_host.h> // Mac
	
	struct task_basic_info t_info; // Mac
	mach_msg_type_number_t t_info_count = TASK_BASIC_INFO_COUNT; // Mac
#endif

/*=============================================================================*/

using namespace std;

/* some global variables */
string FATEHDD_VERSION = "6.2-3";
SimulMap* simulMap;

void saveFATE(string objFileName)
{
	// Create an output archive
	ofstream ofs(objFileName.c_str(), fstream::binary | fstream::out);
	boost::archive::text_oarchive ar(ofs);
	ar << simulMap; // Write data
	ofs.close();
}
void loadFATE(string objFileName)
{
	// Create an input archive
	ifstream ifs( objFileName.c_str(), fstream::binary | fstream::in ); //ios::in | ios::binary );
	if (ifs.good())
	{
		boost::archive::text_iarchive ar(ifs);
		ar >> simulMap; // load data
		ifs.close(); // close file
	} else
	{
		cerr << "!!! File of SAVED STATE does not exists or can't be opened!" << endl;
		terminate();
	}
}
void changeFile(int year, string change_type, vector<int>& change_times, vector<string>& change_files)
{
	if (change_times.size() > 0 && change_times.front() == year)
	{
		cout << "Doing " << change_type << " masks change..." << endl;
		simulMap->DoFileChange(change_files.front(), change_type);
		/* remove useless time and file from list */
		change_times.erase(change_times.begin());
		change_files.erase(change_files.begin());
	}
}

void changeFreq(int year, string change_type, vector<int>& freq_change_times, vector<string>& freq_change_files)
{
	if (freq_change_times.size() > 0 && freq_change_times.front() == year)
	{
		cout << "Doing " << change_type << " frequencies change..." << endl;
		simulMap->DoFreqChange(freq_change_files.front(), change_type);
		/* remove useless time and file from list */
		freq_change_times.erase(freq_change_times.begin());
		freq_change_files.erase(freq_change_files.begin());
	}
}


/*=============================================================================*/

int main(int argc, char* argv[])
{
	/* Time consuming measurement */
	time_t Start, End;
	time(&Start);
	
	/* Initializing a random generator seed */
	srand(time(NULL));
	
	/* Read global parameter for this simulation */
	cout << endl;
	cout << "*********************************************" << endl;
	cout << "   WELCOME TO FATE-HDD SIMULATION RUNTIME" << endl;
	cout << "*********************************************" << endl;
	cout << endl;
	
	/*=============================================================================*/
	/* Test on given arguments */
	{
		// If the user didn't provide a filename command line argument, print an error and exit.
		if (argc <= 1)
		{
			cout << "Usage: " << argv[0] << " <parameters_file>" << endl;
			cout << endl;
			cout << "You can also ask for software version info with " << argv[0] << " -v" << endl;
			exit(1);
		}
		
		stringstream ss;
		string arg_test;
		for (int i=1; i<=argc-1; i++)
		{
			ss << argv[i];
			ss >> arg_test;
			if (arg_test == "-v" || arg_test == "--version")
			{
				cout << argv[0] << " version is " << FATEHDD_VERSION << endl;
				exit(1);
			}
			if (arg_test == "-np")
			{
				i++;
				int nb_cpus;
				ss << argv[i];
				ss >> nb_cpus; // get the number of cpu required
				cout << "PARALLEL VERSION : programme will run on " << nb_cpus << " cpus." << endl;
				omp_set_num_threads(nb_cpus);
			}
		}
	} // end test arguments
	
	/*=============================================================================*/
	/* Arguments reading */
	string paramFile(argv[argc-1]);
	
	/* print input args */
	cout << "This simulation will be based on " << paramFile  << " parameters file."<< endl;
	cout << endl;
	
	/* create the simulation parameter object that store path to all needed parameters files */
	FOPL file_of_params(paramFile);
	cout << endl;
	cout << "File of parameters created !" << endl;
	file_of_params.checkCorrectParams();
	file_of_params.checkCorrectMasks();
	file_of_params.show();
	
	/*=============================================================================*/
	/* FILE for saving COMPUTATION statistics */
	
	/* Headed file creation */
	ostringstream ossFileName;
	ossFileName.str("");
	ossFileName << file_of_params.getSavingDir() << "/ComputationStatistics.txt";
	string strFileName = ossFileName.str();
	
	/* Open the file or create it */
	ofstream FileStats(strFileName.c_str(), ios::out | ios::trunc);
	
	#if defined(__unix__) || defined(__linux__) || defined(linux) || defined(LINUX)
		/* Memory consuming measurement */
		sysinfo (&memInfo); // Unix
		
		/* TOTAL VIRTUAL MEMORY */
		long long totalVirtualMem = memInfo.totalram;
		totalVirtualMem += memInfo.totalswap;
		totalVirtualMem *= memInfo.mem_unit;
		
		/* TOTAL PHYSICAL MEMORY (RAM) */
		long long totalPhysMem = memInfo.totalram;
		totalPhysMem *= memInfo.mem_unit;
		
		FileStats << "TOTAL VIRTUAL MEMORY : " << totalVirtualMem << endl;
		FileStats << "TOTAL PHYSICAL MEMORY : " << totalPhysMem << endl;
		FileStats << "Initial VIRTUAL MEMORY used : " << getMemUsed("virtual") << endl;
		FileStats << "Initial PHYSICAL MEMORY used : " << getMemUsed("physical") << endl;
	#elif defined(__APPLE__)
		/* Memory consuming measurement */
		vm_size_t page_size; // Mac
		mach_port_t mach_port; // Mac
		mach_msg_type_number_t count; // Mac
		vm_statistics64_data_t vm_stats; // Mac
	#endif
	
	/*=============================================================================*/
	/* check if a saving of an old simulation is given or if we start a new one from scratch */
	
	// the map on which the whole simulation process is based on
	if (file_of_params.getSavedState() == "0")
	{ // start from scratch
		cout << "Starting from scratch..." << endl;
		simulMap = new SimulMap(file_of_params);
	} else
	{ // start from previous simulation state
		cout << "Starting with outputs stored in " << file_of_params.getSavedState() << " file" << endl;
		{
			cout << "Loading previous simulation outputs..." << endl;
			loadFATE(file_of_params.getSavedState());
			cout << "> done! " << endl;
		}
		
		/* update the simulation parameters (replace the object saved ones by the current ones) */
		cout << "*** UPDATE simulation files..." << endl;
		simulMap->UpdateSimulationParameters(file_of_params);
		
		// FROM SAVED STATE BUT WITH NEW FG file_of_params
		cout << "*** REBUILDING Global simulation parameters..." << endl;
		GSP glob_params = GSP(file_of_params.getGlobSimulParams());
		int nb_fg = glob_params.getNbFG();
		cout << "*** REBUILDING Functional groups..." << endl;
		if (nb_fg!=(int)file_of_params.getFGLifeHistory().size())
		{
			cerr << "!!! Parameters NB_FG (" << nb_fg << ") and --PFG_LIFE_HISTORY_PARAMS-- (" ;
			cerr << file_of_params.getFGLifeHistory().size() << ") do not match in term of number!" << endl;
			terminate();
		}
		if (glob_params.getDoDispersal() && nb_fg!=(int)file_of_params.getFGDispersal().size())
		{
			cerr << "!!! Parameters NB_FG (" << nb_fg << ") and --PFG_DISPERSAL_PARAMS-- (" ;
			cerr << file_of_params.getFGDispersal().size() << ") do not match in term of number!" << endl;
			terminate();
		}
		if (glob_params.getDoDisturbances() && nb_fg!=(int)file_of_params.getFGDisturbance().size())
		{
			cerr << "!!! Parameters NB_FG (" << nb_fg << ") and --PFG_DISTURBANCES_PARAMS-- (" ;
			cerr << file_of_params.getFGDisturbance().size() << ") do not match in term of number!" << endl;
			terminate();
		}
		if (glob_params.getDoFireDisturbances() && nb_fg!=(int)file_of_params.getFGFire().size())
		{
			cerr << "!!! Parameters NB_FG (" << nb_fg << ") and --PFG_FIRES_PARAMS-- (" ;
			cerr << file_of_params.getFGFire().size() << ") do not match in term of number!" << endl;
			terminate();
		}
		if (glob_params.getDoDroughtDisturbances() && nb_fg!=(int)file_of_params.getFGDrought().size())
		{
			cerr << "!!! Parameters NB_FG (" << nb_fg << ") and --PFG_DROUGHT_PARAMS-- (" ;
			cerr << file_of_params.getFGDrought().size() << ") do not match in term of number!" << endl;
			terminate();
		}
		vector<FG> fg_vec_tmp;
		for (int fg_id=0; fg_id<nb_fg; fg_id++)
		{
			FG fg_tmp = FG(glob_params, file_of_params, fg_id);
			fg_tmp.show();
			fg_vec_tmp.push_back(fg_tmp);
		}
		simulMap->setFGparams(fg_vec_tmp);
	}
	
	cout << "\n***" << " NbCpus = " << simulMap->getGlobalParameters().getNbCpus() << endl;
	FileStats << "Number of CPUs used : " << simulMap->getGlobalParameters().getNbCpus() << endl;
	
	/*=============================================================================*/
	/* get all needed parameters */
	
	/* timing parameters */
	cout << "Getting timing parameters..." << endl;
	int simul_duration = simulMap->getGlobalParameters().getSimulDuration();
	bool seeding_on = false;
	int seeding_duration = simulMap->getGlobalParameters().getSeedingDuration();
	int seeding_timestep = simulMap->getGlobalParameters().getSeedingTimeStep();
	
	/* saving parameters */
	cout << "Getting saving parameters..." << endl;
	vector< int > summarised_array_saving_times = ReadTimingsFile( file_of_params.getSummArrSavingTimes() );
	vector< int > simul_objects_saving_times = ReadTimingsFile( file_of_params.getObjectsSavingTimes() );
	
	/* study area change parameters */
	cout << "Getting MASK timing parameters..." << endl;
	vector< int > mask_change_times = ReadTimingsFile( file_of_params.getMaskChangeTimes() );
	vector< string > mask_change_files = file_of_params.getMaskChangeMasks();
	
	/* MODULES change parameters */
	if (simulMap->getGlobalParameters().getDoHabSuitability())
	{
		cout << "Getting HS timing parameters..." << endl;
	}
	vector< int > hab_change_times = ReadTimingsFile( file_of_params.getHabChangeTimes() );
	vector< string > hab_change_files = file_of_params.getHabChangeMasks();
	if (simulMap->getGlobalParameters().getDoDisturbances())
	{
		cout << "Getting dist timing parameters..." << endl;
	}
	vector< int > dist_change_times = ReadTimingsFile( file_of_params.getDistChangeTimes() );
	vector< string > dist_change_files = file_of_params.getDistChangeMasks();
	if (simulMap->getGlobalParameters().getDoFireDisturbances())
	{
		cout << "Getting fire timing parameters..." << endl;
	}
	vector< int > fire_change_times = ReadTimingsFile( file_of_params.getFireChangeTimes() );
	vector< string > fire_change_files = file_of_params.getFireChangeMasks();
	vector< int > fire_freq_change_times = ReadTimingsFile( file_of_params.getFireFreqChangeTimes() );
	vector< string > fire_freq_change_files = file_of_params.getFireChangeFrequencies();
	if (simulMap->getGlobalParameters().getDoFireDisturbances())
	{
		cout << "Getting climData timing parameters..." << endl;
	}
	vector< int > climData_change_times = ReadTimingsFile( file_of_params.getClimDataChangeTimes() );
	vector< string > climData_change_files = file_of_params.getClimDataChangeMasks();
	if (simulMap->getGlobalParameters().getDoDroughtDisturbances())
	{
		cout << "Getting moist index timing parameters..." << endl;
	}
	vector< int > moist_change_times = ReadTimingsFile( file_of_params.getMoistIndexChangeTimes() );
	vector< string > moist_change_files = file_of_params.getMoistIndexChangeMasks();
	if (simulMap->getGlobalParameters().getDoAliensDisturbance())
	{
		cout << "Getting aliens timing parameters..." << endl;
	}
	vector< int > aliens_change_times = ReadTimingsFile( file_of_params.getAliensChangeTimes() );
	vector< string > aliens_change_files = file_of_params.getAliensChangeMasks();
	vector< int > aliens_freq_change_times = ReadTimingsFile( file_of_params.getAliensFreqChangeTimes() );
	vector< string > aliens_freq_change_files = file_of_params.getAliensChangeFrequencies();
	
	/*=============================================================================*/
	/* Simulation main loop */
	for (int year=0; year<=simul_duration; year++)
	{
		cout << endl;
		cout << "Starting year " << year << " :" << endl;
		
		/* SAVING OUTPUTS PROCEDURE =================================================*/
		/* Saving computing statistics */
		if (year%100==0)
		{
			#if defined(__unix__) || defined(__linux__) || defined(linux) || defined(LINUX)
				FileStats << "Year " << year << ", VIRTUAL MEMORY used : " << getMemUsed("virtual") << endl;
				FileStats << "Year " << year << ", PHYSICAL MEMORY used : " << getMemUsed("physical") << endl;
			#elif defined(__APPLE__) 
				if (KERN_SUCCESS != task_info(mach_task_self(), TASK_BASIC_INFO, (task_info_t)&t_info, &t_info_count)) { return -1; }
				FileStats << "Year " << year << ", RESIDENT SIZE : " << t_info.resident_size << endl;
				FileStats << "Year " << year << ", VIRTUAL MEMORY used : " << t_info.virtual_size << endl;
				
				mach_port = mach_host_self();
				count = sizeof(vm_stats) / sizeof(natural_t);
				if (KERN_SUCCESS == host_page_size(mach_port, &page_size) && KERN_SUCCESS == host_statistics64(mach_port, HOST_VM_INFO,(host_info64_t)&vm_stats, &count))
				{
					long long free_memory = (int64_t)vm_stats.free_count * (int64_t)page_size;
					long long used_memory = ((int64_t)vm_stats.active_count + (int64_t)vm_stats.inactive_count + (int64_t)vm_stats.wire_count) *  (int64_t)page_size;
					FileStats << "Year " << year << ", PHYSICAL FREE MEMORY : " << free_memory << endl;
					FileStats << "Year " << year << ", PHYSICAL USED MEMORY : " << used_memory << endl;
				}
			#endif
			
			time(&End);
			int TotTime = difftime(End,Start);
			FileStats << "Year " << year << ", COMPUTATION TIME : " << TotTime/3600 << "h " << (TotTime%3600)/60 << "m " << (TotTime%3600)%60 << "s" << endl;
		}
		
		/* omp_set_num_threads( simulMap->getGlobalParameters().getNbCpus() );
		#pragma omp parallel
		{*/
		/* Saving summarised array */
		if (summarised_array_saving_times.size() > 0 && summarised_array_saving_times.front() == year)
		{
			cout << "Saving rasters..." << endl;
			simulMap->SaveRasterAbund( file_of_params.getSavingDir(), year, file_of_params.getMask());
			/* remove saved time from list */
			summarised_array_saving_times.erase(summarised_array_saving_times.begin());
		}
		
		/* Saving simulation object */
		if (simul_objects_saving_times.size() > 0 && simul_objects_saving_times.front() == year)
		{
			cout << "Saving simulation object..." << endl;
			{
				// Create an output archive
				string objFileName = file_of_params.getSavingDir() + "SimulMap_" + boost::lexical_cast<string>(year) + ".sav";
				cout << objFileName.c_str() << endl;
				saveFATE(objFileName);
			}
			cout << "> done! " << endl;
			
			/* remove saved time from list */
			simul_objects_saving_times.erase(simul_objects_saving_times.begin());
			
			/* TEST EQUALITY */
			/*string objFileName = file_of_params.getSavingDir() + "SimulMap_" + boost::lexical_cast<string>(year) + ".sav";
			loadFATE(objFileName);
			assert(*test == *simulMap);*/
		}
		
		/* Do mask, climat, disturbances and climatic data changes ==================*/
		/* Do MASK change */
		if (mask_change_times.size() > 0 && mask_change_times.front() == year)
		{
			cout << "Doing MASK change..." << endl;
			simulMap->DoFileChange(mask_change_files.front(), "mask");
			
			/* Change mask name in file_of_params to create outputs rasters with the correct studied area */
			string strTmp; // tmp string to keep change filenames
			vector< string > newNameFiles; // vector of change filenames
			
			/* open newChangeFile */
			ifstream file(mask_change_files.front().c_str(), ios::in);
			if (file)
			{
				/* Read file line by line */
				while (file >> strTmp)
				{
					if (strTmp != "")
					{
						/* store new files */
						newNameFiles.push_back(strTmp);
						cout << "*** " << strTmp << endl;
					}
				}
				
				/* Close file */
				file.close();
			} else
			{
				cerr << "Impossible to open " << mask_change_files.front() << " file!" << endl;
				terminate();
			}
			file_of_params.setMask(newNameFiles[0]); // change mask name
			file_of_params.checkCorrectMasks(); // check that new mask is similar to the other simulation masks
			
			/* remove useless time and file from list */
			mask_change_times.erase(mask_change_times.begin());
			mask_change_files.erase(mask_change_files.begin());
		}
		
		/* Do habitat suitability change */
		if (simulMap->getGlobalParameters().getDoHabSuitability())
		{
			changeFile(year, "habSuit", hab_change_times, hab_change_files);
		}
		
		/* Do disturbances change */
		if (simulMap->getGlobalParameters().getDoDisturbances())
		{
			changeFile(year, "dist", dist_change_times, dist_change_files);
		}
		
		/* FIRE DISTURBANCE */
		if (simulMap->getGlobalParameters().getDoFireDisturbances())
		{
			/* Do fire change */
			changeFile(year, "fire", fire_change_times, fire_change_files);
			/* Do fire frequencies change */
			changeFreq(year, "fire", fire_freq_change_times, fire_freq_change_files);
			/* Do climatic data change */
			changeFile(year, "climData", climData_change_times, climData_change_files);
		}
		
		/* DROUGHT DISTURBANCE */
		/* Do moisture index change */
		if (simulMap->getGlobalParameters().getDoDroughtDisturbances())
		{
			changeFile(year, "moist", moist_change_times, moist_change_files);
		}
		
		/* ALIENS DISTURBANCE */
		if (simulMap->getGlobalParameters().getDoAliensDisturbance())
		{
			/* Do aliens introduction change */
			changeFile(year, "aliens", aliens_change_times, aliens_change_files);
			/* Do aliens introduction frequencies change */
			changeFreq(year, "aliens", aliens_freq_change_times, aliens_freq_change_files);
		}
		
		/* Check seeding parameters =================================================*/
		/* DISPERSAL MODULE */
		if (simulMap->getGlobalParameters().getDoDispersal())
		{
			if (seeding_duration > 0 && year < seeding_duration)
			{
				if (year % seeding_timestep == 0)
				{
					cout << "Seeding occurs this year..." << endl;
					seeding_on = true;
					simulMap->StartSeeding();
				} else
				{
					seeding_on = false;
					simulMap->StopSeeding();
				}
			}
			
			/* Stop seeding the last year of seeding */
			if (seeding_duration > 0 && year == seeding_duration)
			{
				cout << "End of seeding campain..." << endl;
				seeding_on = false;
				simulMap->StopSeeding();
			}
		}
		
		/* Run aliens introduction model ============================================*/
		if (simulMap->getGlobalParameters().getDoAliensDisturbance())
		{
			simulMap->DoAliensIntroduction(year);
		}
		
		/* Run drought disturbance model : PREVIOUS succession ======================*/
		if (simulMap->getGlobalParameters().getDoDroughtDisturbances())
		{
			cout << "Calculate drought disturbances..." << endl;
			simulMap->DoDroughtDisturbance_part1();
			cout << "Apply drought disturbances..." << endl;
			simulMap->DoDroughtDisturbance_part2("prev");
		}
		
		/*===========================================================================*/
		/* Run succession model */
		cout << "Do Succession..." << endl;
		simulMap->DoSuccession();
		/*===========================================================================*/
		
		/* Run drought disturbance model : POST succession */
		if (simulMap->getGlobalParameters().getDoDroughtDisturbances())
		{
			cout << "Apply drought disturbances..." << endl;
			simulMap->DoDroughtDisturbance_part2("post");
		}
		
		/* Run seeds dispersal model ================================================*/
		if (simulMap->getGlobalParameters().getDoDispersal() && !seeding_on)
		{
			cout << "Disperse seeds..." << endl;
			simulMap->DoDispersal();
		}
		
		/* Run disturbance model ====================================================*/
		if (simulMap->getGlobalParameters().getDoDisturbances())
		{
			cout << "Apply disturbances..." << endl;
			simulMap->DoDisturbance(year);
		}
		
		/* Run fire disturbance model ===============================================*/
		if (simulMap->getGlobalParameters().getDoFireDisturbances())
		{
			cout << "Apply fire disturbances..." << endl;
			simulMap->DoFireDisturbance(year);
		}
		
		/* Run stability check ======================================================*/
		if (simulMap->getGlobalParameters().getDoHabStabilityCheck())
		{
			cout << "Save statistics..." << endl;
			simulMap->SaveStatsPerHabitat();
			if (year >= 5)
			{
				cout << "Apply stability check..." << endl;
				simulMap->StabilityCheck();
			}
		}
	} // end main simulation loop
	//} // end PRAGMA
	
	delete simulMap;
	
	/* End of Run */
	time(&End);
	int TotTime = difftime(End,Start);
	
	FileStats << "End of simul, COMPUTATION TIME : " << TotTime/3600 << "h " << (TotTime%3600)/60 << "m " << (TotTime%3600)%60 << "s" << endl;
	FileStats.close();
	
	cout 	<< "Process executed normally! It took "
	<< TotTime/3600 << "h " << (TotTime%3600)/60
	<< "m " << (TotTime%3600)%60 << "s." << endl;
	
	return 0;
}
