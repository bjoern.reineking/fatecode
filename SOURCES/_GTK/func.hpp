#ifndef FUNC_H
#define FUNC_H

using namespace std;

/*############################################################################################################################*/

string IntToString (int a)
{
  ostringstream temp;
  temp << a;
  return temp.str();
}
string DoubleToString (double a)
{
  ostringstream temp;
  temp << a;
  return temp.str();
}
int calcMaxAbund (string type, int h) //PASOK
{
  if(strcmp(type.c_str(),"H")==0){ return 1; }
  else if((strcmp(type.c_str(),"C")==0) & (h>=150)){ return 1; }
  else { return 3; }
}
int calcImmSize (string type, int h) //PASOK
{
  if((strcmp(type.c_str(),"C")==0) & (h>150)){ return 2; }
  else if((strcmp(type.c_str(),"P")==0) & (h<=1000)){ return 4; }
  else if((strcmp(type.c_str(),"P")==0) & (h>1000)){ return 2; }
  else { return 4; }
}

/*############################################################################################################################*/

void errorMessage(gchar* message){
  GtkWidget* dialog = gtk_message_dialog_new_with_markup(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_WARNING, GTK_BUTTONS_OK, message, NULL);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);
}

void infoMessage(gchar* message){
  GtkWidget* dialog = gtk_message_dialog_new_with_markup(NULL, GTK_DIALOG_MODAL, GTK_MESSAGE_INFO, GTK_BUTTONS_OK, message, NULL);
  gtk_dialog_run(GTK_DIALOG(dialog));
  gtk_widget_destroy(dialog);
}

bool carefulMessage(gchar* message){ //OK
  bool response = FALSE;

  GtkWidget* label_message = gtk_label_new(NULL);
  gtk_label_set_markup(GTK_LABEL(label_message),message);
  gtk_label_set_justify(GTK_LABEL(label_message), GTK_JUSTIFY_CENTER);
  GtkWidget* pBox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_set_border_width(GTK_CONTAINER(pBox), 20);
  gtk_box_pack_start(GTK_BOX(pBox), label_message, TRUE, FALSE, 0);

  GtkWidget* dialog = gtk_dialog_new_with_buttons("Be careful !", NULL,GTK_DIALOG_MODAL,
						  "OK",GTK_RESPONSE_OK,
						  "CANCEL",GTK_RESPONSE_CANCEL,NULL);
  GtkWidget* content_area = gtk_dialog_get_content_area (GTK_DIALOG (dialog));
  gtk_container_add (GTK_CONTAINER (content_area), pBox);
  gtk_widget_show_all(dialog);
  gtk_window_set_transient_for(GTK_WINDOW(dialog),NULL);
  gtk_window_set_modal(GTK_WINDOW(dialog),TRUE);

  switch (gtk_dialog_run(GTK_DIALOG(dialog))){
  case GTK_RESPONSE_OK:
    response = TRUE;
    break;
  case GTK_RESPONSE_CANCEL:
    break;
  }
  gtk_widget_destroy(dialog);
  return(response);
}

/*############################################################################################################################*/

void addToBox(GtkWidget* box, vector<GtkWidget*> toAdd, bool expand_v, bool expand_h){ //OK
  for(unsigned i=0; i<toAdd.size(); i++){
    gtk_box_pack_start(GTK_BOX(box), toAdd[i], expand_v, expand_h, 0);
  }
}

void addToContainer(GtkWidget* cont, vector<GtkWidget*> toAdd){ //OK
  for(unsigned i=0; i<toAdd.size(); i++){
    gtk_container_add(GTK_CONTAINER(cont), toAdd[i]);
  }
}

GtkWidget* expandInGrid(GtkWidget* wid){ //OK
  gtk_widget_set_hexpand(wid, TRUE);
  gtk_widget_set_halign(wid, GTK_ALIGN_FILL);
  return(wid);
}

void addToGrid(GtkWidget* cont, vector<GtkWidget*> toAdd){ //OK
  for(unsigned i=0; i<toAdd.size(); i++){
    gtk_grid_attach(GTK_GRID(cont), expandInGrid(toAdd[i]), i, 1, 1, 1);
  }
}

void addToGrid(GtkWidget* cont, vector<GtkWidget*> toAdd, gint row){ //OK
  for(unsigned i=0; i<toAdd.size(); i++){
    gtk_grid_attach(GTK_GRID(cont), expandInGrid(toAdd[i]), i, row, 1, 1);
  }
}

/*############################################################################################################################*/

void selectMaskFile(GtkWidget* pButton, gpointer data) //OK
{
  GtkWidget* selection = gtk_file_chooser_dialog_new(g_locale_to_utf8( "Select a mask file", -1, NULL, NULL, NULL),
						     NULL,GTK_FILE_CHOOSER_ACTION_OPEN,"Cancel", GTK_RESPONSE_CANCEL,"Select", GTK_RESPONSE_ACCEPT,NULL);
  gint res = gtk_dialog_run(GTK_DIALOG(selection));

  if (res==GTK_RESPONSE_ACCEPT){
    char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(selection));
    gtk_entry_set_text(GTK_ENTRY(data), filename);
  }
  gtk_widget_destroy(selection);
}

/*############################################################################################################################*/

void selectMaskDirectory(GtkWidget* pButton, gpointer data) //PASOK
{
  GtkWidget* selection = gtk_file_chooser_dialog_new(g_locale_to_utf8( "Select a directory", -1, NULL, NULL, NULL),
						     NULL,GTK_FILE_CHOOSER_ACTION_SELECT_FOLDER,"Cancel", GTK_RESPONSE_CANCEL,"Select",
						     GTK_RESPONSE_ACCEPT,NULL);
  gint res = gtk_dialog_run(GTK_DIALOG(selection));

  if (res==GTK_RESPONSE_ACCEPT){
    char* filename = gtk_file_chooser_get_filename(GTK_FILE_CHOOSER(selection));
    gtk_entry_set_text(GTK_ENTRY(data), filename);
  }
  gtk_widget_destroy(selection);
}

/*############################################################################################################################*/

#endif // FUNC_H

