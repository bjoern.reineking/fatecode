#ifndef FUNC_PARAM_FIRE_H
#define FUNC_PARAM_FIRE_H

using namespace std;

/*############################################################################################################################*/

void initOptData(GtkWidget* pRadio, gpointer data) //OK
{
  bool allume;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) { allume = TRUE; }
  else { allume = FALSE; }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
  pList = g_list_reverse(pList);
  for(unsigned i=0; i<7; i++){ pList = g_list_next(pList); }
  gtk_widget_set_sensitive(GTK_WIDGET(pList->data), allume);
  gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data), allume);
}

/*############################################################################################################################*/

void initOptMap(GtkWidget* pRadio, gpointer data) //OK
{
  bool allume, allume_data, allume_chao, allume_neigh = TRUE;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) { allume = FALSE; }
  else { allume = TRUE; }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_fire[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_fire[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))) { allume_data = TRUE; }
  else { allume_data = FALSE; }
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->next->data))) { allume_chao = TRUE; }
  else { allume_chao = FALSE; }
  for(unsigned i=0; i<5; i++){ ppList = g_list_next(ppList); }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume);
    ppList = g_list_next(ppList);
  }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume_data);
    ppList = g_list_next(ppList);
  }
  for(unsigned i=0; i<6; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume_chao);
    ppList = g_list_next(ppList);
  }

  pList = g_list_next(pList);                                         //box_fire_h[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) {
    for(unsigned i=0; i<11; i++){
      gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), FALSE);
      ppList = g_list_next(ppList);
    }
  } else {
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), TRUE);
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->next->data), TRUE);
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->next->next->data), TRUE);
    if((gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->data))) | (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data)))) {
      allume_neigh = FALSE;
      for(unsigned i=0; i<3; i++){ ppList = g_list_next(ppList); }
      for(unsigned i=0; i<8; i++){
	gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), TRUE);
	ppList = g_list_next(ppList);
      }
    }
  }

  if(allume_neigh){
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
    ppList = g_list_next(ppList);                                       //frame_fire[2]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[2]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
    ppList = g_list_reverse(ppList);
    for(unsigned i=0; i<4; i++){
      gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume);
      ppList = g_list_next(ppList);
    }

    pList = g_list_next(pList);                                         //box_fire_h[1]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[3]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
    ppList = g_list_reverse(ppList);
    for(unsigned i=0; i<4; i++){
      gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume);
      ppList = g_list_next(ppList);
    }
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));	     //frame_fire[0]
  pList = g_list_next(pList);                                                     //box_fire_h[0]
  pList = g_list_next(pList);                                                     //box_fire[1]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data)); 		     //table_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))){
    pList = g_list_next(pList);                                                     //frame_fire[4]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //table_fire[4]
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data), allume);
  } else {
    pList = g_list_next(pList);                                                     //frame_fire[4]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //table_fire[4]
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data), FALSE);
  }
}

/*############################################################################################################################*/

void initOptChao(GtkWidget* pRadio, gpointer data) //OK
{
  bool allume, allume_data;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) { allume = TRUE; }
  else { allume = FALSE; }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_fire[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_fire[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))) { allume_data = TRUE; }
  else { allume_data = FALSE; }
  if((allume==FALSE) & (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->next->data)))){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),TRUE);
  }
  for(unsigned i=0; i<4; i++){ ppList = g_list_next(ppList); }
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))==FALSE){ gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),allume); }
  ppList = g_list_next(ppList);
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), !allume);
    ppList = g_list_next(ppList);
  }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume_data);
    ppList = g_list_next(ppList);
  }
  for(unsigned i=0; i<6; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume);
    ppList = g_list_next(ppList);
  }

  pList = g_list_next(pList);                                         //box_fire_h[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  ppList = g_list_next(ppList);
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), !allume);
    ppList = g_list_next(ppList);
  }

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = g_list_next(ppList);                                       //frame_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  gtk_widget_set_sensitive(GTK_WIDGET(ppList->next->data), !allume);

  pList = g_list_next(pList);                                         //box_fire_h[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  if((allume==FALSE) & (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->data)))){
    gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),TRUE);
  }
  for(unsigned i=0; i<3; i++){ ppList = g_list_next(ppList); }
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))==FALSE){ gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),allume); }
  ppList = g_list_next(ppList);
  for(unsigned i=0; i<6; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume);
    ppList = g_list_next(ppList);
  }
}

/*############################################################################################################################*/

void neighOpt8(GtkWidget* pRadio, gpointer data) //OK
{
  bool allume;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) { allume = FALSE; }
  else { allume = TRUE; }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_fire[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_fire[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<4; i++){ ppList = g_list_next(ppList); }
  gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), !allume);

  pList = g_list_next(pList);                                         //box_fire_h[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<3; i++){ ppList = g_list_next(ppList); }
  for(unsigned i=0; i<8; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), allume);
    ppList = g_list_next(ppList);
  }

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = g_list_next(ppList);                                       //frame_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<4; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), !allume);
    ppList = g_list_next(ppList);
  }

  pList = g_list_next(pList);                                         //box_fire_h[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<4; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data), !allume);
    ppList = g_list_next(ppList);
  }
}

/*############################################################################################################################*/

void quotaOptCons(GtkWidget* pRadio, gpointer data) //OK
{
  bool allume;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) { allume = FALSE; }
  else { allume = TRUE; }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_fire[0]
  pList = g_list_next(pList);                                                     //box_fire_h[0]
  pList = g_list_next(pList);                                                     //box_fire_h[1]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_fire[3]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //table_fire[3]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  pList = g_list_reverse(pList);
  gtk_widget_set_sensitive(GTK_WIDGET(pList->data), allume);
  gtk_widget_set_sensitive(GTK_WIDGET(pList->next->next->data), allume);
  gtk_widget_set_sensitive(GTK_WIDGET(pList->next->next->next->data), allume);
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pList->next->data))==FALSE){ gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(pList->next->data),!allume); }
}

/*############################################################################################################################*/

void propOptIntensity(GtkWidget* pRadio, gpointer data) //OK
{
  bool allume;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pRadio))) { allume = TRUE; }
  else { allume = FALSE; }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_fire[3]
  pList = g_list_next(pList);                                                     //frame_fire[4]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //table_fire[4]
  gtk_widget_set_sensitive(GTK_WIDGET(pList->data), allume);
}


/*############################################################################################################################*/



#endif // FUNC_PARAM_FIRE_H
