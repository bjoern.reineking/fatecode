#ifndef FUNC_PARAM_MASK_H
#define FUNC_PARAM_MASK_H

using namespace std;

/*############################################################################################################################*/

void generateNewMask(GtkWidget* pButton, gpointer data) //OK
{
  GtkWidget* pTable = gtk_grid_new();
  gtk_container_set_border_width(GTK_CONTAINER(pTable), 10);

  GtkWidget* pEntry = gtk_entry_new();
  GtkWidget* Butt = gtk_button_new_with_label("Select NEW map");
  g_signal_connect(G_OBJECT(Butt), "clicked", G_CALLBACK(selectMaskFile), pEntry);

  gtk_grid_attach(GTK_GRID(pTable), gtk_label_new("New date"), 0, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), gtk_entry_new(), 0, 1, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), Butt, 1, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), pEntry, 1, 1, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), gtk_check_button_new(), 2, 0, 1, 2);

  gtk_box_pack_start(GTK_BOX((GtkWidget*) data), pTable, FALSE, FALSE, 0);
  gtk_widget_show_all(pTable);
}

void removeNewMask(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
  GList* ppList;
  gint pSize = g_list_length(pList);
  gint compt = 0;
  bool bool_stop = FALSE;

  while((compt<pSize) & (pSize>0)){
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    ppList = g_list_reverse(ppList);
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->next->data))){
      gtk_container_remove(GTK_CONTAINER((GtkWidget*) data), (GtkWidget*) pList->data);
      bool_stop = TRUE;
    }

    compt++;
    if(bool_stop){
      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
      pSize = g_list_length(pList);
      compt = 0;
      bool_stop = FALSE;
    } else { pList = g_list_next(pList); }
  }
}

void cleanMask(gpointer data, unsigned typeMask) //PASOK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data)); //BOX_v[0]
  for(unsigned i=0; i<typeMask; i++){ pList = g_list_next(pList); }
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data)); //frame[0] or box_h
  pList = g_list_next(pList); //frame
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data)); //pTable or notebook?
  gtk_widget_destroy(GTK_WIDGET(ppList->data));
  gtk_container_add(GTK_CONTAINER(pList->data), gtk_grid_new());

}
/*############################################################################################################################*/

void removeNewMaskFire(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
  GList* ppList;
  gint pSize = g_list_length(pList);
  gint compt = 0;
  bool bool_stop = FALSE;

  while((compt<pSize) & (pSize>0)){
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    ppList = g_list_reverse(ppList);
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->next->next->next->data))){
      gtk_container_remove(GTK_CONTAINER((GtkWidget*) data), (GtkWidget*) pList->data);
      bool_stop = TRUE;
    }

    compt++;
    if(bool_stop){
      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
      pSize = g_list_length(pList);
      compt = 0;
      bool_stop = FALSE;
    } else { pList = g_list_next(pList); }
  }
}


#endif // FUNC_PARAM_MASK_H
