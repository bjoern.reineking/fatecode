#ifndef FUNC_PARAM_PFG_H
#define FUNC_PARAM_PFG_H

using namespace std;

/*############################################################################################################################*/

void setParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_pfg_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));        //frame_pfg[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));         //table_pfg[0]
  ppList= gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<9; i++) { ppList = g_list_next(ppList); }       //label_pfg
  for(unsigned i=0; i<11; i++) {
    gtk_entry_set_text(GTK_ENTRY(GTK_WIDGET(ppList->data)),"");
    ppList = g_list_next(ppList);
  }

  pList = g_list_next(pList);                                                    //frame_pfg[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));               //table_pfg[2]
  ppList= gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<12; i++) {
    ppList = g_list_next(ppList);
    gtk_entry_set_text(GTK_ENTRY(GTK_WIDGET(ppList->data)),"");
    ppList = g_list_next(ppList);
  }
}

/*############################################################################################################################*/

void cleanDelAddPFG(gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = g_list_next(pList);                                                     //BOX_SUCC_v
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //box_succ_h
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //box_succ_v[0]

  /* Delete succession panel 1 (Change strata ages) */
  GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));        //box_succ_v[1]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));               //pButton1
  pppList = g_list_next(pppList);                                                   //pButton2
  gtk_widget_set_sensitive(GTK_WIDGET(pppList->data),false);
  pppList = g_list_next(pppList);                                                   //pButton3
  gtk_widget_set_sensitive(GTK_WIDGET(pppList->data),false);
  GtkWidget* label_tmp = gtk_label_new(NULL);
  gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
  gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
  gtk_button_set_image(GTK_BUTTON(pppList->data),label_tmp);

  pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));        	    //box_succ_v[1]
  pppList = g_list_next(pppList);                                                   //frame_succ[0]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));               //scrollbar0
  GList* ppppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));       //viewport
  if(g_list_length(ppppList)>0){
    gtk_widget_destroy(GTK_WIDGET(ppppList->data));
    gtk_container_add(GTK_CONTAINER(pppList->data), gtk_grid_new());
  }


  /* Delete succession panel 2 (Light dependant parameters) */
  pppList = g_list_next(ppList);        				            //box_succ_v[2]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));               //box_succ_v[3]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));               //pButton1
  pppList = g_list_next(pppList);                                                   //pButton2
  gtk_widget_set_sensitive(GTK_WIDGET(pppList->data),false);
  pppList = g_list_next(pppList);                                                   //pButton3
  gtk_widget_set_sensitive(GTK_WIDGET(pppList->data),false);
  label_tmp = gtk_label_new(NULL);
  txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
  gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
  gtk_button_set_image(GTK_BUTTON(pppList->data),label_tmp);

  pppList = g_list_next(ppList);        				            //box_succ_v[2]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));               //box_succ_v[3]
  pppList = g_list_next(pppList);                                                   //frame_succ[1]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));               //scrollbar1
  ppppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));              //viewport
  if(g_list_length(ppppList)>0){
    gtk_widget_destroy(GTK_WIDGET(ppppList->data));
    gtk_container_add(GTK_CONTAINER(pppList->data), gtk_grid_new());
  }

  /* Delete maskPFG panel */
  pList = g_list_next(pList);                                                     //BOX_MASK_v
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));        	  //mask_notebook
  if(g_list_length(pList)>0){
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));        	  //BOX_mask_v[0]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));        	  //frame_mask[0]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));        	  //box_mask_h[0]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));        	  //table_mask[0]
    ppList = g_list_next(ppList);                                                   //box_mask_v[0]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->next->data),false);
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(ppList->next->data),label_tmp);

    pList = g_list_next(pList);                                                     //frame_mask[1]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));        	  //notebook or pTable
    gtk_widget_destroy(GTK_WIDGET(ppList->data));
    gtk_container_add(GTK_CONTAINER(pList->data), gtk_grid_new());
  }
}

#endif // FUNC_PARAM_PFG_H
