#ifndef FUNC_PARAM_SIMUL_H
#define FUNC_PARAM_SIMUL_H

using namespace std;

/*############################################################################################################################*/

void generateStrata(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_sim_h[2]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));     //label

  if(gtk_entry_get_text_length(GTK_ENTRY(ppList->next->data))>0){
    unsigned nb_strata = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->data)));

    pList = g_list_next(pList);                                         //scrollbar_sim
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));     //viewport
    GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }


    GtkWidget* pTable = gtk_grid_new();
    vector<GtkWidget*> pEntry;
    for(unsigned i=0; i<(nb_strata+1); i++){
      pEntry.push_back(gtk_entry_new());
      gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),8);
    }
    gtk_entry_set_text(GTK_ENTRY(pEntry[0]),"0");
    gtk_widget_set_sensitive(GTK_WIDGET(pEntry[0]),FALSE);

    gtk_container_add(GTK_CONTAINER(GTK_WIDGET(pList->data)), pTable);
    gtk_container_set_border_width(GTK_CONTAINER(pTable), 5);

    for(unsigned i=0; i<(nb_strata+1); i++){
      gtk_grid_attach(GTK_GRID(pTable), gtk_label_new(("Strata"+IntToString(i)+IntToString(i+1)).c_str()), i, 0, 1, 1);
    }
    for(unsigned i=0; i<(nb_strata+1); i++){
      gtk_grid_attach(GTK_GRID(pTable), pEntry[i], i, 1, 1, 1);
    }
    gtk_widget_show_all(pTable);
  } else {
    gchar* message = g_locale_to_utf8("You must fill the \nstrata parameters !", -1, NULL, NULL, NULL);
    errorMessage(message);
    return;
  }
}

/*############################################################################################################################*/

void setCheckSensitiveSim(GtkWidget* pCheck, gpointer data) //OK
{
  const gchar* pLabel = gtk_button_get_label(GTK_BUTTON(pCheck));
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
  pList = g_list_reverse(pList);
  unsigned nb_param = 4;
  bool allume = TRUE;
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pCheck))) { allume = TRUE; }
  else { allume = FALSE; }

  if(strcmp(pLabel,"Disturbances")==0) {
    for(unsigned i=0; i<6; i++) { pList = g_list_next(pList); }
  } else if(strcmp(pLabel,"Soil competition")==0) {
    for(unsigned i=0; i<10; i++) { pList = g_list_next(pList); }
    nb_param = 2;
  } else if(strcmp(pLabel,"Fire disturbance")==0) {
    for(unsigned i=0; i<12; i++) { pList = g_list_next(pList); }
  } else if(strcmp(pLabel,"Habitat stability")==0) {
    for(unsigned i=0; i<16; i++) { pList = g_list_next(pList); }
    nb_param = 2;
  } else if(strcmp(pLabel,"Drought disturbance")==0) {
    for(unsigned i=0; i<18; i++) { pList = g_list_next(pList); }
    nb_param = 2;
  } else if(strcmp(pLabel,"Aliens introduction")==0) {
    nb_param = 0;
  }
  for(unsigned i=0; i<nb_param; i++) {
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),allume);
    pList = g_list_next(pList);
  }
}

/*############################################################################################################################*/

void setSimulParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_sim_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_sim[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_sim[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<7; i++) { ppList = g_list_next(ppList); }        //labels
  gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  ppList = g_list_next(ppList);   //compiled file
  for(unsigned i=0; i<2; i++) {
    ppList = g_list_next(ppList);
    gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  }
  ppList = g_list_next(ppList);   //succ_model
  for(unsigned i=0; i<2; i++) {
    ppList = g_list_next(ppList);
    gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  }

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_sim[0]
  ppList = g_list_next(ppList);   				      //frame_sim[1]
  GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //box_sim_v[0]
  pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));   //box_sim_h[2]
  GList* ppppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));
  gtk_entry_set_text(GTK_ENTRY(ppppList->next->data),"");

  pppList = g_list_next(pppList);                                       //scrollbar_sim
  ppppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));  //viewport
  GList* pppppList = gtk_container_get_children(GTK_CONTAINER(ppppList->data)); //pTable?
  pppppList = gtk_container_get_children(GTK_CONTAINER(pppppList->data));
  if(g_list_length(pppppList)>0){
    gtk_widget_destroy(GTK_WIDGET(ppppList->data));
    gtk_container_add(GTK_CONTAINER(pppList->data), gtk_grid_new());
  }


  ppList = g_list_next(ppList);   				      //frame_sim[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));  //box_sim_v[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  gtk_entry_set_text(GTK_ENTRY(ppList->next->data),"");
  gtk_entry_set_text(GTK_ENTRY(ppList->next->next->next->data),"");

  pList = g_list_next(pList);                                       //box_sim_h[1]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));  //frame_sim[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));  //table_sim[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<7; i++) { ppList = g_list_next(ppList); } //checkboxes
  for(unsigned i=0; i<7; i++) {
    gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
    ppList = g_list_next(ppList);
    ppList = g_list_next(ppList);
  }

  pList = g_list_next(pList);                                       //box_sim_v[2]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  GtkWidget* label_tmp = gtk_label_new(NULL);
  gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
  gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
  gtk_button_set_image(GTK_BUTTON(pList->next->data),label_tmp);

}


#endif // FUNC_PARAM_SIMUL_H
