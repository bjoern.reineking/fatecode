#ifndef CLASS_SIMUL_H
#define CLASS_SIMUL_H

using namespace std;

#include <boost/iostreams/concepts.hpp> 
#include <boost/iostreams/stream_buffer.hpp>
#include <iostream>

#ifdef G_OS_WIN32
#include <windows.h>
#endif

static bool running = FALSE;
static GPid child_pid;
static GIOChannel* outchannel;
static GIOChannel* errchannel;

/*############################################################################################################################*/

void addMsgInfo(GtkWidget* scrollview, GtkWidget* textview, gchar *buf){
  GtkTextIter enditer;
  GtkTextBuffer* buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER (buffer), &enditer);
  /* Check if messages are in UTF8. If not, assume they are in current locale and try to convert.
   *  We assume we're getting the stream in a 1-byte encoding here, ie. that we do not have cut-off
   *   characters at the end of our buffer (=BAD) */
  //cout << "TEST ENCODING UTF-8 : " << g_utf8_validate(buf,-1,NULL) << endl;
  if (g_utf8_validate(buf,-1,NULL)) {
    gtk_text_buffer_insert(buffer, &enditer, buf, -1);
    GtkAdjustment* vadj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrollview));
    gtk_adjustment_set_value(vadj, gtk_adjustment_get_upper(vadj));
    gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(scrollview), vadj);
  }
}
void addMsgInfoErr(GtkWidget* scrollview, GtkWidget* textview, gchar *buf){
  GtkTextIter enditer;
  GtkTextBuffer* buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER (buffer), &enditer);
  if (g_utf8_validate(buf,-1,NULL)) {
    gtk_text_buffer_insert(buffer, &enditer, buf, -1);
    GtkAdjustment* vadj = gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(scrollview));
    gtk_adjustment_set_value(vadj, gtk_adjustment_get_upper(vadj));
    gtk_scrolled_window_set_vadjustment(GTK_SCROLLED_WINDOW(scrollview), vadj);
  }
}

gboolean on_pipe_output (GIOChannel* source, GIOCondition condition, gpointer data){
if(running){
  if(condition == G_IO_HUP){
    g_io_channel_unref(source);
    return FALSE ;
  }
  gchar buffer[BUFSIZ+1];
  gsize* bytes_read = 0;
  g_io_channel_read_chars(source,buffer,BUFSIZ,bytes_read,NULL);
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data)); //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data)); //textview
  addMsgInfo((GtkWidget*) data, GTK_WIDGET(pList->data), buffer);
  while(gtk_events_pending()) gtk_main_iteration ();

  ofstream fichier("FATE_output_file_"+IntToString(child_pid)+".txt", ios::out | ios::app);
  if(fichier) {
    fichier << buffer;
    fichier.close();
    return TRUE;
  } else {
    cerr << "Failed to open FATE output file !" << endl;
    return FALSE;
  }
} else { return FALSE; }
}

gboolean on_pipe_error (GIOChannel* source, GIOCondition condition, gpointer data){
  if(condition == G_IO_HUP){
    running = FALSE;
    g_io_channel_unref(source);
    g_spawn_close_pid( child_pid );
    return FALSE ;
  }
  gchar buffer[BUFSIZ+1];
  gsize* bytes_read = 0;
  g_io_channel_read_chars(source,buffer,BUFSIZ,bytes_read,NULL);
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data)); //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data)); //textview
  addMsgInfoErr((GtkWidget*) data, GTK_WIDGET(pList->data), buffer);
  while(gtk_events_pending()) gtk_main_iteration ();

  ofstream fichier("FATE_error_file_"+IntToString(child_pid)+".txt", ios::out | ios::app);
  if(fichier) {
    fichier << buffer;
    fichier.close();
    return TRUE;
  } else {
    cerr << "Failed to open FATE error file !" << endl;
    return FALSE;
  }
}

/*****************************************************************
 *  wget_exit_cb : Our child process has exited.
 *****************************************************************/
/*
static void wget_exit_cb_unix (GPid child_pid, gint status, gpointer data) {
  g_print("Child PID: %lu exit status: %d\n", (gulong) child_pid, status);
  if (WIFEXITED (status)) { // Did child terminate in a normal way?
    if (WEXITSTATUS (status) == EXIT_SUCCESS) {
      g_print ("Child PID: %lu exited normally without errors.\n", (gulong) child_pid);
    } else {
      g_print ("Child PID: %lu exited with an error (code %d).\n", (gulong) child_pid, WEXITSTATUS (status));
    }
  } else if (WIFSIGNALED (status)) { // was it terminated by a signal
    g_print ("Child PID: %lu was terminated by signal %d\n", (gulong) child_pid, WTERMSIG(status));
  } else {
    g_print ("Child PID: %lu was terminated in some other way.\n", (gulong) child_pid);
  }
  //g_spawn_close_pid( child_pid );
}

#ifdef G_OS_WIN32
static void wget_exit_cb_windows (GPid child_pid, gint status, gpointer data) {
  g_print("Child PID: %lu exit status: %d\n", *((gulong*)child_pid), status);
  DWORD exitCode = 0;
  if (GetExitCodeProcess(child_pid,&exitCode)==TRUE) { // Did child terminate in a normal way?
    g_print ("Child PID: %lu exited normally without errors.\n", *((gulong*)child_pid));
  }
  if (exitCode!=STILL_ACTIVE) {
    g_print ("Child PID: %lu was terminated in some other way.\n", *((gulong*)child_pid));
  }
}
#endif*/

/*############################################################################################################################*/

string GetLastDir(string toLookAt, string delim){
  size_t pos = 0;
  string token;
  while ((pos = toLookAt.find(delim)) != string::npos) {
    token = toLookAt.substr(0, pos);
    toLookAt.erase(0, pos + delim.length());
  }
  return(toLookAt);
}

string GoBackDir(string toLookAt, string lastDir){
  size_t pos = toLookAt.find(lastDir);
  return(toLookAt.substr(0, pos));
}

static GIOChannel* set_up_io_channel (gint fd, GIOCondition cond, GIOFunc func, gpointer data){
  #ifdef G_OS_UNIX
  GIOChannel* ioc = g_io_channel_unix_new(fd); /* set up handler for data */
  #endif
  #if G_OS_WIN32
  GIOChannel* ioc = g_io_channel_win32_new_fd(fd); /* set up handler for data */
  #endif
  /* Set IOChannel encoding to none to make it fit for binary data */
  g_io_channel_set_encoding (ioc, NULL, NULL);
  g_io_channel_set_buffered (ioc, FALSE);
  /* Tell the io channel to close the file descriptor when the io channel gets destroyed */
  g_io_channel_set_close_on_unref (ioc, TRUE);
  /* g_io_add_watch() adds its own reference, which will be dropped when the watch source
   * is removed from the main loop (which happens when we return FALSE from the callback) */
  g_io_add_watch (ioc, cond, func, data);
  return ioc;
}

/*############################################################################################################################*/

void runFate (GtkWidget* pButton, gpointer data){ //PASOK
  GdkRGBA color1, color2;
  gdk_rgba_parse (&color1,"black");
  gdk_rgba_parse (&color2,"white");

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //pTable
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  ppList = g_list_reverse(ppList);						//pButton1
  ppList = g_list_next(ppList); 						//pEntry1;
  string simul_dir = (string)gtk_entry_get_text(GTK_ENTRY(ppList->data));
  string param_dir = simul_dir+delimiter+"PARAM_SIMUL"+delimiter;
  boost::filesystem::path path_param(param_dir.c_str());

  ppList = g_list_next(ppList); 						//pButton2;
  ppList = g_list_next(ppList); 						//pEntry2;
  string fate_file = (string)gtk_entry_get_text(GTK_ENTRY(ppList->data));
  boost::filesystem::path path_fate(fate_file.c_str());

  ppList = g_list_next(ppList); 						//pButton3;
  ppList = g_list_next(ppList); 						//pButton4;
gtk_widget_set_sensitive(GTK_WIDGET(ppList->data),TRUE);

  pList = g_list_next(pList); 							//box_h
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data)); 		//pFrame1
  ppList = g_list_next(pList);							//pFrame2
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//scrollbar2
  GList* pScroll2 = ppList;
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//viewport
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//textview
  gtk_widget_override_background_color(GTK_WIDGET(ppList->data), GTK_STATE_FLAG_NORMAL, &color1);
  gtk_widget_override_color(GTK_WIDGET(ppList->data), GTK_STATE_FLAG_NORMAL, &color2);
  gtk_container_set_border_width(GTK_CONTAINER(ppList->data), 10);
  gtk_text_view_set_editable(GTK_TEXT_VIEW(ppList->data), FALSE);
  gtk_text_view_set_overwrite(GTK_TEXT_VIEW(ppList->data), TRUE);

  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//scrollbar1
  GList* pScroll1 = pList;
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//textview
  gtk_widget_override_background_color(GTK_WIDGET(pList->data), GTK_STATE_FLAG_NORMAL, &color1);
  gtk_widget_override_color(GTK_WIDGET(pList->data), GTK_STATE_FLAG_NORMAL, &color2);
  gtk_container_set_border_width(GTK_CONTAINER(pList->data), 10);
  gtk_text_view_set_editable(GTK_TEXT_VIEW(pList->data), FALSE);
  gtk_text_view_set_overwrite(GTK_TEXT_VIEW(pList->data), TRUE);

  string message_str = "The <b>simulation directory</b> used is :\n\"<i>"+simul_dir+"</i>\"\n\nThe <b>FATE-HD version</b> used is :\n\"<i>"+GetLastDir(fate_file,delimiter)+"</i>\"\n\n and will be copied to :\n\"<i>"+GoBackDir(simul_dir, GetLastDir(simul_dir,delimiter))+"</i>\"";
  gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
  infoMessage(message);  

  copyFiles(fate_file.c_str(),(GoBackDir(simul_dir, GetLastDir(simul_dir,delimiter))+GetLastDir(fate_file,delimiter)).c_str(),path_fate.extension().string());
  #ifdef G_OS_UNIX
  int changeDir = chdir(GoBackDir(simul_dir, GetLastDir(simul_dir,delimiter)).c_str());
  if(changeDir==0){
    message_str = "The <b>current directory</b> has been changed to :\n\"<i>"+GoBackDir(simul_dir, GetLastDir(simul_dir,delimiter))+"</i>\"\n";
    message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
    infoMessage(message);  
  }
  #endif
  #ifdef G_OS_WIN32
  boost::filesystem::current_path(GoBackDir(simul_dir, GetLastDir(simul_dir,delimiter)).c_str());
  #endif

  /* EXECUTE FATE & PRINT OUTPUT and ERRORS ONTO THE GTK WINDOW */
  if(boost::filesystem::is_directory(path_param)){
    boost::filesystem::recursive_directory_iterator it(path_param);
    boost::filesystem::recursive_directory_iterator endit;

    /* VERSION WITH G_SPAWN_COMMAND_LINE_ASYNC */
    while(it != endit && !running){
      boost::filesystem::path path_new_param(it->path());

      if(path_new_param.extension()==".txt"){
	bool runThisOne = FALSE;
	message_str = "Do you want to run <b>FATE-HD</b> simulation for\n\"<i>"+path_new_param.string()+"</i>\" ?";
	message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
	runThisOne = carefulMessage(message);

	if(runThisOne){
          running = TRUE;
	  gchar* params[2];
	  params[0] = g_locale_to_utf8(("."+delimiter+GetLastDir(fate_file,delimiter)).c_str(), -1, NULL, NULL, NULL);
	  params[1] = g_locale_to_utf8(path_new_param.string().c_str(), -1, NULL, NULL, NULL);

	  GError* error = NULL;
	  gint stdout_fd, stderr_fd;

	  g_spawn_async_with_pipes(NULL, // use current working directory
				   params,		// the program we want to run and parameters for it
				   NULL,		// use the environment variables that are set for the parent
				   (GSpawnFlags)(G_IO_FLAG_NONBLOCK), // look for wget in $PATH
				   NULL,		// don't need a child setup function either
				   NULL,		// and therefore no child setup func data argument
				   &child_pid,	// where to store the child's PID
				   NULL,		// don't need standard input (=> will be /dev/null)
				   &stdout_fd,	// where to put wget's stdout file descriptor
				   &stderr_fd,	// where to put wget's stderr file descriptor
				   &error);

          /*#ifdef G_OS_UNIX
	  guint is_done = g_child_watch_add (child_pid, wget_exit_cb_unix, NULL);
          #endif
          #ifdef G_OS_WIN32
	  guint is_done = g_child_watch_add (child_pid, wget_exit_cb_windows, NULL);
          #endif*/

	  outchannel = set_up_io_channel(stdout_fd, (GIOCondition)(G_IO_IN|G_IO_PRI|G_IO_ERR|G_IO_HUP|G_IO_NVAL), on_pipe_output, pScroll1->data);
	  errchannel = set_up_io_channel(stderr_fd, (GIOCondition)(G_IO_IN|G_IO_PRI|G_IO_ERR|G_IO_HUP|G_IO_NVAL), on_pipe_error, pScroll2->data);

	  if (error != NULL){
	    errorMessage(error->message);  
	    g_error_free(error);
	    return;
	  }
	}
      } else {
	message_str = "The selected file does not have the right extension :\n\"<i>"+path_new_param.string()+"</i>\"";
	message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
        errorMessage(message);
      }
      ++it;
    }

  } else {
    cerr << "Error : this file or directory does not exist !" << endl;
    string message_str = "This directory does not exist !";
    gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void stopFate (GtkWidget* pButton, gpointer data){ //PASOK
  if(running){
    g_io_channel_unref(outchannel);
    g_io_channel_unref(errchannel);
    g_spawn_close_pid(child_pid);
    running = FALSE;
    child_pid = NULL;
    gtk_widget_set_sensitive(pButton,FALSE);
  } else {
	string message_str = "There is no simulation running !";
	gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
        errorMessage(message);
      }
}

#endif // CLASS_SIMUL_H
