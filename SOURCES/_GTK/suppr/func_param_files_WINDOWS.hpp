#ifndef FUNC_FILES_H
#define FUNC_FILES_H

using namespace std;

static string delimiter = "\\";

/*############################################################################################################################*/

void createFileStructure (string simul_name, unsigned nb_rep) //PASOK
{
  vector<string> list_path;
  list_path.push_back(simul_name);
  list_path.push_back(simul_name+delimiter+"RESULTS"+delimiter);
  for(unsigned i=0; i<nb_rep; i++){
    list_path.push_back(simul_name+delimiter+"RESULTS"+delimiter+IntToString(i)+delimiter);
  }
  list_path.push_back(simul_name+delimiter+"PARAM_SIMUL"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"SAVE"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"STATS"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DISP"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"ENVSUIT"+delimiter);
  list_path.push_back(simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"SUCC"+delimiter);

  string local_dir = boost::filesystem::current_path().filename().string();
  string full_path_str = boost::filesystem::current_path().string();
  unsigned index = full_path_str.find(local_dir, 0);
  full_path_str.replace(index,local_dir.size(),"");

  for(unsigned i=0; i<list_path.size(); i++){
    string dir_path_str = full_path_str+delimiter+list_path[i];
    const char* dir_path = dir_path_str.c_str();
    boost::filesystem::path dir(dir_path);
    boost::filesystem::create_directories(dir);
  }
}

unsigned copyFiles(const char* old_path, const char* new_path, const string& ext) //PASOK
{
  boost::filesystem::path file_old_path(old_path);
  boost::filesystem::path file_new_path(new_path);

  if(file_old_path.extension()==ext){
    if(boost::filesystem::exists(file_old_path)){
      if(!boost::filesystem::exists(file_new_path)){
	boost::filesystem::copy_file(file_old_path, file_new_path);
        return(1);
      } else {
	string message_str = "<b>Copy failure for the file :</b>\n\n\"<i>"+
	  (string)old_path+
	  "</i>\"\n\ninto\n\n\"<i>"+
	  (string)new_path+
	  "</i>\"\n\nThe <b>file OUTPUT</b> already exists!";
	gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
	errorMessage(message);
        return(1);
      }
    } else {
      string message_str = "<b>Copy failure for the file :</b>\n\n\"<i>"+
	(string)old_path+
	"</i>\"\n\ninto\n\n\"<i>"+
	(string)new_path+
	"</i>\"\n\nThe <b>file INPUT</b> does not exist!";
      gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
      errorMessage(message);
      return(0);
    }
  } else {
    string message_str = "<b>Copy failure for the file :</b>\n\n\"<i>"+
      (string)old_path+
      "</i>\"\n\ninto\n\n\"<i>"+
      (string)new_path+
      "</i>\"\n\nIt does not have the required extension : <b>"+ext+"</b>!";
    gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
    errorMessage(message);
    return(0);
  }

}

//vector<string> copyFilesDir(const char* old_path, string new_path, const string& ext)
//{
//     boost::filesystem::path dir_old_path(old_path);
//     vector<string> new_saved_files;
//     if(boost::filesystem::is_directory(dir_old_path)){
//	  boost::filesystem::recursive_directory_iterator it(dir_old_path);
//	  boost::filesystem::recursive_directory_iterator endit;
//
//	  unsigned compt = 0;
//	  while(it != endit){
//	       boost::filesystem::path file_new_path(new_path+it->path().filename().string());
//	       if((boost::filesystem::is_regular_file(*it)) & (it->path().extension()==ext) & (!boost::filesystem::exists(file_new_path))){
//		    boost::filesystem::copy_file(it->path(), file_new_path);
//		    new_saved_files.push_back(file_new_path.string());
//	       } else {
//		    cerr << "Error : this file might not have the required extension, or the file already exists !" << endl;
//		    string message_str = "Copy failure for the directory "+(string)old_path+"\nIt might not have the required extension,\nor the file already exists !";
//		    gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
//		    errorMessage(message);
//	       }
//	       ++it;
//	       compt++;
//	  }
//     } else {
//	  cerr << "Error : this file or directory does not exist !" << endl;
//	  string message_str = "Copy failure for the file "+(string)old_path+"\nThis directory does not exist !";
//	  gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
//	  errorMessage(message);
//     }
//
//     return new_saved_files;
//}

/*############################################################################################################################*/

void writeDispFile (string simul_name, string name_pfg, unsigned d50, unsigned d99, unsigned dLD) //PASOK
{
  const char* name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DISP"+delimiter+"DISP_"+name_pfg+".txt").c_str();
  ofstream file(name_file, ios::out | ios::trunc);
  if(file){
    file << "DISPERS_DIST " << d50 << " " << d99 << " " << dLD << endl;
    file.close();
  } else {
    cerr << "Error : cannot open the file : " << name_file << endl;
  }
}

void writeSuccFile (string simul_name, string name_pfg, unsigned matur, unsigned longev, unsigned max_abund,
                    unsigned imm_size, vector<unsigned> chg_strat_ag, bool is_alien, unsigned wide_disp, unsigned mode_disp,
                    vector<unsigned> act_germ, vector<unsigned> shade_tol_germ,
                    vector<unsigned> shade_tol_imm, vector<unsigned> shade_tol_mat,
                    unsigned seed_life_act, unsigned seed_life_dorm, unsigned seed_dorm) //PASOK
{
  const char* name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"SUCC"+delimiter+"SUCC_"+name_pfg+".txt").c_str();
  ofstream file(name_file, ios::out | ios::trunc);
  if(file){
    file << "NAME " << name_pfg << endl;
    file << "MATURITY " << matur << endl;
    file << "LONGEVITY " << longev << endl;
    file << "MAX_ABUNDANCE " << max_abund << endl;
    file << "IMM_SIZE " << imm_size << endl;
    file << "CHANG_STR_AGES";
    for(unsigned i=0; i<chg_strat_ag.size(); i++){ file << " " << chg_strat_ag[i]; }
    file << endl;
    file << "IS_ALIEN " << is_alien << endl;
    file << "WIDE_DISPERS " << wide_disp << endl;
    file << "MODE_DISPERS " << mode_disp << endl;
    file << "ACTIVE_GERM";
    for(unsigned i=0; i<act_germ.size(); i++){ file << " " << act_germ[i]; }
    file << endl;
    file << "SHADE_TOL";
    for(unsigned i=0; i<shade_tol_germ.size(); i++){ file << " " << shade_tol_germ[i]; }
    for(unsigned i=0; i<shade_tol_imm.size(); i++){ file << " " << shade_tol_imm[i]; }
    for(unsigned i=0; i<shade_tol_mat.size(); i++){ file << " " << shade_tol_mat[i]; }
    file << endl;
    file << "SEED_POOL_LIFE " << seed_life_act << " " << seed_life_dorm << endl;
    file << "SEED_DORMANCY " << seed_dorm << endl;
    file.close();
  } else {
    cerr << "Error : cannot open the file : " << name_file << endl;
  }
}

void writeSuccFile (string simul_name, string name_pfg, unsigned matur, unsigned longev, unsigned max_abund,
                    unsigned imm_size, vector<unsigned> chg_strat_ag, bool is_alien, unsigned wide_disp, unsigned mode_disp,
                    vector<unsigned> act_germ, vector<unsigned> shade_tol_germ,
                    vector<unsigned> shade_tol_imm, vector<unsigned> shade_tol_mat,
                    unsigned seed_life_act, unsigned seed_life_dorm, unsigned seed_dorm,
                    double soil_contr, vector<unsigned> soil_tol_germ,
                    vector<unsigned> soil_tol_imm, vector<unsigned> soil_tol_mat) //PASOK
{
  const char* name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"SUCC"+delimiter+"SUCC_"+name_pfg+".txt").c_str();
  ofstream file(name_file, ios::out | ios::trunc);
  if(file){
    file << "NAME " << name_pfg << endl;
    file << "MATURITY " << matur << endl;
    file << "LONGEVITY " << longev << endl;
    file << "MAX_ABUNDANCE " << max_abund << endl;
    file << "IMM_SIZE " << imm_size << endl;
    file << "CHANG_STR_AGES";
    for(unsigned i=0; i<chg_strat_ag.size(); i++){ file << " " << chg_strat_ag[i]; }
    file << endl;
    file << "IS_ALIEN " << is_alien << endl;
    file << "WIDE_DISPERS " << wide_disp << endl;
    file << "MODE_DISPERS " << mode_disp << endl;
    file << "ACTIVE_GERM";
    for(unsigned i=0; i<act_germ.size(); i++){ file << " " << act_germ[i]; }
    file << endl;
    file << "SHADE_TOL";
    for(unsigned i=0; i<shade_tol_germ.size(); i++){ file << " " << shade_tol_germ[i]; }
    for(unsigned i=0; i<shade_tol_imm.size(); i++){ file << " " << shade_tol_imm[i]; }
    for(unsigned i=0; i<shade_tol_mat.size(); i++){ file << " " << shade_tol_mat[i]; }
    file << endl;
    file << "SEED_POOL_LIFE " << seed_life_act << " " << seed_life_dorm << endl;
    file << "SEED_DORMANCY " << seed_dorm << endl;

    file << "SOIL_CONTRIB " << soil_contr << endl;
    file << "SOIL_TOL";
    for(unsigned i=0; i<soil_tol_germ.size(); i++){ file << " " << soil_tol_germ[i]; }
    for(unsigned i=0; i<soil_tol_imm.size(); i++){ file << " " << soil_tol_imm[i]; }
    for(unsigned i=0; i<soil_tol_mat.size(); i++){ file << " " << soil_tol_mat[i]; }
    file << endl;
    file.close();
  } else {
    cerr << "Error : cannot open the file : " << name_file << endl;
  }
}

void writePertFile (string typeFile, string simul_name, unsigned ind_pfg, string name_pfg, unsigned nb_dist,
                    unsigned nb_resp_stages, vector<unsigned> prop_kill_prop, vector< vector<unsigned> > break_ages,
                    vector< vector<unsigned> > respr_ages, vector< vector< vector<unsigned> > > prop_kill_ind,
                    vector< vector< vector<unsigned> > > prop_respr_ind, vector<unsigned> prop_act_seed) //PASOK
{
  const char* name_file;
  if(strcmp(typeFile.c_str(),"dist")==0){ name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"DIST_"+name_pfg+".txt").c_str();
  } else if(strcmp(typeFile.c_str(),"fire")==0){ name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"FIRE_"+name_pfg+".txt").c_str();
  } else if(strcmp(typeFile.c_str(),"drought")==0){ name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"DROUGHT_"+name_pfg+".txt").c_str();
  }
  ofstream file(name_file, ios::out | ios::trunc);
  if(file){
    file << "PROP_KILLED";
    for(unsigned i=0; i<nb_dist; i++){
      file << " " << prop_kill_prop[i];
    }
    file << endl;
    file << "BREAK_AGE";
    for(unsigned i=0; i<nb_dist; i++){
      for(unsigned j=0; j<nb_resp_stages; j++){
	file << " " << break_ages[i][j];
      }
    }
    file << endl;
    file << "RESPR_AGE";
    for(unsigned i=0; i<nb_dist; i++){
      for(unsigned j=0; j<nb_resp_stages; j++){
	file << " " << respr_ages[i][j];
      }
    }
    file << endl;
    file << "FATES";
    for(unsigned i=0; i<nb_dist; i++){
      for(unsigned j=0; j<nb_resp_stages; j++){
	file << " " << prop_kill_ind[i][ind_pfg][j];
	file << " " << prop_respr_ind[i][ind_pfg][j];
      }
    }
    file << endl;
    file << "ACTIVATED_SEED";
    for(unsigned i=0; i<nb_dist; i++){
      file << " " << prop_act_seed[i];
    }
    file << endl;
    file.close();
  } else {
    cerr << "Error : cannot open the file : " << name_file << endl;
  }
}

/*void writeDistFile (string simul_name, unsigned ind_pfg, string name_pfg, unsigned nb_dist,
  unsigned nb_resp_stages, vector<unsigned> prop_kill_prop, vector< vector<unsigned> > break_ages,
  vector< vector<unsigned> > respr_ages, vector< vector< vector<unsigned> > > prop_kill_ind,
  vector< vector< vector<unsigned> > > prop_respr_ind, vector<unsigned> prop_act_seed)
  {
  const char* name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"DIST_"+name_pfg+".txt").c_str();
  ofstream file(name_file, ios::out | ios::trunc);
  if(file){
  file << "PROP_KILLED";
  for(unsigned i=0; i<nb_dist; i++){
  file << " " << prop_kill_prop[i];
  }
  file << endl;
  file << "BREAK_AGE";
  for(unsigned i=0; i<nb_dist; i++){
  for(unsigned j=0; j<nb_resp_stages; j++){
  file << " " << break_ages[i][j];
  }
  }
  file << endl;
  file << "RESPR_AGE";
  for(unsigned i=0; i<nb_dist; i++){
  for(unsigned j=0; j<nb_resp_stages; j++){
  file << " " << respr_ages[i][j];
  }
  }
  file << endl;
  file << "FATES";
  for(unsigned i=0; i<nb_dist; i++){
  for(unsigned j=0; j<nb_resp_stages; j++){
  file << " " << prop_kill_ind[i][ind_pfg][j];
  file << " " << prop_respr_ind[i][ind_pfg][j];
  }
  }
  file << endl;
  file << "ACTIVATED_SEED";
  for(unsigned i=0; i<nb_dist; i++){
  file << " " << prop_act_seed[i];
  }
  file << endl;
  file.close();
  } else {
  cerr << "Error : cannot open the file : " << name_file << endl;
  }
  }

  void writeFireFile (string simul_name, unsigned ind_pfg, string name_pfg, unsigned nb_dist,
  unsigned nb_resp_stages, vector<unsigned> prop_kill_prop, vector< vector<unsigned> > break_ages,
  vector< vector<unsigned> > respr_ages, vector< vector< vector<unsigned> > > prop_kill_ind,
  vector< vector< vector<unsigned> > > prop_respr_ind, vector<unsigned> prop_act_seed)
  {
  const char* name_file = (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"FIRE_"+name_pfg+".txt").c_str();
  ofstream file(name_file, ios::out | ios::trunc);
  if(file){
  file << "PROP_KILLED";
  for(unsigned i=0; i<nb_dist; i++){
  file << " " << prop_kill_prop[i];
  }
  file << endl;
  file << "BREAK_AGE";
  for(unsigned i=0; i<nb_dist; i++){
  for(unsigned j=0; j<nb_resp_stages; j++){
  file << " " << break_ages[i][j];
  }
  }
  file << endl;
  file << "RESPR_AGE";
  for(unsigned i=0; i<nb_dist; i++){
  for(unsigned j=0; j<nb_resp_stages; j++){
  file << " " << respr_ages[i][j];
  }
  }
  file << endl;
  file << "FATES";
  for(unsigned i=0; i<nb_dist; i++){
  for(unsigned j=0; j<nb_resp_stages; j++){
  file << " " << prop_kill_ind[i][ind_pfg][j];
  file << " " << prop_respr_ind[i][ind_pfg][j];
  }
  }
  file << endl;
  file << "ACTIVATED_SEED";
  for(unsigned i=0; i<nb_dist; i++){
  file << " " << prop_act_seed[i];
  }
  file << endl;
  file.close();
  } else {
  cerr << "Error : cannot open the file : " << name_file << endl;
  }
  }

  void writeDroughtFile (string simul_name, unsigned ind_pfg, string name_pfg, unsigned nb_dist,
  unsigned nb_resp_stages, vector<unsigned> prop_kill_prop, vector< vector<unsigned> > break_ages,
  vector< vector<unsigned> > respr_ages, vector< vector< vector<unsigned> > > prop_kill_ind,
  vector< vector< vector<unsigned> > > prop_respr_ind, vector<unsigned> prop_act_seed)
  {

  }*/

/*############################################################################################################################*/

void writeGlobalParamFile (string name_file, unsigned nb_cpu, string succ_mod, unsigned nb_FG, unsigned nb_strata,
			   unsigned seed_timestep, unsigned seed_dur, unsigned simul_time,
			   bool do_dist, unsigned nb_dist, unsigned nb_subdist, vector<unsigned> freq_dist,
			   bool do_soil, double soil_default, unsigned nb_soil_cat, vector<double> soil_cat_thresh,
			   bool do_fire, unsigned nb_fires, unsigned nb_subfires, vector<unsigned> freq_fires,
			   unsigned init, unsigned neigh, unsigned prop, unsigned quota, unsigned nb_start_fires,
			   vector<unsigned> prev_data, vector<unsigned> cc_extent, vector<double> fire_prob,
			   unsigned flamm_max, vector<double> init_logis, vector<double> prop_logis, unsigned nb_clim_var,
			   bool do_drought, unsigned nb_subdrought, vector<string> drought_chrono,
			   bool do_hab, unsigned nb_hab,
			   bool do_aliens, vector<unsigned> freq_aliens) //PASOK
{
  ofstream file(name_file.c_str(), ios::out | ios::trunc);
  if(file){
    file << "NB_CPUS " << nb_cpu << endl;
    if(strcmp(succ_mod.c_str(),"fate")==0){
      file << "SUCC_MOD " << 1 << endl;
    } else if(strcmp(succ_mod.c_str(),"fateh")==0){
      file << "SUCC_MOD " << 2 << endl;
    }
    file << "NB_FG " << nb_FG << endl;
    file << "NB_STRATUM " << nb_strata << endl;
    file << "SEEDING_TIMESTEP " << seed_timestep << endl;
    file << "SEEDING_DURATION " << seed_dur << endl;
    file << "SIMULATION_TIME " << simul_time << endl;
    file << "ENVSUIT_OPTION 1" << endl;

    if(do_dist){
      file << "DO_DISTURBANCES 1" << endl;
      file << "NB_DISTURBANCES " << nb_dist << endl;
      file << "NB_SUBDISTURBANCES " << nb_subdist << endl;
      file << "FREQ_DISTURBANCES";
      for(unsigned i=0; i<freq_dist.size(); i++){
	file << " " << freq_dist[i];
      }
      file << endl;
    } else {
      file << "DO_DISTURBANCES 0" << endl;
    }

    if(do_soil){
      file << "DO_SOIL_COMPETITION 1" << endl;
      file << "SOIL_DEFAULT_VALUE " << soil_default << endl;
      file << "NB_SOIL_CATEGORIES " << nb_soil_cat << endl;
      file << "SOIL_CATEGORIES_TRESHOLDS";
      for(unsigned i=0; i<soil_cat_thresh.size(); i++){
	file << " " << soil_cat_thresh[i];
      }
      file << endl;
    } else {
      file << "DO_SOIL_COMPETITION 0" << endl;
    }

    if(do_fire){
      file << "DO_FIRE_DISTURBANCES 1" << endl;
      file << "NB_FIRE_DISTURBANCES " << nb_fires << endl;
      file << "NB_SUBFIRES " << nb_subfires << endl;
      file << "FREQ_FIRES";
      for(unsigned i=0; i<freq_fires.size(); i++){
	file << " " << freq_fires[i];
      }
      file << endl;
      file << "INIT_OPTION " << init << endl;
      file << "NEIGH_OPTION " << neigh << endl;
      file << "PROP_OPTION " << prop << endl;
      file << "QUOTA_OPTION " << quota << endl;
      file << "NB_FIRES " << nb_start_fires << endl;
      file << "PREV_DATA";
      for(unsigned i=0; i<prev_data.size(); i++){
	file << " " << prev_data[i];
      }
      file << endl;
      file << "COOKIE_CUTTER_EXTENT";
      for(unsigned i=0; i<cc_extent.size(); i++){
	file << " " << cc_extent[i];
      }
      file << endl;
      file << "FIRE_PROB";
      for(unsigned i=0; i<fire_prob.size(); i++){
	file << " " << fire_prob[i];
      }
      file << endl;
      file << "FLAMM_MAX " << flamm_max << endl;
      file << "LOGIS_INIT";
      for(unsigned i=0; i<init_logis.size(); i++){
	file << " " << init_logis[i];
      }
      file << endl;
      file << "LOGIS_SPREAD";
      for(unsigned i=0; i<prop_logis.size(); i++){
	file << " " << prop_logis[i];
      }
      file << endl;
      file << "NB_CLIM_VAR " << nb_clim_var << endl;
    } else {
      file << "DO_FIRE_DISTURBANCES 0" << endl;
    }
    
    if(do_drought){
      file << "DO_DROUGHT_DISTURBANCES 1" << endl;
      file << "NB_SUBDROUGHT " << nb_subdrought << endl;
      if(strcmp(drought_chrono[0].c_str(),"prev")==0){
	file << "CHRONO_POST_DROUGHT 1" << endl;
      } else if(strcmp(drought_chrono[0].c_str(),"post")==0){
	file << "CHRONO_POST_DROUGHT 2" << endl;
      } else {
	file << "CHRONO_POST_DROUGHT 1" << endl;
      }
      if(strcmp(drought_chrono[1].c_str(),"prev")==0){
	file << "CHRONO_CURR_DROUGHT 1" << endl;
      } else if(strcmp(drought_chrono[1].c_str(),"post")==0){
	file << "CHRONO_CURR_DROUGHT 2" << endl;
      } else {
	file << "CHRONO_CURR_DROUGHT 2" << endl;
      }
    } else {
      file << "DO_DROUGHT_DISTURBANCES 0" << endl;
    }

    if(do_hab){
      file << "DO_HAB_STABILITY 1" << endl;
      file << "NB_HABITATS " << nb_hab << endl;
    } else {
      file << "DO_HAB_STABILITY 0" << endl;
    }

    if(do_aliens){
      file << "DO_ALIENS_DISTURBANCE 1" << endl;
      file << "FREQ_ALIENS";
      for(unsigned i=0; i<freq_aliens.size(); i++){
	file << " " << freq_aliens[i];
      }
      file << endl;
    } else {
      file << "DO_ALIENS_DISTURBANCE 0" << endl;
    }

    file.close();
  } else {
    cerr << "Error : cannot open the file : " << name_file << endl;
  }
}

void writeNamespaceConstantsFile () //PASOK
{
  ofstream file("Namespace_constants.txt", ios::out | ios::trunc);
  if(file){
    file << "GLOBAL_LOW_ABUND 3000000" << endl;
    file << "GLOBAL_MEDIUM_ABUND 7000000" << endl;
    file << "GLOBAL_HIGH_ABUND 10000000" << endl;
    file << "GLOBAL_LOW_RESOURCES_TRESH 9000000" << endl;
    file << "GLOBAL_MEDIUM_RESOURCES_TRESH 6000000" << endl;
    file << "GLOBAL_FULL_SOIL_COVERAGE 9000000" << endl;
    file << "GLOBAL_MAX_BY_COHORT 7000000" << endl;

    file.close();
  } else {
    cerr << "Error : cannot open the file : Namespace_constants.txt" << endl;
  }
}


void writeParamSimulFile (string simul_name, bool savedState, bool savedArrays, bool savedObjects, unsigned nb_rep, string mask_file,
			  vector<string> pfg_names, vector< vector<string> > clim_mask_change,
			  vector<string> dist_names, vector< vector<string> > dist_mask_change,
			  vector<string> fire_names, vector< vector<string> > fire_mask_change, vector< vector<string> > fire_freq_change,
			  vector<string> climdata_mask, vector< vector<string> > climdata_mask_change,
			  vector<string> moist_mask_change,
			  vector<string> hab_BL_stats, vector<string> hab_mask,
                          vector< vector<string> > aliens_mask_change, vector< vector<string> > aliens_freq_change,
			  bool do_dist, bool do_fire, bool do_drought, bool do_hab, bool do_aliens,
			  bool envChange, bool distChange, bool fireChange, bool fireFreqChange,
                          bool climData, bool climDataChange, bool droughtChange, bool aliensChange, bool aliensFreqChange) //PASOK
{
  for(unsigned rep=0; rep<nb_rep; rep++){
    const char* name_file = (simul_name+delimiter+"PARAM_SIMUL"+delimiter+"paramSimul_"+IntToString(rep)+".txt").c_str();
    ofstream file(name_file, ios::out | ios::trunc);
    if(file){
      file << "--GLOBAL_PARAMS--" << endl;
      //file << (simul_name+delimiter+"DATA"+delimiter+"Global_parameters_"+IntToString(rep)+".txt") << endl;
      file << (simul_name+delimiter+"DATA"+delimiter+"Global_parameters.txt") << endl;
      file << "--NAMESPACE_CONSTANTS--" << endl;
      file << (simul_name+delimiter+"DATA"+delimiter+"Namespace_constants.txt") << endl;
      file << "--SAVE_DIR--" << endl;
      file << (simul_name+delimiter+"RESULTS"+delimiter+IntToString(rep)) << endl;
      if(savedState){
	file << "--SAVED_STATE--" << endl;
	file << (simul_name+delimiter+"RESULTS"+delimiter+"prevSavedState.sav") << endl;
      }
      if(savedArrays){
	file << "--ARRAYS_SAVING_YEARS--" << endl;
	file << (simul_name+delimiter+"DATA"+delimiter+"SAVE"+delimiter+"arraysSavingYears.txt") << endl;
      }
      if(savedObjects){
	file << "--OBJECTS_SAVING_YEARS--" << endl;
	file << (simul_name+delimiter+"DATA"+delimiter+"SAVE"+delimiter+"objectsSavingYears.txt") << endl;
      }
      file << "--MASK--" << endl;
      file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+"maskSimulation.asc") << endl;
      file << "--PFG_LIFE_HISTORY_PARAMS--" << endl;
      for(unsigned i=0; i<pfg_names.size(); i++){
	file << (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"SUCC"+delimiter+"SUCC_"+pfg_names[i]+".txt") << endl;
      }
      file << "--PFG_DISPERSAL_PARAMS--" << endl;
      for(unsigned i=0; i<pfg_names.size(); i++){
	file << (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DISP"+delimiter+"DISP_"+pfg_names[i]+".txt") << endl;
      }
      /*file << "--PFG_CONDINIT--" << endl;
	for(unsigned i=0; i<nb_rep; i++){
	file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+"maskSimulation.asc") << endl;
	}*/
      file << "--PFG_ENVSUIT--" << endl;
      for(unsigned i=0; i<pfg_names.size(); i++){
	file << (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"ENVSUIT"+delimiter+"HS_"+pfg_names[i]+".asc") << endl;
      }
      if(envChange){
	file << "--CLIM_CHANGE_TIME--" << endl;
	file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"environmental_changing_times.txt") << endl;
	file << "--CLIM_CHANGE_MASK--" << endl;
	for(unsigned i=0; i<pfg_names.size(); i++){
	  for(unsigned j=0; j<clim_mask_change[i].size(); j++){
	    file << clim_mask_change[i][j] << endl;
	  }
	}
      }

      if(do_dist){
	file << "--PFG_DISTURBANCES_PARAMS--" << endl;
	for(unsigned i=0; i<pfg_names.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"DIST_"+pfg_names[i]+".txt") << endl;
	}
	file << "--DIST_MASK--" << endl;
	for(unsigned i=0; i<dist_names.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+dist_names[i]+".asc") << endl;
	}
	if(distChange){
	  file << "--DIST_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"disturbances_changing_times.txt") << endl;
	  file << "--DIST_CHANGE_MASK--" << endl;
	  for(unsigned i=0; i<dist_mask_change.size(); i++){
	    for(unsigned j=0; j<dist_mask_change[i].size(); j++){
	      file << dist_mask_change[i][j] << endl;
	    }
	  }
	}
      }
      if(do_fire){
	file << "--PFG_FIRES_PARAMS--" << endl;
	for(unsigned i=0; i<pfg_names.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"FIRE_"+pfg_names[i]+".txt") << endl;
	}
	file << "--FIRE_MASK--" << endl;
	for(unsigned i=0; i<fire_names.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+fire_names[i]+".asc") << endl;
	}
	if(fireChange){
	  file << "--FIRE_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"fires_changing_times.txt") << endl;
	  file << "--FIRE_CHANGE_MASK--" << endl;
	  for(unsigned i=0; i<fire_mask_change.size(); i++){
	    for(unsigned j=0; j<fire_mask_change[i].size(); j++){
	      file << fire_mask_change[i][j] << endl;
	    }
	  }
	}
	if(fireFreqChange){
	  file << "--FIRE_FREQ_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"fires_freq_changing_times.txt") << endl;
	  file << "--FIRE_CHANGE_FREQUENCIES--" << endl;
	  for(unsigned i=0; i<fire_freq_change.size(); i++){
	    for(unsigned j=0; j<fire_freq_change[i].size(); j++){
	      file << fire_freq_change[i][j] << endl;
	    }
	  }
	}
	if(climData){
	  file << "--CLIM_DATA_MASK--" << endl;
	  for(unsigned i=0; i<climdata_mask.size(); i++){
	    file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+climdata_mask[i]+".txt") << endl;
	  }
	}
	if(climDataChange){
	  file << "--CLIM_DATA_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"climaticData_changing_times.txt") << endl;
	  file << "--CLIM_DATA_CHANGE_MASK--" << endl;
	  for(unsigned i=0; i<climdata_mask_change.size(); i++){
	    for(unsigned j=0; j<climdata_mask_change[i].size(); j++){
	      file << climdata_mask_change[i][j] << endl;
	    }
	  }
	}
      }
      if(do_drought){
	file << "--PFG_DROUGHT_PARAMS--" << endl;
	for(unsigned i=0; i<pfg_names.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"DIST"+delimiter+"DROUGHT_"+pfg_names[i]+".txt") << endl;
	}
	file << "--MOIST_MASK--" << endl;
	file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+"moistureIndex.asc") << endl;
	if(droughtChange){
	  file << "--MOIST_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"moistureIndex_changing_times.txt") << endl;
	  file << "--MOIST_CHANGE_MASK--" << endl;
	  for(unsigned i=0; i<moist_mask_change.size(); i++){
	    file << moist_mask_change[i] << endl;
	  }
	}
      }
      if(do_hab){
	file << "--HAB_BASELINE_STATS--" << endl;
	for(unsigned i=0; i<hab_BL_stats.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+hab_BL_stats[i]+".txt") << endl;
	}
	file << "--HAB_MASK--" << endl;
	for(unsigned i=0; i<hab_mask.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+hab_mask[i]+".txt") << endl;
	}
      }
      if(do_aliens){
	file << "--ALIENS_MASK--" << endl;
	for(unsigned i=0; i<pfg_names.size(); i++){
	  file << (simul_name+delimiter+"DATA"+delimiter+"MASK"+delimiter+"Alien_"+pfg_names[i]+".asc") << endl;
	}
	if(aliensChange){
	  file << "--ALIENS_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"aliens_changing_times.txt") << endl;
	  file << "--ALIENS_CHANGE_MASK--" << endl;
	  for(unsigned i=0; i<aliens_mask_change.size(); i++){
	    for(unsigned j=0; j<aliens_mask_change[i].size(); j++){
	      file << aliens_mask_change[i][j] << endl;
	    }
	  }
	}
	if(aliensFreqChange){
	  file << "--ALIENS_FREQ_CHANGE_TIME--" << endl;
	  file << (simul_name+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"aliens_freq_changing_times.txt") << endl;
	  file << "--ALIENS_CHANGE_FREQUENCIES--" << endl;
	  for(unsigned i=0; i<aliens_freq_change.size(); i++){
	    for(unsigned j=0; j<aliens_freq_change[i].size(); j++){
	      file << aliens_freq_change[i][j] << endl;
	    }
	  }
	}
      }

      file << "--END_OF_FILE--" << endl;
      file.close();
    } else {
      cerr << "Error : cannot open the file : " << name_file << endl;
    }
  }
}


#endif // FUNC_FILES_H
