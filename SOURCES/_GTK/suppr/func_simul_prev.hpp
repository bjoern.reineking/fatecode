#ifndef CLASS_SIMUL_H
#define CLASS_SIMUL_H

using namespace std;

/*############################################################################################################################*/

void scroll_to_end(GtkWidget* textview){
  GtkTextBuffer *buffer;
  GtkTextIter enditer;

  buffer = gtk_text_view_get_buffer (GTK_TEXT_VIEW(textview));
  gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER (buffer), &enditer);
  gtk_text_view_scroll_to_iter(GTK_TEXT_VIEW(textview), &enditer, 0, TRUE, 0.0, 0.0);
}

void addMsgInfo(GtkWidget* textview, gchar *buf){
  GtkTextBuffer *buffer;
  GtkTextIter  enditer;

  buffer = gtk_text_view_get_buffer(GTK_TEXT_VIEW(textview));
  gtk_text_buffer_get_end_iter (GTK_TEXT_BUFFER (buffer), &enditer);
  gtk_text_buffer_insert(buffer, &enditer,buf,-1);
  g_idle_add ((GSourceFunc)scroll_to_end, NULL);
}

gboolean on_pipe_output (GIOChannel *source, GIOCondition condition, gpointer data){
  gchar buffer[BUFSIZ+1];
  gsize *bytes_read;
  g_io_channel_read_chars(source,buffer,BUFSIZ,bytes_read,NULL);
        /*GIOStatus ret;
        GError *err = NULL;
        gchar *msg;
        gsize len;

        if (condition & G_IO_HUP) g_error ("Read end of pipe died!\n");
        ret = g_io_channel_read_line (source, &msg, &len, NULL, &err);*/

  addMsgInfo((GtkWidget*) data, buffer);
  //gtk_widget_show_all((GtkWidget*) data);
}

void run_program(gchar** argv, GtkWidget** data){
  GError *error = NULL;
  GPid	 child_pid;
  gint	 stdout_fd;
  gint	 stderr_fd;

  g_spawn_async_with_pipes(NULL, // use current working directory
			   argv,		// the program we want to run and parameters for it
			   NULL,		// use the environment variables that are set for the parent
			   (GSpawnFlags)(G_IO_FLAG_NONBLOCK|G_SPAWN_DO_NOT_REAP_CHILD),
			   // look for wget in $PATH | we'll check the exit status ourself
			   NULL,		// don't need a child setup function either
			   NULL,		// and therefore no child setup func data argument
			   &child_pid,	// where to store the child's PID
			   NULL,		// don't need standard input (=> will be /dev/null)
			   &stdout_fd,	// where to put wget's stdout file descriptor
			   &stderr_fd,	// where to put wget's stderr file descriptor
			   &error);

  if (error != NULL){
    errorMessage(error->message);  
    g_error_free(error);
    return;
  }

  GIOChannel* outchannel = g_io_channel_unix_new(stdout_fd);
  //GIOChannel* outchannel = g_io_channel_new_file("/dev/stdout","r",NULL);
  g_io_add_watch (outchannel, (GIOCondition)(G_IO_IN), on_pipe_output, (gpointer) data[0]);
  GIOChannel* errchannel = g_io_channel_unix_new(stderr_fd);
  g_io_add_watch (errchannel, (GIOCondition)(G_IO_ERR), on_pipe_output, (gpointer) data[1]);
}


/*############################################################################################################################*/

string GetLastDir(string toLookAt, string delim){
  size_t pos = 0;
  string token;
  while ((pos = toLookAt.find(delim)) != string::npos) {
    token = toLookAt.substr(0, pos);
    toLookAt.erase(0, pos + delim.length());
  }
  return(toLookAt);
}

string GoBackDir(string toLookAt, string lastDir){
  size_t pos = toLookAt.find(lastDir);
  return(toLookAt.substr(0, pos));
}

void runFate (GtkWidget* pButton, gpointer data){ //PASOK
  GdkRGBA color;
  gdk_rgba_parse (&color,"black");
  string delimiter = "/";
  gchar *output_txt, *error_txt;       
  GError *error = NULL;

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //pTable
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  ppList = g_list_reverse(ppList);						//pButton1
  ppList = g_list_next(ppList); 						//pEntry1;
  string simul_dir = (string)gtk_entry_get_text(GTK_ENTRY(ppList->data));
  string param_dir = simul_dir+"/PARAM_SIMUL/";
  boost::filesystem::path path_param(param_dir.c_str());

  ppList = g_list_next(ppList); 						//pButton2;
  ppList = g_list_next(ppList); 						//pEntry2;
  string fate_file = (string)gtk_entry_get_text(GTK_ENTRY(ppList->data));
  boost::filesystem::path path_fate(fate_file.c_str());

  pList = g_list_next(pList); 							//box_h
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data)); 		//pFrame1
  ppList = g_list_next(pList);							//pFrame2
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//scrollbar2
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//viewport
  gtk_widget_override_background_color(GTK_WIDGET(ppList->data), GTK_STATE_FLAG_NORMAL, &color);

  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  gtk_container_set_border_width(GTK_CONTAINER(ppList->data), 10);

  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//scrollbar1
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//viewport
  gtk_widget_override_background_color(GTK_WIDGET(pList->data), GTK_STATE_FLAG_NORMAL, &color);
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  gtk_container_set_border_width(GTK_CONTAINER(pList->data), 10);

  string message_str = "The <b>simulation directory</b> used is :\n\"<i>"+simul_dir+"</i>\"\n\nThe <b>FATE-HD version</b> used is :\n\"<i>"+GetLastDir(fate_file, "/")+"</i>\"\n\n and has been copied to :\n\"<i>"+GoBackDir(simul_dir, GetLastDir(simul_dir, "/"))+"</i>\"";
  gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
  infoMessage(message);  

  copyFiles(fate_file.c_str(),(GoBackDir(simul_dir, GetLastDir(simul_dir, "/"))+GetLastDir(fate_file, "/")).c_str(),path_fate.extension().string());

  chdir(GoBackDir(simul_dir, GetLastDir(simul_dir, "/")).c_str());

  /* EXECUTE FATE & PRINT OUTPUT and ERRORS ONTO THE GTK WINDOW */
  if(boost::filesystem::is_directory(path_param)){
    bool runAll = TRUE;

    message_str = "Do you want to run <b>all</b> \"<i>paramSimul</i>\"\nfiles found in "+GetLastDir(simul_dir, "/")+"/PARAM_SIMUL/ ?";
    message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
    runAll = carefulMessage(message);

    boost::filesystem::recursive_directory_iterator it(path_param);
    boost::filesystem::recursive_directory_iterator endit;
    boost::filesystem::path path_new_param(it->path());


    //#####################################


    GtkWidget* view_output = gtk_text_view_new();
    GtkWidget* view_error = gtk_text_view_new();

    //ostringstream outs;
    //cout.rdbuf(outs.rdbuf());

    GtkTextBuffer* buffer_output = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view_output));
    GtkTextBuffer* buffer_error = gtk_text_view_get_buffer(GTK_TEXT_VIEW(view_error));

    GtkWidget* view_array[2];
    view_array[0] = view_output;
    view_array[1] = view_error;

    gtk_box_pack_start(GTK_BOX(pList->data), view_output, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(ppList->data), view_error, TRUE, TRUE, 0);
    gtk_widget_show_all((GtkWidget*) data);

    /*const gchar* command = ("./"+GetLastDir(fate_file, "/")+" "+path_new_param.string()).c_str();

    gchar* params[2];
    params[0] = g_locale_to_utf8(("./"+GetLastDir(fate_file, "/")).c_str(), -1, NULL, NULL, NULL);
    params[1] = g_locale_to_utf8(path_new_param.string().c_str(), -1, NULL, NULL, NULL);
        
    run_program(params,view_array);*/

    //#####################################

    /* VERSION WITH G_SPAWN_COMMAND_LINE_SYNC */
    /*if(runAll){
      while(it != endit){
      boost::filesystem::path path_new_param(it->path());
      const gchar* command = ("./"+GetLastDir(fate_file, "/")+" "+path_new_param.string()).c_str();

      /* execute command */
    /*g_spawn_command_line_sync(command, &output_txt, &error_txt, NULL, &error);
    //g_spawn_command_line_async(command, &error);

    if (error != NULL){
    errorMessage(error->message);  
    g_error_free(error);
    return;
    }

    /* send the text to the output */
    /*gtk_box_pack_start(GTK_BOX(pList->data), gtk_label_new(output_txt), TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(ppList->data), gtk_label_new(error_txt), TRUE, TRUE, 0);
      gtk_widget_show_all((GtkWidget*) data);

      /* free the output and error string */
    /*g_free(output_txt);
      g_free(error_txt);
      ++it;
      }
      } else {
      bool runThisOne = FALSE;
      while(it != endit && !runThisOne){
      boost::filesystem::path path_new_param(it->path());
      message_str = "Do you want to run FATE-HD with\n\"<i>"+path_new_param.string()+"</i>\"\n\n?";
      message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
      runThisOne = carefulMessage(message);
      if(!runThisOne){ ++it; }
      }

      if(runThisOne){
      const gchar* command = ("./"+GetLastDir(fate_file, "/")+" "+path_new_param.string()).c_str();

      /* execute command */
    /*g_spawn_command_line_sync(command, &output_txt, &error_txt, NULL, &error);  
      if (error != NULL){
      errorMessage(error->message);  
      g_error_free(error);
      return;
      }

      /* send the text to the output */
    /*gtk_box_pack_start(GTK_BOX(pList->data), gtk_label_new(output_txt), TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(ppList->data), gtk_label_new(error_txt), TRUE, TRUE, 0);
      gtk_widget_show_all((GtkWidget*) data);

      /* free the output and error string */
    /*g_free(output_txt);
      g_free(error_txt);
      }

      }

    //#####################################
      /* PREVIOUS VERSION WITH POPEN */
    //while(it != endit){
      //boost::filesystem::path path_new_param(it->path());
      const gchar* command = ("./"+GetLastDir(fate_file, "/")+" "+path_new_param.string()+" > FATE_stdout.txt 2>FATE_stderr.txt").c_str();
cout << "Command to be ran : " << command << endl;
      FILE* pipe = popen(command,"r");
cout << "Command launched ! " << endl;

      /*string temp, line;
      gchar* res;
      GtkWidget* pLabel;

      // STDOUT FILE
      ifstream file("FATE_stdout.txt", ios::in);
      while (getline(file,line)){
      temp = "<span font_desc=\"Times New Roman bold 8\" foreground=\"#FFFFFF\">"+line+"</span>";
      res = g_locale_to_utf8(temp.c_str(),-1,NULL,NULL,NULL);
      pLabel = gtk_label_new(NULL);
      gtk_label_set_markup(GTK_LABEL(pLabel),res);
      gtk_misc_set_alignment(GTK_MISC(pLabel), 0, 0);
      gtk_box_pack_start(GTK_BOX(pList->data), pLabel, TRUE, TRUE, 0);
	gtk_widget_show_all((GtkWidget*) data);
      }*/


  GIOChannel* outchannel = g_io_channel_new_file("FATE_stdout.txt","r",NULL);
cout << "outchannel created" << endl;
  g_io_add_watch (outchannel, (GIOCondition)(G_IO_IN), on_pipe_output, (gpointer) view_output);
cout << "first watch created" << endl;
  GIOChannel* errchannel = g_io_channel_new_file("FATE_stderr.txt","r",NULL);
cout << "errchannel created" << endl;
  g_io_add_watch (errchannel, (GIOCondition)(G_IO_ERR), on_pipe_output, (gpointer) view_error);
cout << "second watch created" << endl;
      //FILE* pipe = popen(("./FATE_exe "+path_new_param.string()+" 2>/tmp/tmpfileFATE.txt").c_str(), "r");
      /*if (pipe){
      char buffer[128];
      std::string result = "";
      while(!feof(pipe)) {
      if(fgets(buffer, 128, pipe) != NULL)
      result += buffer;
      }

      stringstream ss(result);
      string temp, line;
      gchar* res;
      GtkWidget* pLabel;
      GtkWidget* pLabel2;
      size_t index;

      temp = "<span font_desc=\"Times New Roman bold 8\" foreground=\"#FFFFFF\" >\n###############################################\n</span>";
      res = g_locale_to_utf8(temp.c_str(), -1, NULL, NULL, NULL);
      pLabel = gtk_label_new(NULL);
      gtk_label_set_markup(GTK_LABEL(pLabel),res);
      pLabel2 = gtk_label_new(NULL);
      gtk_label_set_markup(GTK_LABEL(pLabel2),res);
      gtk_misc_set_alignment(GTK_MISC(pLabel), 0, 0);
      gtk_box_pack_start(GTK_BOX(pList->data), pLabel, TRUE, TRUE, 0);
      gtk_box_pack_start(GTK_BOX(ppList->data), pLabel2, TRUE, TRUE, 0);

      // STDERR FILE
      while(getline(ss,line,'\n')){
      index = line.find("<", 0);
      if(index!=std::string::npos){ line.replace(index,1,""); }
      index = line.find(">", 0);
      if(index!=std::string::npos){ line.replace(index,1,""); }
			
      temp = "<span font_desc=\"Times New Roman bold 8\" foreground=\"#FFFFFF\" >"+line+"</span>";
      res = g_locale_to_utf8(temp.c_str(), -1, NULL, NULL, NULL);
      pLabel = gtk_label_new(NULL);
      gtk_label_set_markup(GTK_LABEL(pLabel),res);
      gtk_misc_set_alignment(GTK_MISC(pLabel), 0, 0);
      gtk_box_pack_start(GTK_BOX(pList->data), pLabel, TRUE, TRUE, 0);
      }
	
      // STDOUT FILE
      ifstream file("/tmp/tmpfileFATE.txt", ios::in);
      while (getline(file,line)){
      temp = "<span font_desc=\"Times New Roman bold 8\" foreground=\"#FFFFFF\">"+line+"</span>";
      res = g_locale_to_utf8(temp.c_str(),-1,NULL,NULL,NULL);
      pLabel = gtk_label_new(NULL);
      gtk_label_set_markup(GTK_LABEL(pLabel),res);
      gtk_misc_set_alignment(GTK_MISC(pLabel), 0, 0);
      gtk_box_pack_start(GTK_BOX(ppList->data), pLabel, TRUE, TRUE, 0);
      }
      }*/
      pclose(pipe);
      //++it;
      //}


    /*pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //pTable
      pList = g_list_next(pList); 						//box_h
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data)); 		//pFrame1
      ppList = g_list_next(pList);						//pFrame2
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//scrollbar2
      gtk_adjustment_set_value(gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(ppList->data)),gtk_adjustment_get_upper(gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(ppList->data))));*/

    //pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//scrollbar1
    //gtk_adjustment_set_value(gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(pList->data)),gtk_adjustment_get_upper(gtk_scrolled_window_get_vadjustment(GTK_SCROLLED_WINDOW(pList->data))));

  } else {
    cerr << "Error : this file or directory does not exist !" << endl;
    string message_str = "This directory does not exist !";
    gchar* message = g_locale_to_utf8(message_str.c_str(), -1, NULL, NULL, NULL);
    errorMessage(message);
  }
}

#endif // CLASS_SIMUL_H
