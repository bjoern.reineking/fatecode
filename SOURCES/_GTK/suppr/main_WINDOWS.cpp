#include <stdlib.h>
#include <gtk/gtk.h>
//#include <gtksourceview/gtksource.h>
#include <vector>
#include <string.h>
#include <iostream>
#include <fstream>
#include <stdio.h>
#include <sstream>

#define BOOST_NO_CXX11_SCOPED_ENUMS
#include <boost/filesystem.hpp>

#include "func.hpp"
#include "func_param_files_WINDOWS.hpp"
#include "func_simul_WINDOWS.hpp"
#include "func_param_simul.hpp"
#include "func_param_fire.hpp"
#include "func_param_pfg.hpp"
#include "func_param_mask.hpp"

using namespace std;


/*############################################################################################################################*/

bool IS_PARAM_CREATED = FALSE, IS_SIMUL1_CREATED = FALSE, IS_SIMUL2_CREATED = FALSE, IS_SIMUL3_CREATED = FALSE;

bool submit_simul, submit_succ1, submit_succ2, submit_maskPFG, submit_maskDist, submit_maskFire, submit_maskDrought, submit_soil, submit_fire1;
vector<bool> submit_fire, submit_dist, submit_drought;
bool CHECK_submit;

/* SIMUL parameters */
string SimulName, SuccModel, CompiledFile, SavedState;
unsigned NbCPUs, NbRep, SimulDuration;
unsigned NbStrata;
vector<unsigned> HeightStrata;
unsigned SeedTimeStep, SeedDuration;
bool is_savedState, is_savedArrays, is_savedObjects, is_dist, is_soil, is_fire, is_drought, is_hab, is_aliens;
unsigned NbDist, NbRespStagesDist, NbFires, NbRespStagesFire, NbRespStagesDrought, NbHabitats;
double SoilDefaultValue;

/* PFG parameters */
vector<string> Group, Type;
vector<unsigned> Light, Height, Longevity, Maturity;
vector<unsigned> DispDist50, DispDist99, DispDistLD;

vector<unsigned> MaxAbund, ImmSize, IsAlien, WideDisp, ModeDisp;
vector<bool> SeedDorm;
vector<unsigned> SeedLife_active, SeedLife_dormant;

vector<unsigned> Palatability;
vector<double> SoilContrib, TolMin, TolMax;
vector<unsigned> Flammability;
vector<double> SDminMod, SDmaxMod, SDminSev, SDmaxSev;
vector<unsigned> CountModToSev, CountSevMort, Recovery;

/* SUCCESSION parameters */
vector< vector<unsigned> > ChangeStrataAges, ActiveGerm, ShadeTolGerm, ShadeTolImm, ShadeTolMat;

/* DISTURBANCES parameters */
vector<string> DistNames;
vector<unsigned> DistFreq, PropKilledProp, PercentActivSeeds;
vector< vector<unsigned> > BreakAges, ResprAges;
vector< vector <vector<unsigned> > > PropKilledInd, PropResproutInd;

/* SOIL parameters */
unsigned NbSoilStrata;
vector< vector<unsigned> > ChangeSoilStrataAges, SoilTolGerm, SoilTolImm, SoilTolMat;

/* FIRE parameters */
unsigned initOption, neighOption, quotaOption, propagationOption;
unsigned initNbFires;
string prevDataFile;
vector<unsigned> ccExtent;
vector<double> initLogis, propLogis;
vector<double> probFires;
vector<string> FireNames;
vector<unsigned> FireFreq, FirePropKilledProp, FirePercentActivSeeds;
vector< vector<unsigned> > FireBreakAges, FireResprAges;
vector< vector <vector<unsigned> > > FirePropKilledInd, FirePropResproutInd;

/* DROUGHT parameters */
vector<string> DroughtChrono;
vector<unsigned> DroughtFreq, DroughtPropKilledProp, DroughtPercentActivSeeds;
vector< vector<unsigned> > DroughtBreakAges, DroughtResprAges;
vector< vector <vector<unsigned> > > DroughtPropKilledInd, DroughtPropResproutInd;

/* MASK parameters */
string MaskFile, ArraySavingFile, ObjectSavingFile, MoistFile;
vector<string> EnvsuitFile, DistFile, FireFile;
string FireMap, DroughtMap, SlopeMap, ElevationMap;
vector< vector<unsigned> > ChangingYearsPFG, ChangingYearsDist, ChangingYearsFire;
vector<unsigned> ChangingYearsDrought;
vector< vector<string> > newEnvsuitFiles, newDistFiles, newFireFiles, newClimDataFiles;
vector<string> newMoistFiles;

void clearParams(){

  submit_simul = FALSE;
  submit_succ1 = FALSE;
  submit_succ2 = FALSE;
  submit_maskPFG = FALSE;
  submit_soil = FALSE;
  submit_fire1 = FALSE;
  submit_fire.clear();
  submit_dist.clear();
  submit_drought.clear();
  submit_maskDist = FALSE;
  submit_maskFire = FALSE;
  submit_maskDrought = FALSE;
  CHECK_submit = FALSE;

  SimulName = "";
  SuccModel = "";
  CompiledFile = "";
  SavedState = "";
  NbCPUs = 0;
  NbRep = 0;
  SimulDuration = 0;
  NbStrata = 0;
  HeightStrata.clear();
  SeedTimeStep = 0;
  SeedDuration = 0;
  is_savedState = FALSE;
  is_savedArrays = FALSE;
  is_savedObjects = FALSE;
  is_dist = FALSE;
  is_soil = FALSE;
  is_fire = FALSE;
  is_drought = FALSE;
  is_hab = FALSE;
  is_aliens = FALSE;
  NbDist = 0;
  NbRespStagesDist = 0;
  NbFires = 0;
  NbRespStagesFire = 0;
  NbRespStagesDrought = 0;
  SoilDefaultValue = 0.0;

  Group.clear();
  Type.clear();
  Light.clear();
  Height.clear();
  Longevity.clear();
  Maturity.clear();
  DispDist50.clear();
  DispDist99.clear();
  DispDistLD.clear();
  MaxAbund.clear();
  ImmSize.clear();
  IsAlien.clear();
  WideDisp.clear();
  ModeDisp.clear();
  SeedDorm.clear();
  SeedLife_active.clear();
  SeedLife_dormant.clear();

  Palatability.clear();
  SoilContrib.clear();
  TolMin.clear();
  TolMax.clear();
  Flammability.clear();
  SDminMod.clear();
  SDmaxMod.clear();
  SDminSev.clear();
  SDmaxSev.clear();
  CountModToSev.clear();
  CountSevMort.clear();

  ChangeStrataAges.clear();
  ActiveGerm.clear();
  ShadeTolGerm.clear();
  ShadeTolImm.clear();
  ShadeTolMat.clear();

  DistNames.clear();
  DistFreq.clear();
  PropKilledProp.clear();
  PercentActivSeeds.clear();
  BreakAges.clear();
  ResprAges.clear();
  PropKilledInd.clear();
  PropResproutInd.clear();

  SoilTolGerm.clear();
  SoilTolImm.clear();
  SoilTolMat.clear();

  initOption = 0;
  neighOption = 0;
  quotaOption = 0;
  propagationOption = 0;
  initNbFires = 0;
  prevDataFile = "";
  ccExtent.clear();
  initLogis.clear();
  propLogis.clear();
  NbFires = 0;
  probFires.clear();
  FireNames.clear();
  FireFreq.clear();
  FirePropKilledProp.clear();
  FirePercentActivSeeds.clear();
  FireBreakAges.clear();
  FireResprAges.clear();
  FirePropKilledInd.clear();
  FirePropResproutInd.clear();

  DroughtChrono.clear();
  DroughtFreq.clear();
  DroughtPropKilledProp.clear();
  DroughtPercentActivSeeds.clear();
  DroughtBreakAges.clear();
  DroughtResprAges.clear();
  DroughtPropKilledInd.clear();
  DroughtPropResproutInd.clear();

  MaskFile = "";
  ArraySavingFile = "";
  ObjectSavingFile = "";
  MoistFile = "";
  EnvsuitFile.clear();
  newEnvsuitFiles.clear();
  ChangingYearsPFG.clear();
  ChangingYearsDist.clear();
  ChangingYearsFire.clear();
  ChangingYearsDrought.clear();
  DistFile.clear();
  newDistFiles.clear();
  FireFile.clear();
  newFireFiles.clear();
  newClimDataFiles.clear();
  FireMap = "";
  DroughtMap = "";
  SlopeMap = "";
  ElevationMap = "";
  newMoistFiles.clear();
}

/*############################################################################################################################*/

void PARAM_PART (GtkWidget* pButton, gpointer data);
void SIMUL1_PART (GtkWidget* pButton, gpointer data);
void SIMUL2_PART (GtkWidget* pButton, gpointer data);
void SIMUL3_PART (GtkWidget* pButton, gpointer data);
void INFO_PART (GtkWidget* pButton, gpointer data);

void NEW_SIMUL (GtkWidget* pButton, gpointer data);
void CHECK_SUBMIT (GtkWidget* pButton, gpointer data);
void WRITE_PARAM (GtkWidget* pButton, gpointer data);
void QUIT_SIMUL (GtkWidget* pButton, gpointer data);

void selectMaskFile (GtkWidget* pButton, gpointer data);
void selectMaskDirectory (GtkWidget* pButton, gpointer data);
void runFate (GtkWidget* pButton, gpointer data);

void generateStrata (GtkWidget* pButton, gpointer data);
void setCheckSensitiveSim (GtkWidget* pCheck, gpointer data);
void setSimulParamsZero (GtkWidget* pButton, gpointer data);
void submitSimulParams (GtkWidget* pButton, gpointer data);

void setParamsZero (GtkWidget* pButton, gpointer data);
void setParamsZero_sub (GtkWidget* pButton, gpointer data, unsigned tot);
void addNewPFG (GtkWidget* pButton, gpointer data);
void removePFG (GtkWidget* pButton, gpointer data);
void removeAllPFG(GtkWidget* pButton, gpointer data);

void generateStrataAges (GtkWidget* pButton, gpointer data);
void setSucc1ParamsZero (GtkWidget* pButton, gpointer data);
void submitSucc1Params (GtkWidget* pButton, gpointer data);
void generateLightStages (GtkWidget* pButton, gpointer data);
void setSucc2ParamsZero (GtkWidget* pButton, gpointer data);
void submitSucc2Params (GtkWidget* pButton, gpointer data);

void generateRespStages (GtkWidget* pButton, gpointer data);
void setDistParamsZero (GtkWidget* pButton, gpointer data);
void submitDistParams (GtkWidget* pButton, gpointer data);

void generateStrataSoil (GtkWidget* pButton, gpointer data);
void generateSoilStages (GtkWidget* pButton, gpointer data);
void setSoilParamsZero (GtkWidget* pButton, gpointer data);
void submitSoilParams (GtkWidget* pButton, gpointer data);

void generateProbFire (GtkWidget* pButton, gpointer data);
void setFireParamsZero (GtkWidget* pButton, gpointer data);
void submitFire1Params (GtkWidget* pButton, gpointer data);
void submitFire2Params (GtkWidget* pButton, gpointer data);

void submitPertParams (GtkWidget* pButton, gpointer data);

void generateMaskPFG (GtkWidget* pButton, gpointer data);
void generateMaskDist (GtkWidget* pButton, gpointer data);
void generateMaskFire (GtkWidget* pButton, gpointer data);
void generateNewMask (GtkWidget* pButton, gpointer data);
void generateNewMaskFire (GtkWidget* pButton, gpointer data);
void removeNewMask (GtkWidget* pButton, gpointer data);
void removeNewMaskFire (GtkWidget* pButton, gpointer data);
void submitMask1Params (GtkWidget* pButton, gpointer data);
void submitMask2Params (GtkWidget* pButton, gpointer data);
void submitMask3Params (GtkWidget* pButton, gpointer data);

void initOptData (GtkWidget* pRadio, gpointer data);
void initOptMap (GtkWidget* pRadio, gpointer data);
void initOptChao (GtkWidget* pRadio, gpointer data);
void neighOpt8 (GtkWidget* pRadio, gpointer data);
void quotaOptCons (GtkWidget* pRadio, gpointer data);
void propOptIntensity (GtkWidget* pRadio, gpointer data);

/*############################################################################################################################*/

int main(int argc,char **argv) //OK
{
  /* Initialisation de GTK+ */
  gtk_init(&argc,&argv);

  /* ----------------------------------------------------------------------------------- */
  /* Variables */
  GtkWidget* main_Window = NULL;
  GtkWidget* menu_bar = NULL;
  GtkWidget* WELCOME_BOX = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget* MAIN_BOX = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

  /* ----------------------------------------------------------------------------------- */

  /* Création de la fenêtre */
  main_Window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(main_Window), "FATE-HD");
  gtk_window_set_default_size(GTK_WINDOW(main_Window), 1200, 800);
  gtk_window_set_position(GTK_WINDOW (main_Window), GTK_WIN_POS_CENTER);
  gtk_container_set_border_width(GTK_CONTAINER(main_Window), 10);
  g_signal_connect(G_OBJECT(main_Window), "destroy", G_CALLBACK(gtk_main_quit), NULL);

  /* ----------------------------------------------------------------------------------- */
  /* Menu creation */

  menu_bar = gtk_toolbar_new();
  gtk_toolbar_set_style(GTK_TOOLBAR(menu_bar), GTK_TOOLBAR_ICONS);
  gtk_container_set_border_width(GTK_CONTAINER(menu_bar), 20);

  GtkToolItem* PARAM = gtk_tool_button_new(gtk_image_new_from_icon_name("go-home",GTK_ICON_SIZE_MENU),"HOME");
  gtk_tool_item_set_homogeneous(PARAM,TRUE);
  gtk_tool_item_set_expand(PARAM,TRUE);
  g_signal_connect(G_OBJECT(PARAM), "clicked", G_CALLBACK(PARAM_PART), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), PARAM, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(PARAM),g_locale_to_utf8("PART 1 : Create a new set of parameters",-1,NULL,NULL,NULL));

  GtkToolItem* sep0 = gtk_separator_tool_item_new();
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), sep0, -1);

  GtkToolItem* new_param = gtk_tool_button_new(gtk_image_new_from_icon_name("folder-new",GTK_ICON_SIZE_MENU),"NEW");
  gtk_tool_item_set_homogeneous (new_param,TRUE);
  gtk_tool_item_set_expand(new_param,TRUE);
  g_signal_connect(G_OBJECT(new_param), "clicked", G_CALLBACK(NEW_SIMUL), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), new_param, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(new_param),g_locale_to_utf8("Create a new set of parameters",-1,NULL,NULL,NULL));
  gtk_widget_set_sensitive(GTK_WIDGET(new_param),FALSE);

  GtkToolItem* exec = gtk_tool_button_new(gtk_image_new_from_icon_name("system-run",GTK_ICON_SIZE_MENU),"EXECUTE");
  gtk_tool_item_set_homogeneous (exec,TRUE);
  gtk_tool_item_set_expand(exec,TRUE);
  g_signal_connect(G_OBJECT(exec), "clicked", G_CALLBACK(CHECK_SUBMIT), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), exec, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(exec),g_locale_to_utf8("Check if all the required \nparameters have been submitted",-1,NULL,NULL,NULL));
  gtk_widget_set_sensitive(GTK_WIDGET(exec),FALSE);

  GtkToolItem* apply = gtk_tool_button_new(gtk_image_new_from_icon_name("document-save",GTK_ICON_SIZE_MENU),"APPLY");
  gtk_tool_item_set_homogeneous (apply,TRUE);
  gtk_tool_item_set_expand(apply,TRUE);
  g_signal_connect(G_OBJECT(apply), "clicked", G_CALLBACK(WRITE_PARAM), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), apply, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(apply),g_locale_to_utf8("Create the Directory architecture and \nthe required parameters files",-1,NULL,NULL,NULL));
  gtk_widget_set_sensitive(GTK_WIDGET(apply),FALSE);

  GtkToolItem* sep1 = gtk_separator_tool_item_new();
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), sep1, -1);

  GtkToolItem* SIMUL = gtk_tool_button_new(gtk_image_new_from_icon_name("go-home",GTK_ICON_SIZE_MENU),"HOME");
  gtk_tool_item_set_homogeneous (SIMUL,TRUE);
  gtk_tool_item_set_expand(SIMUL,TRUE);
  g_signal_connect(G_OBJECT(SIMUL), "clicked", G_CALLBACK(SIMUL1_PART), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), SIMUL, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(SIMUL),g_locale_to_utf8("PART 2 : Create a new FATE-HD simulation",-1,NULL,NULL,NULL));

  GtkToolItem* sep2 = gtk_separator_tool_item_new();
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), sep2, -1);

  GtkToolItem* new_simul = gtk_tool_button_new(gtk_image_new_from_icon_name("media-playback-start",GTK_ICON_SIZE_MENU),"MEDIA_PLAY");
  gtk_tool_item_set_homogeneous (new_simul,TRUE);
  gtk_tool_item_set_expand(new_simul,TRUE);
  g_signal_connect(G_OBJECT(new_simul), "clicked", G_CALLBACK(SIMUL2_PART), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), new_simul, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(new_simul),g_locale_to_utf8("Start a new FATE-HD simulation !",-1,NULL,NULL,NULL));
  gtk_widget_set_sensitive(GTK_WIDGET(new_simul),FALSE);


  GtkToolItem* new_results = gtk_tool_button_new(gtk_image_new_from_icon_name("system-search",GTK_ICON_SIZE_MENU),"PROPERTIES");
  gtk_tool_item_set_homogeneous (new_results,TRUE);
  gtk_tool_item_set_expand(new_results,TRUE);
  g_signal_connect(G_OBJECT(new_results), "clicked", G_CALLBACK(SIMUL3_PART), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), new_results, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(new_results),g_locale_to_utf8("Analyse FATE-HD results !",-1,NULL,NULL,NULL));
  gtk_widget_set_sensitive(GTK_WIDGET(new_results),FALSE);

  GtkToolItem* sep3 = gtk_separator_tool_item_new();
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), sep3, -1);

  GtkToolItem* info = gtk_tool_button_new(gtk_image_new_from_icon_name("help-contents",GTK_ICON_SIZE_MENU),"INFO");
  gtk_tool_item_set_homogeneous (info,TRUE);
  gtk_tool_item_set_expand(info,TRUE);
  g_signal_connect(G_OBJECT(info), "clicked", G_CALLBACK(INFO_PART), MAIN_BOX);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), info, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(info),g_locale_to_utf8("Information about FATE-HDD",-1,NULL,NULL,NULL));

  GtkToolItem* exit = gtk_tool_button_new(gtk_image_new_from_icon_name("window-close",GTK_ICON_SIZE_MENU),"QUIT");
  gtk_tool_item_set_homogeneous (exit,TRUE);
  gtk_tool_item_set_expand(exit,TRUE);
  g_signal_connect(G_OBJECT(exit), "clicked", G_CALLBACK(QUIT_SIMUL), NULL);
  gtk_toolbar_insert(GTK_TOOLBAR(menu_bar), exit, -1);
  gtk_widget_set_tooltip_text(GTK_WIDGET(exit),g_locale_to_utf8("Quit",-1,NULL,NULL,NULL));


  /* ----------------------------------------------------------------------------------- */
  /* WELCOME CREATION */

  GtkWidget* scrollbar = gtk_scrolled_window_new(NULL, NULL);
  GtkWidget* box_welcome_h = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
  GtkWidget* box_welcome_v1 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  GtkWidget* box_welcome_v2 = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  
  gtk_box_pack_start(GTK_BOX(WELCOME_BOX), scrollbar, TRUE, TRUE, 0);
  //gtk_container_add(GTK_CONTAINER(scrollbar), box_welcome_h);
  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar), box_welcome_h);
  gtk_box_pack_start(GTK_BOX(box_welcome_h), box_welcome_v1, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(box_welcome_h), box_welcome_v2, TRUE, TRUE, 0);
  
  GdkRGBA color;
  gdk_rgba_parse(&color,"white");
  GList* pList = gtk_container_get_children(GTK_CONTAINER(scrollbar)); 
  gtk_widget_override_background_color(GTK_WIDGET(pList->data), GTK_STATE_FLAG_NORMAL, &color);
  gtk_container_set_border_width(GTK_CONTAINER(scrollbar), 20);
  gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
  

  GtkWidget* label_tmp;
  vector<gchar*> txt_tmp = {(gchar*)"<b><big>Welcome in FATE-HD!</big></b>",
			    (gchar*)"<i>Developped by :\n\n</i>Isabelle Boulangeat\nDamien Georges\nMaya Guéguen",
			    (gchar*)"<i>Last stable version :\n\n</i><b>5-7.0</b>"};
  for(unsigned i=0; i<3; i++){
    label_tmp = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
    gtk_label_set_justify(GTK_LABEL(label_tmp), GTK_JUSTIFY_CENTER);
    gtk_box_pack_start(GTK_BOX(box_welcome_v1), label_tmp, TRUE, FALSE, 0);
  }
  
  GtkWidget* pImage = gtk_image_new_from_file(("."+delimiter+"tree.jpg").c_str());
  gtk_box_pack_start(GTK_BOX(box_welcome_v2), pImage, TRUE, TRUE, 0);
    
  /* ----------------------------------------------------------------------------------- */

  gtk_container_add(GTK_CONTAINER(main_Window), MAIN_BOX);
  gtk_box_pack_start(GTK_BOX(MAIN_BOX), menu_bar, FALSE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(MAIN_BOX), WELCOME_BOX, TRUE, TRUE, 0);
  gtk_box_pack_start(GTK_BOX(MAIN_BOX), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0), FALSE, FALSE, 0);	//PARAM_BOX
  gtk_box_pack_start(GTK_BOX(MAIN_BOX), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0), FALSE, FALSE, 0);	//SIMUL1_BOX
  gtk_box_pack_start(GTK_BOX(MAIN_BOX), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0), FALSE, FALSE, 0);	//SIMUL2_BOX
  gtk_box_pack_start(GTK_BOX(MAIN_BOX), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0), FALSE, FALSE, 0);	//SIMUL3_BOX

  gtk_widget_show_all(main_Window);
  gtk_main();

  return EXIT_SUCCESS;
}

/*############################################################################################################################*/

void PARAM_PART (GtkWidget* pButton, gpointer data){ //OK
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));

  for(unsigned i=0; i<2; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<3; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),TRUE);
    pList = g_list_next(pList);
  }
  for(unsigned i=0; i<3; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),FALSE);
    pList = g_list_next(pList);
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = g_list_next(pList);   //WELCOME_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //PARAM_BOX
  if(IS_PARAM_CREATED){ gtk_widget_show(GTK_WIDGET(pList->data)); }
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
}

/*############################################################################################################################*/

void SIMUL1_PART (GtkWidget* pButton, gpointer data){ //OK
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));

  for(unsigned i=0; i<2; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<3; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),FALSE);
    pList = g_list_next(pList);
  }
  for(unsigned i=0; i<3; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),TRUE);
    pList = g_list_next(pList);
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = g_list_next(pList);   //WELCOME_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //PARAM_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  if(IS_SIMUL1_CREATED){ gtk_widget_show(GTK_WIDGET(pList->data)); }
  else {
    IS_SIMUL1_CREATED = TRUE;
    gtk_widget_show(GTK_WIDGET(pList->data));
  }
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
}

/*############################################################################################################################*/

void SIMUL2_PART (GtkWidget* pButton, gpointer data){
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));

  for(unsigned i=0; i<2; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<3; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),FALSE);
    pList = g_list_next(pList);
  }
  for(unsigned i=0; i<3; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),TRUE);
    pList = g_list_next(pList);
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = g_list_next(pList);   //WELCOME_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //PARAM_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  if(IS_SIMUL2_CREATED){ gtk_widget_show(GTK_WIDGET(pList->data)); }
  else {
    /* ----------------------------------------------------------------------------------- */
    /* SIMUL CREATION */
    GtkWidget* box_simul_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    GtkWidget* SIMUL_BOX = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    gtk_box_pack_start(GTK_BOX(SIMUL_BOX), box_simul_v, TRUE, TRUE, 0);

    GtkWidget* pTable = gtk_grid_new();
    gtk_grid_set_column_spacing(GTK_GRID(pTable), 20);
    gtk_container_set_border_width(GTK_CONTAINER(pTable), 40);

    //GtkWidget* pCheck = gtk_check_button_new_with_label("Use FATE binary file basic");
    //gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(pCheck),TRUE);
    //g_signal_connect(G_OBJECT(pCheck), "clicked", G_CALLBACK(setCheckSensitiveSim), pTable);

    GtkWidget* pEntry1 = gtk_entry_new();
    GtkWidget* pEntry2 = gtk_entry_new();
    GtkWidget* pButton1 = gtk_button_new_with_label("Select a parameter directory");
    g_signal_connect(G_OBJECT(pButton1), "clicked", G_CALLBACK(selectMaskDirectory), pEntry1);
    GtkWidget* pButton2 = gtk_button_new_with_label("Select a FATE binary file");
    g_signal_connect(G_OBJECT(pButton2), "clicked", G_CALLBACK(selectMaskFile), pEntry2);

    GtkWidget* pButton3 = gtk_button_new();
    GtkWidget* label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>RUN FATE</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(pButton3),label_tmp);
    g_signal_connect(G_OBJECT(pButton3), "clicked", G_CALLBACK(runFate), box_simul_v);

    //gtk_widget_set_sensitive(pButton2,FALSE);
    //gtk_widget_set_sensitive(pEntry2,FALSE);

    gtk_box_pack_start(GTK_BOX(box_simul_v), pTable, FALSE, FALSE, 0);
    gtk_grid_attach(GTK_GRID(pTable), pButton1, 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(pTable), pEntry1, 0, 1, 1, 1);
    //gtk_grid_attach(GTK_GRID(pTable), pCheck, 1, 2, 0, 2, GTK_EXPAND, GTK_EXPAND, 0, 0);
    gtk_grid_attach(GTK_GRID(pTable), pButton2, 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(pTable), pEntry2, 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(pTable), pButton3, 2, 0, 1, 2);

    vector<gchar*> txt_tmp = {(gchar*)"<b>RESULTS output</b>",(gchar*)"<b>RESULTS error</b>"};
    label_tmp = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[0]);
    GtkWidget* pFrame1 = gtk_frame_new(NULL);
    gtk_frame_set_label_widget(GTK_FRAME(pFrame1),label_tmp);
    gtk_container_set_border_width(GTK_CONTAINER(pFrame1), 10);
    label_tmp = gtk_label_new(NULL);
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[1]);
    GtkWidget* pFrame2 = gtk_frame_new(NULL);
    gtk_frame_set_label_widget(GTK_FRAME(pFrame2),label_tmp);
    gtk_container_set_border_width(GTK_CONTAINER(pFrame2), 10);
    GtkWidget* scrollbar1 = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar1), 5);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar1), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    GtkWidget* scrollbar2 = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar2), 5);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar2), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    GtkWidget* box_h = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0);
    gtk_box_pack_start(GTK_BOX(box_simul_v), box_h, TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(box_h), pFrame1, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(pFrame1), scrollbar1);
    //gtk_container_add(GTK_CONTAINER(scrollbar1), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    //gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar1), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar1), gtk_text_view_new());
    gtk_box_pack_start(GTK_BOX(box_h), pFrame2, TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(pFrame2), scrollbar2);
    //gtk_container_add(GTK_CONTAINER(scrollbar2), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    //gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar2), gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar2), gtk_text_view_new());

    GdkRGBA color;
    gdk_rgba_parse(&color,"black");
    GList* tmpList = gtk_container_get_children(GTK_CONTAINER(scrollbar1));
    gtk_widget_override_background_color(GTK_WIDGET(tmpList->data), GTK_STATE_FLAG_NORMAL, &color);
    tmpList = gtk_container_get_children(GTK_CONTAINER(scrollbar2));
    gtk_widget_override_background_color(GTK_WIDGET(tmpList->data), GTK_STATE_FLAG_NORMAL, &color);

    IS_SIMUL2_CREATED = TRUE;

    gtk_widget_destroy(GTK_WIDGET(pList->data));
    gtk_box_pack_start(GTK_BOX((GtkWidget*) data), SIMUL_BOX, TRUE, TRUE, 0);
    gtk_box_reorder_child(GTK_BOX((GtkWidget*) data), SIMUL_BOX, 4);
    gtk_widget_show_all(SIMUL_BOX);
  }
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
}

/*############################################################################################################################*/

void SIMUL3_PART (GtkWidget* pButton, gpointer data){

}

/*############################################################################################################################*/

void INFO_PART (GtkWidget* pButton, gpointer data){ //OK
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));

  for(unsigned i=0; i<2; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<3; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),FALSE);
    pList = g_list_next(pList);
  }
  for(unsigned i=0; i<3; i++){ pList = g_list_next(pList); }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(pList->data),FALSE);
    pList = g_list_next(pList);
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //menu_bar
  pList = g_list_next(pList);   //WELCOME_BOX
  gtk_widget_show(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //PARAM_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
  pList = g_list_next(pList);   //SIMUL_BOX
  gtk_widget_hide(GTK_WIDGET(pList->data));
}

/*############################################################################################################################*/

void QUIT_SIMUL (GtkWidget* pButton, gpointer data){ //OK
  bool reallyquit = FALSE;

  if(IS_PARAM_CREATED){
    gchar* message = g_locale_to_utf8("You have already started to create a set of parameters !\n Do you want to abandon it ?",-1,NULL,NULL,NULL);
    reallyquit = carefulMessage(message);
  } else if(IS_SIMUL2_CREATED) {
    gchar* message = g_locale_to_utf8("You have already started a simulation !\n Do you want to abandon it ?",-1,NULL,NULL,NULL);
    reallyquit = carefulMessage(message);
  } else { reallyquit = TRUE; }
  if(reallyquit) gtk_main_quit();
}

/*############################################################################################################################*/

void CHECK_SUBMIT (GtkWidget* pButton, gpointer data){
  if(IS_PARAM_CREATED){
    unsigned compt_submit = 0, compt;
    if(submit_simul){
      compt_submit++;
      if(submit_succ1){
	compt_submit++;
	if(submit_succ2){
	  compt_submit++;
	  if(submit_maskPFG){
	    compt_submit++;
	    if(is_dist){
	      compt = 0;
	      for(unsigned i=0; i<NbDist; i++){ if(submit_dist[i]) compt++; }
	      if(compt==NbDist){
		compt_submit++;
		if(submit_maskDist){ compt_submit++;
		} else {
		  gchar* message = g_locale_to_utf8("You must submit the mask \nparameters for the disturbances !",-1,NULL,NULL,NULL);
		  errorMessage(message);
		}
	      } else {
		gchar* message = g_locale_to_utf8("You must submit the \nparameters for each disturbance !",-1,NULL,NULL,NULL);
		errorMessage(message);
	      }
	    } else { compt_submit+=2; }
	    if(is_soil){
	      if(submit_soil){ compt_submit++;
	      } else {
		gchar* message = g_locale_to_utf8("You must submit the\nsoil parameters !",-1,NULL,NULL,NULL);
		errorMessage(message);
	      }
	    } else { compt_submit++; }
	    if(is_fire){
	      compt = 0;
	      for(unsigned i=0; i<NbFires; i++){ if(submit_fire[i]) compt++; }
	      if(compt==NbFires){
		compt_submit++;
		if(submit_maskFire){ compt_submit++;
		} else {
		  gchar* message = g_locale_to_utf8("You must submit the mask \nparameters for the fire disturbances !",-1,NULL,NULL,NULL);
		  errorMessage(message);
		}
	      } else {
		gchar* message = g_locale_to_utf8("You must submit the \nparameters for each fire disturbance !",-1,NULL,NULL,NULL);
		errorMessage(message);
	      }
	    } else { compt_submit+=2; }
	    if(is_drought){
	      compt = 0;
	      for(unsigned i=0; i<2; i++){ if(submit_drought[i]) compt++; }
	      if(compt==2){
		compt_submit++;
		compt_submit++;
		/*if(submit_maskDrought){ compt_submit++;
		  } else {
		  gchar* message = g_locale_to_utf8("You must submit the mask \nparameters for the drought disturbances !",-1,NULL,NULL,NULL);
		  errorMessage(message);
		  }*/
	      } else {
		gchar* message = g_locale_to_utf8("You must submit the \nparameters for each drought disturbance !",-1,NULL,NULL,NULL);
		errorMessage(message);
	      }
	    } else { compt_submit+=2; }
	  } else {
	    gchar* message = g_locale_to_utf8("You must submit the mask parameters \nfor the simulation and the PFGs !",-1,NULL,NULL,NULL);
	    errorMessage(message);
	  }
	} else {
	  gchar* message = g_locale_to_utf8("You must submit the \n light succession parameters !",-1,NULL,NULL,NULL);
	  errorMessage(message);
	}
      } else {
	gchar* message = g_locale_to_utf8("You must submit the \n strata succession parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
      }
    } else {
      gchar* message = g_locale_to_utf8("You must submit the\nsimulation parameters !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }

    if(compt_submit==11){
      CHECK_submit = TRUE;
      gchar* message = g_locale_to_utf8("Congratulations !\n You have filled all the parameters needed !\n You can write your parameters files !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }

  } else {
    gchar* message = g_locale_to_utf8("You must create \na new simulation !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void WRITE_PARAM (GtkWidget* pButton, gpointer data){
  if(IS_PARAM_CREATED){
    if(CHECK_submit){

      string local_dir = boost::filesystem::current_path().filename().string();
      string full_path_str = boost::filesystem::current_path().string();
      unsigned index = full_path_str.find(local_dir, 0);
      full_path_str.replace(index,local_dir.size(),"");

      createFileStructure(SimulName,NbRep);

      for(unsigned i=0; i<Group.size(); i++){
	/* DISPERSAL files*/
	writeDispFile(full_path_str+delimiter+SimulName,Group[i],DispDist50[i],DispDist99[i],DispDistLD[i]);

	/* SUCCESSION files*/
	if(is_soil){
	  writeSuccFile(full_path_str+delimiter+SimulName,Group[i],Maturity[i],Longevity[i],MaxAbund[i],ImmSize[i],ChangeStrataAges[i],
			0,WideDisp[i],ModeDisp[i],ActiveGerm[i],ShadeTolGerm[i],ShadeTolImm[i],ShadeTolMat[i],
			SeedLife_active[i],SeedLife_dormant[i],SeedDorm[i],SoilContrib[i],
			SoilTolGerm[i],SoilTolImm[i],SoilTolMat[i]);
	} else {
	  writeSuccFile(full_path_str+delimiter+SimulName,Group[i],Maturity[i],Longevity[i],MaxAbund[i],ImmSize[i],ChangeStrataAges[i],
			0,WideDisp[i],ModeDisp[i],ActiveGerm[i],ShadeTolGerm[i],ShadeTolImm[i],ShadeTolMat[i],
			SeedLife_active[i],SeedLife_dormant[i],SeedDorm[i]);
	}

	/* DISTURBANCES files*/
	if(is_dist){
	  writePertFile("dist",full_path_str+delimiter+SimulName,i,Group[i],NbDist,NbRespStagesDist,
			PropKilledProp,BreakAges,ResprAges,PropKilledInd,
			PropResproutInd,PercentActivSeeds);
	}
	/* FIRE files*/
	if(is_fire){
	  writePertFile("fire",full_path_str+delimiter+SimulName,i,Group[i],NbFires,NbRespStagesFire,
			FirePropKilledProp,FireBreakAges,FireResprAges,FirePropKilledInd,
			FirePropResproutInd,FirePercentActivSeeds);
	}
	/* DROUGHT files*/
	if(is_drought){
	  writePertFile("drought",full_path_str+delimiter+SimulName,i,Group[i],2,NbRespStagesDrought,
			DroughtPropKilledProp,DroughtBreakAges,DroughtResprAges,DroughtPropKilledInd,
			DroughtPropResproutInd,DroughtPercentActivSeeds);
	}
      }


      /* Copy and rename .txt and .asc files */
      /* mask, arrays and objects saving years */
      string new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+"maskSimulation.asc";
      copyFiles(MaskFile.c_str(),new_path.c_str(),".asc");
      new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SAVE"+delimiter+"arraysSavingYears.txt";
      if(copyFiles(ArraySavingFile.c_str(),new_path.c_str(),".txt")){ is_savedArrays = TRUE; }
      new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SAVE"+delimiter+"objectsSavingYears.txt";
      if(copyFiles(ObjectSavingFile.c_str(),new_path.c_str(),".txt")){ is_savedObjects = TRUE; }
      if(SavedState.length()>0){
	new_path = full_path_str+SimulName+delimiter+"RESULTS"+delimiter+"prevSavedState.sav";
	copyFiles(SavedState.c_str(),new_path.c_str(),".sav");
      }

      /* HS PFGs masks */
      for(unsigned i=0; i<Group.size(); i++){
	new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"ENVSUIT"+delimiter+"HS_"+Group[i]+".asc";
	copyFiles(EnvsuitFile[i].c_str(),new_path.c_str(),".asc");
      }

      vector<unsigned> tmp_years_PFG, tmp_years_dist, tmp_years_fire, tmp_years_fireFreq, tmp_years_clim, tmp_years_drought, tmp_years_aliens, tmp_years_aliensFreq;
      vector< vector<string> > newPathsPFG, newPathsDist, newPathsFire, newFreqFire, newPathsClimData, newPathsAliens, newFreqAliens;

      vector<string> climData_Mask, newPathsMoist;
      //vector< vector<string> > newClimaticFiles;

      /* ------------------------------------------------------------------------------------------------------------- */
      /* GLOBAL environmental changing times */
      for(unsigned i=0; i<Group.size(); i++){
	for(vector<unsigned>::iterator it=ChangingYearsPFG[i].begin(); it!=ChangingYearsPFG[i].end(); ++it) {
	  if(find(tmp_years_PFG.begin(),tmp_years_PFG.end(),*it)==tmp_years_PFG.end()){
	    tmp_years_PFG.push_back(*it);
	  }
	}
      }
      sort(tmp_years_PFG.begin(),tmp_years_PFG.end());

      if(tmp_years_PFG.size()>0){
	new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"environmental_changing_times.txt";
	ofstream file(new_path.c_str(), ios::out | ios::trunc);
	if(file){
	  for(unsigned i=0; i<tmp_years_PFG.size(); i++){ file << tmp_years_PFG[i] << endl; }
	  file.close();
	} else { cerr << "Error : cannot open the file : " << new_path << endl; }

	/* NEW HS PFGs masks */
	newPathsPFG.resize(Group.size());
	for(unsigned i=0; i<Group.size(); i++){
	  for(unsigned j=0; j<newEnvsuitFiles[i].size(); j++){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"PFGS"+delimiter+"ENVSUIT"+delimiter+"HS_"+Group[i]+"_t"+IntToString(ChangingYearsPFG[i][j])+".asc";
	    copyFiles(newEnvsuitFiles[i][j].c_str(),new_path.c_str(),".asc");
	    newPathsPFG[i].push_back(new_path);
	  }
	}

	/* INDIVIDUAL environmental changing times */
	for(unsigned t=0; t<tmp_years_PFG.size(); t++){
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"environmental_changing_masks_t"+IntToString(tmp_years_PFG[t])+".txt";
	  ofstream file(new_path.c_str(), ios::out | ios::trunc);
	  if(file){
	    for(unsigned i=0; i<Group.size(); i++){
	      if(find(ChangingYearsPFG[i].begin(),ChangingYearsPFG[i].end(),tmp_years_PFG[t])!=ChangingYearsPFG[i].end()){
		file << newPathsPFG[i][distance(ChangingYearsPFG[i].begin(),find(ChangingYearsPFG[i].begin(),ChangingYearsPFG[i].end(),tmp_years_PFG[t]))] << endl;
	      }
	    }
	    file.close();
	  } else { cerr << "Error : cannot open the file : " << new_path << endl; }
	}
      }
      /* ------------------------------------------------------------------------------------------------------------- */

      if(is_dist){

	/* Disturbances masks */
	for(unsigned i=0; i<NbDist; i++){
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+DistNames[i]+".asc";
	  copyFiles(DistFile[i].c_str(),new_path.c_str(),".asc");
	}

	/* ------------------------------------------------------------------------------------------------------------- */
	/* GLOBAL disturbances changing times */
	for(unsigned i=0; i<NbDist; i++){
	  for(vector<unsigned>::iterator it=ChangingYearsDist[i].begin(); it!=ChangingYearsDist[i].end(); ++it) {
	    if(find(tmp_years_dist.begin(),tmp_years_dist.end(),*it)==tmp_years_dist.end()){
	      tmp_years_dist.push_back(*it);
	    }
	  }
	}
	sort(tmp_years_dist.begin(),tmp_years_dist.end());

	if(tmp_years_dist.size()>0){
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"disturbances_changing_times.txt";
	  ofstream file(new_path.c_str(), ios::out | ios::trunc);
	  if(file){
	    for(unsigned i=0; i<tmp_years_dist.size(); i++){ file << tmp_years_dist[i] << endl; }
	    file.close();
	  } else { cerr << "Error : cannot open the file : " << new_path << endl; }

	  /* NEW disturbances masks */
	  newPathsDist.resize(NbDist);
	  for(unsigned i=0; i<NbDist; i++){
	    for(unsigned j=0; j<newDistFiles[i].size(); j++){
	      new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+DistNames[i]+"_t"+IntToString(ChangingYearsDist[i][j])+".asc";
	      copyFiles(newDistFiles[i][j].c_str(),new_path.c_str(),".asc");
	      newPathsDist[i].push_back(new_path);
	    }
	  }

	  /* INDIVIDUAL disturbances changing times */
	  for(unsigned t=0; t<tmp_years_dist.size(); t++){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"disturbances_changing_masks_t"+IntToString(tmp_years_dist[t])+".txt";
	    ofstream file(new_path.c_str(), ios::out | ios::trunc);
	    if(file){
	      for(unsigned i=0; i<NbDist; i++){
		if(find(ChangingYearsDist[i].begin(),ChangingYearsDist[i].end(),tmp_years_dist[t])!=ChangingYearsDist[i].end()){
		  file << newPathsDist[i][distance(ChangingYearsDist[i].begin(),find(ChangingYearsDist[i].begin(),ChangingYearsDist[i].end(),tmp_years_dist[t]))] << endl;
		}
	      }
	      file.close();
	    } else { cerr << "Error : cannot open the file : " << new_path << endl; }
	  }
	}
	/* ------------------------------------------------------------------------------------------------------------- */
      }

      if(is_fire){
	if(initOption==3){
	  /* Fires masks */
	  for(unsigned i=0; i<NbFires; i++){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+FireNames[i]+".asc";
	    copyFiles(FireFile[i].c_str(),new_path.c_str(),".asc");
	  }
	  /* ------------------------------------------------------------------------------------------------------------- */
	  /* GLOBAL fires changing times */
	  for(unsigned i=0; i<NbFires; i++){
	    for(vector<unsigned>::iterator it=ChangingYearsFire[i].begin(); it!=ChangingYearsFire[i].end(); ++it) {
	      if(find(tmp_years_fire.begin(),tmp_years_fire.end(),*it)==tmp_years_fire.end()){
		tmp_years_fire.push_back(*it);
	      }
	    }
	  }
	  sort(tmp_years_fire.begin(),tmp_years_fire.end());

	  if(tmp_years_fire.size()>0){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"fire_changing_times.txt";
	    ofstream file(new_path.c_str(), ios::out | ios::trunc);
	    if(file){
	      for(unsigned i=0; i<tmp_years_fire.size(); i++){ file << tmp_years_fire[i] << endl; }
	      file.close();
	    } else { cerr << "Error : cannot open the file : " << new_path << endl; }

	    /* NEW fires masks */
	    newPathsFire.resize(NbFires);
	    for(unsigned i=0; i<NbFires; i++){
	      for(unsigned j=0; j<newFireFiles[i].size(); j++){
		new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+FireNames[i]+"_t"+IntToString(ChangingYearsFire[i][j])+".asc";
		copyFiles(newFireFiles[i][j].c_str(),new_path.c_str(),".asc");
		newPathsFire[i].push_back(new_path);
	      }
	    }

	    /* INDIVIDUAL fires changing times */
	    for(unsigned t=0; t<tmp_years_fire.size(); t++){
	      new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"fire_changing_masks_t"+IntToString(tmp_years_fire[t])+".txt";
	      ofstream file(new_path.c_str(), ios::out | ios::trunc);
	      if(file){
		for(unsigned i=0; i<NbFires; i++){
		  if(find(ChangingYearsFire[i].begin(),ChangingYearsFire[i].end(),tmp_years_fire[t])!=ChangingYearsFire[i].end()){
		    file << newPathsFire[i][distance(ChangingYearsFire[i].begin(),find(ChangingYearsFire[i].begin(),ChangingYearsFire[i].end(),tmp_years_fire[t]))] << endl;
		  }
		}
		file.close();
	      } else { cerr << "Error : cannot open the file : " << new_path << endl; }
	    }
	  }
	  /* ------------------------------------------------------------------------------------------------------------- */

	}
	if(initOption==4){
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+"DroughtMap.asc";
	  copyFiles(DroughtMap.c_str(),new_path.c_str(),".asc");
	  climData_Mask.push_back(new_path);
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+"SlopeMap.asc";
	  copyFiles(SlopeMap.c_str(),new_path.c_str(),".asc");
	  climData_Mask.push_back(new_path);
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+"ElevationMap.asc";
	  copyFiles(ElevationMap.c_str(),new_path.c_str(),".asc");
	  climData_Mask.push_back(new_path);

	  /* ------------------------------------------------------------------------------------------------------------- */
	  /* GLOBAL climatic data changing times */
	  for(unsigned i=0; i<NbFires; i++){
	    for(vector<unsigned>::iterator it=ChangingYearsFire[i].begin(); it!=ChangingYearsFire[i].end(); ++it) {
	      if(find(tmp_years_clim.begin(),tmp_years_clim.end(),*it)==tmp_years_clim.end()){
		tmp_years_fire.push_back(*it);
	      }
	    }
	  }
	  sort(tmp_years_clim.begin(),tmp_years_clim.end());

	  if(tmp_years_clim.size()>0){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"climaticData_changing_times.txt";
	    ofstream file(new_path.c_str(), ios::out | ios::trunc);
	    if(file){
	      for(unsigned i=0; i<tmp_years_clim.size(); i++){ file << tmp_years_clim[i] << endl; }
	      file.close();
	    } else { cerr << "Error : cannot open the file : " << new_path << endl; }

	    /* NEW climaticData masks */
	    newPathsClimData.resize(NbFires);
	    for(unsigned i=0; i<NbFires; i++){
	      for(unsigned j=0; j<newClimDataFiles[i].size(); j++){
		new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+FireNames[i]+"drought_t"+IntToString(ChangingYearsFire[i][j])+".asc";
		copyFiles(newClimDataFiles[i][j].c_str(),new_path.c_str(),".asc");
		newPathsClimData[i].push_back(new_path);
	      }
	    }

	    /* INDIVIDUAL climaticData changing times */
	    for(unsigned t=0; t<tmp_years_clim.size(); t++){
	      new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"climaticData_changing_masks_t"+IntToString(tmp_years_clim[t])+".txt";
	      ofstream file(new_path.c_str(), ios::out | ios::trunc);
	      if(file){
		for(unsigned i=0; i<NbFires; i++){
		  if(find(ChangingYearsFire[i].begin(),ChangingYearsFire[i].end(),tmp_years_clim[t])!=ChangingYearsFire[i].end()){
		    file << newPathsClimData[i][distance(ChangingYearsFire[i].begin(),find(ChangingYearsFire[i].begin(),ChangingYearsFire[i].end(),tmp_years_clim[t]))] << endl;
		  }
		}
		file.close();
	      } else { cerr << "Error : cannot open the file : " << new_path << endl; }
	    }
	  }
	  /* ------------------------------------------------------------------------------------------------------------- */

	}

      }
      
      if(is_drought){

	/* Moisture index mask */
	new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+"moistureIndex.asc";
	copyFiles(MoistFile.c_str(),new_path.c_str(),".asc");

	/* ------------------------------------------------------------------------------------------------------------- */
	/* GLOBAL moisture changing times */
	for(vector<unsigned>::iterator it=ChangingYearsDrought.begin(); it!=ChangingYearsDrought.end(); ++it) {
	  if(find(tmp_years_drought.begin(),tmp_years_drought.end(),*it)==tmp_years_drought.end()){
	    tmp_years_drought.push_back(*it);
	  }
	}
	sort(tmp_years_drought.begin(),tmp_years_drought.end());

	if(tmp_years_drought.size()>0){
	  new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"moistureIndex_changing_times.txt";
	  ofstream file(new_path.c_str(), ios::out | ios::trunc);
	  if(file){
	    for(unsigned i=0; i<tmp_years_drought.size(); i++){ file << tmp_years_drought[i] << endl; }
	    file.close();
	  } else { cerr << "Error : cannot open the file : " << new_path << endl; }

	  /* NEW moisture index mask */
	  for(unsigned i=0; i<newMoistFiles.size(); i++){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"MASK"+delimiter+"moistureIndex_t"+IntToString(ChangingYearsDrought[i])+".asc";
	    copyFiles(newMoistFiles[i].c_str(),new_path.c_str(),".asc");
	    newPathsMoist.push_back(new_path);
	  }

	  /* INDIVIDUAL moisture index changing times */
	  for(unsigned t=0; t<tmp_years_drought.size(); t++){
	    new_path = full_path_str+SimulName+delimiter+"DATA"+delimiter+"SCENARIO"+delimiter+"moistureIndex_changing_masks_t"+IntToString(tmp_years_drought[t])+".txt";
	    ofstream file(new_path.c_str(), ios::out | ios::trunc);
	    if(file){
	      if(find(ChangingYearsDrought.begin(),ChangingYearsDrought.end(),tmp_years_drought[t])!=ChangingYearsDrought.end()){
		file << newPathsMoist[distance(ChangingYearsDrought.begin(),find(ChangingYearsDrought.begin(),ChangingYearsDrought.end(),tmp_years_drought[t]))] << endl;
	      }
	      file.close();
	    } else { cerr << "Error : cannot open the file : " << new_path << endl; }
	  }
	}
	/* ------------------------------------------------------------------------------------------------------------- */
      }

      /* Global parameters file creation */
      string name_file = full_path_str+SimulName+delimiter+"DATA"+delimiter+"Global_parameters.txt";
      writeGlobalParamFile(name_file,NbCPUs,SuccModel,Group.size(),NbStrata,SeedTimeStep,SeedDuration,SimulDuration,
			   is_dist,NbDist,NbRespStagesDist,DistFreq,
			   is_soil,SoilDefaultValue,NbSoilStrata,{0,0},
			   is_fire,NbFires,NbRespStagesFire,FireFreq,initOption,neighOption,propagationOption,quotaOption,
			   initNbFires,{0,0},ccExtent,probFires,0,initLogis,propLogis,0,
			   is_drought,NbRespStagesDrought,DroughtChrono,
			   is_hab,0,
                           is_aliens,{0,0});
      /*for(unsigned rep=0; rep<NbRep; rep++){
        new_path = full_path_str+SimulName+"/DATA/Global_parameters_"+IntToString(rep)+".txt";
        copyFiles(name_file.c_str(),new_path.c_str(),".txt");
	}*/

      /* Namespace constants file creation */
      writeNamespaceConstantsFile();

      /* Parameters simulation file creation */
      name_file = full_path_str+SimulName;
      writeParamSimulFile(name_file,is_savedState,is_savedArrays,is_savedObjects,NbRep,MaskFile,
			  Group,newPathsPFG,
			  DistNames,newPathsDist,
			  FireNames,newPathsFire,newFreqFire,
			  climData_Mask,newPathsClimData,
			  newPathsMoist,
			  climData_Mask,climData_Mask,
                          newPathsAliens,newFreqAliens,
			  is_dist,is_fire,is_drought,is_hab,is_aliens,
			  (tmp_years_PFG.size()>0),(tmp_years_dist.size()>0),(tmp_years_fire.size()>0),(tmp_years_fireFreq.size()>0),
			  (initOption==4),(tmp_years_clim.size()>0),(tmp_years_drought.size()>0),(tmp_years_aliens.size()>0),(tmp_years_aliensFreq.size()>0));
 
      gchar* message = g_locale_to_utf8("Congratulations !\n Your directory of parameters files is ready !",-1,NULL,NULL,NULL);
      errorMessage(message);

    } else {
      gchar* message = g_locale_to_utf8("You must check first \nthat you have submitted\nall the parameters required !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must create \na new simulation !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void NEW_SIMUL (GtkWidget* pButton, gpointer data){
  bool overwrite = FALSE;

  if(IS_PARAM_CREATED){
    gchar* message = g_locale_to_utf8("You have already started a simulation !\n Do you want to overwrite it ?",-1,NULL,NULL,NULL);
    overwrite = carefulMessage(message);
  } else { overwrite = TRUE; }

  if(overwrite){

    IS_PARAM_CREATED = TRUE;

    clearParams();

    GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //menu_bar
    pList = g_list_next(pList);                                                 //WELCOME_BOX
    pList = g_list_next(pList);                                                 //PARAM_BOX
    if(g_list_length(pList)>0){ gtk_widget_destroy(GTK_WIDGET(pList->data)); }

    GtkWidget* main_NoteBook = NULL;

    /* Simulation BOX */
    GtkWidget* BOX_SIMUL_v = NULL;
    vector<GtkWidget*> box_sim_h, box_sim_v, frame_sim, table_sim, label_sim, entry_sim, check_sim, button_sim;
    GtkWidget* scrollbar_sim;

    /* PFG BOX */
    GtkWidget* BOX_PFG_v = NULL;
    vector<GtkWidget*> box_pfg_h, box_pfg_v, frame_pfg, table_pfg, label_pfg, label_info, entry_pfg, check_pfg, radio_pfg, button_pfg;
    GtkWidget* scrollbar_pfg;

    /* Succession BOX */
    GtkWidget* BOX_SUCC_v = NULL;
    vector<GtkWidget*> box_succ_h, box_succ_v, frame_succ, table_succ, label_succ, entry_succ, button_succ, scrollbar_succ;

    GtkWidget* BOX_DIST_v = NULL; /* Disturbances BOX */
    GtkWidget* BOX_SOIL_v = NULL; /* Soil competition BOX */
    GtkWidget* BOX_FIRE_v = NULL; /* Fire disturbance BOX */
    GtkWidget* BOX_HAB_v = NULL; /* Habitat stability BOX */
    GtkWidget* BOX_DROUGHT_v = NULL; /* Drought disturbance BOX */
    GtkWidget* BOX_ALIENS_v = NULL; /* Aliens introduction BOX */

    /* Masks BOX */
    GtkWidget* BOX_MASK_v = NULL;
    GtkWidget* mask_notebook;
    vector<GtkWidget*> BOX_mask_v, box_mask_h, box_mask_v, frame_mask, table_mask, label_mask, entry_mask, button_mask, scrollbar_mask;

    vector<GtkWidget*> listToAdd;

    /* ----------------------------------------------------------------------------------- */
    /* NOTEBOOK CREATION */
    BOX_SIMUL_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_PFG_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_SUCC_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_MASK_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_DIST_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_SOIL_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_FIRE_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_HAB_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_DROUGHT_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
    BOX_ALIENS_v = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);


    vector<gchar*> text_NB = {(gchar*)"<span foreground=\"#A00000\"><b>SIMULATION</b></span>",
			      (gchar*)"<span foreground=\"#A00000\"><b>Plant Functional\n Groups</b></span>",
			      (gchar*)"<span foreground=\"#A00000\"><b>Succession</b></span>",
                              (gchar*)"<span foreground=\"#A00000\"><b>Simulation \nmasks</b></span>",
			      (gchar*)"<i>Disturbances</i>",
			      (gchar*)"<i>Soil \ncompetition</i>",
			      (gchar*)"<i>Fire \ndisturbance</i>",
			      (gchar*)"<i>Habitat \nstability</i>",
			      (gchar*)"<i>Drought \ndisturbance</i>",
                              (gchar*)"<i>Aliens \nintroduction</i>"};
    vector<GtkWidget*> label_NB;
    for(unsigned i=0; i<text_NB.size(); i++){
      label_NB.push_back(gtk_label_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_NB[i]),text_NB[i]);
      gtk_label_set_justify(GTK_LABEL(label_NB[i]), GTK_JUSTIFY_CENTER);
    }
    main_NoteBook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(main_NoteBook), true);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_SIMUL_v, (GtkWidget*) label_NB[0]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_PFG_v, (GtkWidget*) label_NB[1]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_SUCC_v, (GtkWidget*) label_NB[2]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_MASK_v, (GtkWidget*) label_NB[3]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_DIST_v, (GtkWidget*) label_NB[4]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_SOIL_v, (GtkWidget*) label_NB[5]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_FIRE_v, (GtkWidget*) label_NB[6]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_HAB_v, (GtkWidget*) label_NB[7]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_DROUGHT_v, (GtkWidget*) label_NB[8]);
    gtk_notebook_append_page(GTK_NOTEBOOK(main_NoteBook), (GtkWidget*) BOX_ALIENS_v, (GtkWidget*) label_NB[9]);

    gtk_box_pack_start(GTK_BOX((GtkWidget*) data), main_NoteBook, TRUE, TRUE, 0);
    gtk_box_reorder_child(GTK_BOX((GtkWidget*) data), main_NoteBook, 2);

    /* ----------------------------------------------------------------------------------- */
    /* SIMULATION PART */ //OK

    for(unsigned i=0; i<5; i++){
      box_sim_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
      gtk_container_set_border_width(GTK_CONTAINER(box_sim_h[i]), 10);
    }
    gtk_box_set_homogeneous(GTK_BOX(box_sim_h[2]),TRUE);
    gtk_box_set_homogeneous(GTK_BOX(box_sim_h[3]),TRUE);
    gtk_box_set_homogeneous(GTK_BOX(box_sim_h[4]),TRUE);
    for(unsigned i=0; i<4; i++){
      box_sim_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttonsbox*/
      gtk_container_set_border_width(GTK_CONTAINER(box_sim_v[i]), 10);
      gtk_box_set_homogeneous(GTK_BOX(box_sim_v[i]),TRUE);
    }

    for(unsigned i=0; i<4; i++){
      table_sim.push_back(gtk_grid_new());    //Simulationtable, Stratatable, Seedtable, Optiontable
      gtk_container_set_border_width(GTK_CONTAINER(table_sim[i]), 10);		
    }
    gtk_grid_set_column_spacing(GTK_GRID(table_sim[3]), 20);

    GtkWidget* label_tmp;
    vector<gchar*> txt_tmp = {(gchar*)"<b>Simulation &amp; computation parameters</b>",
			      (gchar*)"<b>Strata parameters</b>",
			      (gchar*)"<b>Seed &amp; dispersal module</b>",
			      (gchar*)"<i>Optional modules</i>"};
    for(unsigned i=0; i<4; i++){
      label_tmp = gtk_label_new(NULL);
      frame_sim.push_back(gtk_frame_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_frame_set_label_widget(GTK_FRAME(frame_sim[i]),label_tmp);
      gtk_container_set_border_width(GTK_CONTAINER(frame_sim[i]), 10);
    }
    scrollbar_sim = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar_sim), 5);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_sim), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gchar* name_param_sim[] = {(gchar*)"Simulation name",
			       (gchar*)"Compiled file",
			       (gchar*)"Nb CPUs",
			       (gchar*)"Nb replicates",
			       (gchar*)"Succession model",
			       g_locale_to_utf8("<i>Saved state</i>",-1,NULL,NULL,NULL),
			       (gchar*)"Simulation duration",
			       (gchar*)"Nb strata",
			       (gchar*)"Seedling time step",
			       (gchar*)"Seedling duration",
			       (gchar*)"Nb dist",
			       (gchar*)"Nb resp stages",
			       (gchar*)"Soil default value",
			       (gchar*)"Nb fires",
			       (gchar*)"Nb resp stages",
			       (gchar*)"Nb habitat types",
                               (gchar*)"Nb resp stages"};
    gchar* text_tooltip_sim[] = {g_locale_to_utf8("Simulation name",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Compiled file",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of CPUs required\nto run parallelized parts\nof the code",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Nb replicates",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Succession model :\n fate or fateh (for taking into account environmental conditions)",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Object of previous saved state",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("(years)",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of height strata",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Do seeding every x years",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of years of seeding",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of disturbances",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of response stages",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Soil default value",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of fire disturbances",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of response stages",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of habitat types",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Number of response stages",-1,NULL,NULL,NULL)};

    for(unsigned i=0; i<17; i++){
      label_sim.push_back(gtk_label_new(name_param_sim[i]));
      //tooltip_sim.push_back(gtk_tooltips_new());
      gtk_widget_set_tooltip_text(GTK_WIDGET(label_sim[i]),text_tooltip_sim[i]);
      entry_sim.push_back(gtk_entry_new());
    }
    gtk_label_set_markup(GTK_LABEL(label_sim[5]),name_param_sim[5]);
    for(unsigned i=0; i<10; i++){
      gtk_label_set_justify(GTK_LABEL(label_sim[i]), GTK_JUSTIFY_CENTER);
      gtk_entry_set_width_chars(GTK_ENTRY(entry_sim[i]),15);
    }
    gtk_entry_set_width_chars(GTK_ENTRY(entry_sim[7]),8);
    gtk_entry_set_text(GTK_ENTRY(entry_sim[1]),"FATEHDD_5.8-0");
    gtk_entry_set_text(GTK_ENTRY(entry_sim[4]),"fateh");
    for(unsigned i=10; i<17; i++){
      gtk_widget_set_sensitive(label_sim[i],FALSE);
      gtk_widget_set_sensitive(entry_sim[i],FALSE);
      gtk_entry_set_width_chars(GTK_ENTRY(entry_sim[i]),4);
    }

    gchar* txt_col;

    button_sim.push_back(gtk_button_new_with_label(g_locale_to_utf8("Generate strata\nheights",-1,NULL,NULL,NULL)));
    g_signal_connect(G_OBJECT(button_sim[0]), "clicked", G_CALLBACK(generateStrata), box_sim_v[0]);
    button_sim.push_back(gtk_button_new_with_label("Clear parameters"));
    g_signal_connect(G_OBJECT(button_sim[1]), "clicked", G_CALLBACK(setSimulParamsZero), BOX_SIMUL_v);
    button_sim.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_sim[2]),label_tmp);
    g_signal_connect(G_OBJECT(button_sim[2]), "clicked", G_CALLBACK(submitSimulParams), main_NoteBook);

    gchar* label_check[] = {(gchar*)"Disturbances",(gchar*)"Soil competition",(gchar*)"Fire disturbance",(gchar*)"Habitat stability",(gchar*)"Drought disturbance",(gchar*)"Aliens introduction"};
    for(unsigned i=0; i<6; i++){
      check_sim.push_back(gtk_check_button_new_with_label(label_check[i]));
      g_signal_connect(G_OBJECT(check_sim[i]), "clicked", G_CALLBACK(setCheckSensitiveSim), table_sim[3]);
    }

    /* ----------------------------------------------------------------------------------- */
	  
    gtk_box_pack_start(GTK_BOX(BOX_SIMUL_v), box_sim_h[0], TRUE, FALSE, 0); // 1st line
    listToAdd = {frame_sim[0],frame_sim[1],frame_sim[2]};
    addToBox(box_sim_h[0],listToAdd,TRUE,TRUE);
	  
    /* Simulation frame*/
    gtk_container_add(GTK_CONTAINER(frame_sim[0]), table_sim[0]);
    for(unsigned i=0; i<7; i++){
      gtk_grid_attach(GTK_GRID(table_sim[0]), expandInGrid(label_sim[i]), i%2, 2*(i/2), 1, 1);
    }
    for(unsigned i=0; i<7; i++){
      gtk_grid_attach(GTK_GRID(table_sim[0]), expandInGrid(entry_sim[i]), i%2, 2*(i/2)+1, 1, 1);
    }

    /* Strata height frame */
    gtk_container_add(GTK_CONTAINER(frame_sim[1]), box_sim_v[0]);
    gtk_container_add(GTK_CONTAINER(box_sim_v[0]), box_sim_h[2]);
    listToAdd = {label_sim[7],entry_sim[7],button_sim[0]};
    addToBox(box_sim_h[2],listToAdd,FALSE,FALSE);

    gtk_container_add(GTK_CONTAINER(box_sim_v[0]), scrollbar_sim);
    //gtk_container_add(GTK_CONTAINER(scrollbar_sim), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_sim), gtk_grid_new());

    /* Seed frame */
    gtk_container_add(GTK_CONTAINER(frame_sim[2]), box_sim_v[1]);
    listToAdd = {label_sim[8],entry_sim[8],label_sim[9],entry_sim[9]};
    addToContainer(box_sim_v[1],listToAdd);

    /* ----------------------------------------------------------------------------------- */

    gtk_box_pack_start(GTK_BOX(BOX_SIMUL_v), box_sim_h[1], TRUE, FALSE, 0); // 2nd line

    /* Optional frame */
    gtk_box_pack_start(GTK_BOX(box_sim_h[1]), frame_sim[3], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_sim[3]), table_sim[3]);

    for(unsigned i=0; i<6; i++){
      gtk_grid_attach(GTK_GRID(table_sim[3]), expandInGrid(check_sim[i]), 0, i, 1, 1);
    }
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[10], 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[10], 2, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[11], 3, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[11], 4, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[12], 3, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[12], 4, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[13], 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[13], 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[14], 3, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[14], 4, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[15], 3, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[15], 4, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), label_sim[16], 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_sim[3]), entry_sim[16], 4, 4, 1, 1);

    /* Buttons frame */
    gtk_box_pack_start(GTK_BOX(box_sim_h[1]), box_sim_v[2], FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_sim_v[2]), button_sim[1], TRUE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_sim_v[2]), button_sim[2], TRUE, FALSE, 0);

	  
    /* ----------------------------------------------------------------------------------- */
    /* PLANT FUNCTIONAL GROUP PART */ //OK

    box_pfg_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //Allparametersbox
    box_pfg_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //Optionalparametersbox
    box_pfg_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));      //Buttonsbox
    box_pfg_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));      //Informationstablesbox
    for(unsigned i=0; i<4; i++){ table_pfg.push_back(gtk_grid_new()); }    //Requiredparameter,SeedDispersalparameter,Supplementaryparameter,Informationparameter
    gtk_grid_set_column_spacing(GTK_GRID(table_pfg[2]), 2);
    for(unsigned i=0; i<4; i++){
      gtk_container_set_border_width(GTK_CONTAINER(table_pfg[i]), 10);
    }
    for(unsigned i=0; i<2; i++){
      gtk_container_set_border_width(GTK_CONTAINER(box_pfg_h[i]), 10);
      gtk_container_set_border_width(GTK_CONTAINER(box_pfg_v[i]), 10);
    }

    txt_tmp = {(gchar*)"<b>Required succession parameters</b>",
	       (gchar*)"<b>Required seed &amp; dispersal parameters</b>",
	       (gchar*)"<i>Optional succession parameters</i>",
	       (gchar*)"<i>PFG succession parameters</i>"};
    for(unsigned i=0; i<4; i++){
      label_tmp = gtk_label_new(NULL);
      frame_pfg.push_back(gtk_frame_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_frame_set_label_widget(GTK_FRAME(frame_pfg[i]),label_tmp);
      gtk_container_set_border_width(GTK_CONTAINER(frame_pfg[i]), 10);
    }
    scrollbar_pfg = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar_pfg), 10);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_pfg), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    gchar* name_param[] = {(gchar*)"Group",(gchar*)"Type",(gchar*)"Light",
			   (gchar*)"Height",(gchar*)"Longevity",(gchar*)"Maturity",
			   (gchar*)"Dispersal distances (50,99,LD)",
			   (gchar*)"Max abund",(gchar*)"Imm size",
			   (gchar*)"Wide disp",(gchar*)"Seed dorm",
			   g_locale_to_utf8("Seed pool\n life",-1,NULL,NULL,NULL),
			   (gchar*)"(active)",(gchar*)"(dormant)",
			   (gchar*)"Dispersal",
			   (gchar*)"Palatability",
			   (gchar*)"Soil contrib",(gchar*)"Tol min",(gchar*)"Tol max",
			   (gchar*)"Flammability",
			   (gchar*)"-1.5sd",(gchar*)"+1.5sd",(gchar*)"-2sd",(gchar*)"+2sd",
			   (gchar*)"ModToSev",(gchar*)"SevMort",(gchar*)"Recovery"};
    gchar* name_param_short[] = {(gchar*)"Group",(gchar*)"Type",(gchar*)"Light",
				 (gchar*)"Height",(gchar*)"Longev",(gchar*)"Matur",
				 g_locale_to_utf8("Disp dist \n(50,99,LD)",-1,NULL,NULL,NULL),
				 (gchar*)"Max abun",(gchar*)"Imm size",
				 g_locale_to_utf8("Wide\n dispersed",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Seed\n dormancy",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Seed pool\n life (active)",-1,NULL,NULL,NULL),
				 g_locale_to_utf8("Seed pool\n life (dormant)",-1,NULL,NULL,NULL),
				 (gchar*)"active",(gchar*)"dormant",
				 (gchar*)"Dispersal",
				 (gchar*)"Palatab",
				 (gchar*)"Soil",(gchar*)"Tol min",(gchar*)"Tol max",
				 (gchar*)"Flamm",
				 (gchar*)"-1.5sd",(gchar*)"+1.5sd",(gchar*)"-2sd",(gchar*)"+2sd",
				 (gchar*)"ModToSev",(gchar*)"SevMort",(gchar*)"Recovery"};
    gchar* text_tooltip[] = {g_locale_to_utf8("Name of the PFG",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("H for Herbaceous\nC for Chamaephytes\nP for Phanerophytes",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Integer that will match with\na shade tolerance profile",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Maximal height reached\nby a mature individual\n(centimeters)",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("PFG lifespan, or maximal\nage reached by a mature\nindividual (years)",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Transition age from juvenile\nto mature stage (years)",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Radium distances within\nwhich are dispersed from\nthe tree 50%, 99% and\n100% respectively\nof the seeds (meters)",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Maximum abundance",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Relative size of immature\ncompared to mature plants",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Is the PFG wide dispersed ?",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Seed dormancy",-1,NULL,NULL,NULL),
			     (gchar*)"",
			     g_locale_to_utf8("Seed pool life (active)",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Seed pool life (dormant)",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Dispersal option :\nuniform \ntwo-part negative exponential distribution \n exponential kernel with probability",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Integer that will indicate\nthe tendency of the\nplant to be eaten",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("PFG contribution to the\nsoil composition",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("PFG minimal threshold of\ntolerance to soil",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("PFG maximal threshold of\ntolerance to soil",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Integer that will indicate\nthe tendency of the\nplant to burn",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Minimum limit of Moisture index\nsupported by the PFG\nbefore moderate drought",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Maximum limit of Moisture index\nsupported by the PFG\nbefore moderate drought",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Minimum limit of Moisture index\nsupported by the PFG\nbefore severe drought",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Maximum limit of Moisture index\nsupported by the PFG\nbefore severe drought",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Number of accumulated years\nof drought leading a moderate drought\nto act as a severe one",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Number of accumulated years\nof drought leading a severe drought\nto to cause mortality this year",-1,NULL,NULL,NULL),
			     g_locale_to_utf8("Number of previous drought recovered\nby the PFG if no drought this year",-1,NULL,NULL,NULL)};

    for(unsigned i=0; i<27; i++){
      label_pfg.push_back(gtk_label_new(name_param[i]));
      //tooltip_pfg.push_back(gtk_tooltips_new());
      gtk_widget_set_tooltip_text(GTK_WIDGET(label_pfg[i]),text_tooltip[i]);
      //gtk_tooltips_set_tip(tooltip_pfg[i],label_pfg[i],text_tooltip[i],NULL);
      label_info.push_back(gtk_label_new(name_param_short[i]));
      gtk_label_set_justify(GTK_LABEL(label_info[i]), GTK_JUSTIFY_CENTER);
      gtk_label_set_angle(GTK_LABEL(label_info[i]), 90);
    }
    label_info.push_back(gtk_label_new(name_param_short[27]));
    gtk_label_set_justify(GTK_LABEL(label_info[27]), GTK_JUSTIFY_CENTER);
    gtk_label_set_angle(GTK_LABEL(label_info[27]), 90);
    gtk_label_set_justify(GTK_LABEL(label_pfg[11]), GTK_JUSTIFY_CENTER);

    for(unsigned i=0; i<25; i++){
      entry_pfg.push_back(gtk_entry_new());
      gtk_entry_set_max_length(GTK_ENTRY(entry_pfg[i]),8);
      gtk_entry_set_width_chars(GTK_ENTRY(entry_pfg[i]),8);
    }
    gtk_entry_set_text(GTK_ENTRY(entry_pfg[11]),"0");
    gtk_entry_set_text(GTK_ENTRY(entry_pfg[12]),"0");

    for(unsigned i=13; i<25; i++){
      gtk_widget_set_sensitive(GTK_WIDGET(entry_pfg[i]),FALSE);
    }
    for(unsigned i=15; i<27; i++){
      gtk_widget_set_sensitive(GTK_WIDGET(label_pfg[i]),FALSE);
    }
    gtk_entry_set_text(GTK_ENTRY(entry_pfg[9]),"tmp");
    gtk_entry_set_text(GTK_ENTRY(entry_pfg[10]),"tmp");

    radio_pfg.push_back(gtk_radio_button_new_with_label(NULL, "Yes"));
    radio_pfg.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_pfg[0]), "No"));
    radio_pfg.push_back(gtk_radio_button_new_with_label(NULL, "Yes"));
    radio_pfg.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_pfg[2]), "No"));
    radio_pfg.push_back(gtk_radio_button_new_with_label(NULL, "Uniform"));
    radio_pfg.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_pfg[4]), "Exp Kernel"));
    radio_pfg.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_pfg[4]), "+ proba"));

    button_pfg.push_back(gtk_button_new_with_label("Clear parameters"));
    g_signal_connect(G_OBJECT(button_pfg[0]), "clicked", G_CALLBACK(setParamsZero), BOX_PFG_v);
    button_pfg.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Add new PFG</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_pfg[1]),label_tmp);
    g_signal_connect(G_OBJECT(button_pfg[1]), "clicked", G_CALLBACK(addNewPFG), main_NoteBook);
    button_pfg.push_back(gtk_button_new_with_label("Remove PFG"));
    g_signal_connect(G_OBJECT(button_pfg[2]), "clicked", G_CALLBACK(removePFG), main_NoteBook);
    button_pfg.push_back(gtk_button_new_with_label("Remove all PFGs"));
    g_signal_connect(G_OBJECT(button_pfg[3]), "clicked", G_CALLBACK(removeAllPFG), main_NoteBook);

    /* ----------------------------------------------------------------------------------- */
    gtk_box_pack_start(GTK_BOX(BOX_PFG_v), box_pfg_h[0], FALSE, FALSE, 0); //1st line

    /* First panel : required succession parameters */
    gtk_box_pack_start(GTK_BOX(box_pfg_h[0]), frame_pfg[0], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_pfg[0]), table_pfg[0]);

    for(unsigned i=0; i<6; i++){
      gtk_grid_attach(GTK_GRID(table_pfg[0]), expandInGrid(label_pfg[i]), i%3, 2*(i/3), 1, 1);
    }
    gtk_grid_attach(GTK_GRID(table_pfg[0]), expandInGrid(label_pfg[6]), 0, 4, 3, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[0]), expandInGrid(label_pfg[7]), 0, 6, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[0]), expandInGrid(label_pfg[8]), 1, 6, 1, 1);
    for(unsigned i=0; i<11; i++){
      gtk_grid_attach(GTK_GRID(table_pfg[0]), expandInGrid(entry_pfg[i]), i%3, 2*(i/3)+1, 1, 1);
    }

    /* Second panel : seed & dispersal parameters */
    gtk_box_pack_start(GTK_BOX(box_pfg_h[0]), frame_pfg[1], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_pfg[1]), table_pfg[1]);

    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(label_pfg[9]), 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(label_pfg[10]), 0, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), gtk_label_new(""), 0, 2, 3, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(label_pfg[11]), 0, 3, 1, 2);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(label_pfg[12]), 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(label_pfg[13]), 2, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), gtk_label_new(""), 0, 5, 3, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(label_pfg[14]), 0, 6, 3, 1);

    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[0]), 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[1]), 2, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[2]), 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[3]), 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(entry_pfg[11]), 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(entry_pfg[12]), 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[4]), 0, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[5]), 1, 7, 1, 1);
    gtk_grid_attach(GTK_GRID(table_pfg[1]), expandInGrid(radio_pfg[6]), 2, 7, 1, 1);

    /* Third panel : button box */
    gtk_box_pack_start(GTK_BOX(box_pfg_h[0]), box_pfg_v[0], FALSE, FALSE, 0);
    listToAdd = {button_pfg[0],button_pfg[1],button_pfg[2],button_pfg[3]};
    addToBox(box_pfg_v[0],listToAdd,TRUE,FALSE);

    /* ----------------------------------------------------------------------------------- */
    gtk_box_pack_start(GTK_BOX(BOX_PFG_v), frame_pfg[2], FALSE, FALSE, 0); //2nd line
	  
    /* Optional parameters box */
    gtk_container_add(GTK_CONTAINER(frame_pfg[2]), table_pfg[2]);
    for(unsigned i=0; i<12; i++){
      gtk_grid_attach(GTK_GRID(table_pfg[2]), expandInGrid(label_pfg[i+15]), i, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_pfg[2]), expandInGrid(entry_pfg[i+13]), i, 1, 1, 1);
    }

    /* ----------------------------------------------------------------------------------- */
    gtk_box_pack_start(GTK_BOX(BOX_PFG_v), frame_pfg[3], TRUE, TRUE, 0); //3rd line

    /* Information box */
    gtk_container_add(GTK_CONTAINER(frame_pfg[3]), scrollbar_pfg);
    //gtk_container_add(GTK_CONTAINER(scrollbar_pfg), box_pfg_v[1]);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_pfg), box_pfg_v[1]);
    gtk_box_pack_start(GTK_BOX(box_pfg_v[1]), table_pfg[3], FALSE, FALSE, 0);

    gtk_grid_attach(GTK_GRID(table_pfg[3]), expandInGrid(gtk_label_new("")), 0, 0, 1, 1);
    for(unsigned i=0; i<6; i++){
      gtk_grid_attach(GTK_GRID(table_pfg[3]), expandInGrid(label_info[i]), i+1, 0, 1, 1);
    }
    gtk_grid_attach(GTK_GRID(table_pfg[3]), expandInGrid(label_info[6]), 7, 0, 3, 1);
    for(unsigned i=7; i<13; i++){
      gtk_grid_attach(GTK_GRID(table_pfg[3]), expandInGrid(label_info[i]), i+3, 0, 1, 1);
    }
    for(unsigned i=15; i<28; i++){
      gtk_grid_attach(GTK_GRID(table_pfg[3]), expandInGrid(label_info[i]), i+1, 0, 1, 1);
    }

    
    /* ----------------------------------------------------------------------------------- */
    /* SUCCESSION PART */

    box_succ_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //TOT
    gtk_container_set_border_width(GTK_CONTAINER(box_succ_h[0]), 5);
    box_succ_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //BOX1
    box_succ_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttonsbox1
    gtk_container_set_border_width(GTK_CONTAINER(box_succ_v[1]), 10);
    box_succ_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //BOX2
    box_succ_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttonsbox2
    gtk_container_set_border_width(GTK_CONTAINER(box_succ_v[3]), 10);

    txt_tmp = {(gchar*)"<b>Changing strata ages</b>",(gchar*)"<b>Light dependant parameters</b>"};
    for(unsigned i=0; i<2; i++){
      label_tmp = gtk_label_new(NULL);
      frame_succ.push_back(gtk_frame_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_frame_set_label_widget(GTK_FRAME(frame_succ[i]),label_tmp);
      gtk_container_set_border_width(GTK_CONTAINER(frame_succ[i]), 10);
    }
    scrollbar_succ.push_back(gtk_scrolled_window_new(NULL, NULL));
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar_succ[0]), 10);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_succ[0]), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    scrollbar_succ.push_back(gtk_scrolled_window_new(NULL, NULL));
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar_succ[1]), 10);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_succ[1]), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    button_succ.push_back(gtk_button_new_with_label("Generate strata"));
    g_signal_connect(G_OBJECT(button_succ[0]), "clicked", G_CALLBACK(generateStrataAges), box_succ_v[0]);
    button_succ.push_back(gtk_button_new_with_label("Clear parameters"));
    g_signal_connect(G_OBJECT(button_succ[1]), "clicked", G_CALLBACK(setSucc1ParamsZero), box_succ_v[0]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_succ[1]),FALSE);
    button_succ.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_succ[2]),label_tmp);
    g_signal_connect(G_OBJECT(button_succ[2]), "clicked", G_CALLBACK(submitSucc1Params), scrollbar_succ[0]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_succ[2]),FALSE);
    button_succ.push_back(gtk_button_new_with_label("Generate light stages"));
    g_signal_connect(G_OBJECT(button_succ[3]), "clicked", G_CALLBACK(generateLightStages), box_succ_v[2]);
    button_succ.push_back(gtk_button_new_with_label("Clear parameters"));
    g_signal_connect(G_OBJECT(button_succ[4]), "clicked", G_CALLBACK(setSucc2ParamsZero), scrollbar_succ[1]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_succ[4]),FALSE);
    button_succ.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_succ[5]),label_tmp);
    g_signal_connect(G_OBJECT(button_succ[5]), "clicked", G_CALLBACK(submitSucc2Params), scrollbar_succ[1]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_succ[5]),FALSE);

    gtk_box_pack_start(GTK_BOX(BOX_SUCC_v), box_succ_h[0], TRUE, TRUE, 0);

    /* ----------------------------------------------------------------------------------- */
    /* FIRST PANEL : AGES BUTTONS */
    gtk_box_pack_start(GTK_BOX(box_succ_h[0]), box_succ_v[0], TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(box_succ_v[0]), box_succ_v[1], FALSE, FALSE, 0);
    listToAdd = {button_succ[0],button_succ[1],button_succ[2]};
    addToBox(box_succ_v[1],listToAdd,FALSE,FALSE);

    /* ----------------------------------------------------------------------------------- */
    /* SECOND PANEL : AGES CHANGING STRATA PARAMETERS */
    gtk_box_pack_start(GTK_BOX(box_succ_v[0]), frame_succ[0], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_succ[0]), scrollbar_succ[0]);
    //gtk_container_add(GTK_CONTAINER(scrollbar_succ[0]), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_succ[0]), gtk_grid_new());

    /* ----------------------------------------------------------------------------------- */
    /* THIRD PANEL : LIGHT BUTTONS */
    gtk_box_pack_start(GTK_BOX(box_succ_h[0]), box_succ_v[2], TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(box_succ_v[2]), box_succ_v[3], FALSE, FALSE, 0);
    listToAdd = {button_succ[3],button_succ[4],button_succ[5]};
    addToBox(box_succ_v[3],listToAdd,FALSE,FALSE);

    /* ----------------------------------------------------------------------------------- */
    /* FOURTH PANEL : LIGHT RESPONSE STAGES PARAMETERS */
    gtk_box_pack_start(GTK_BOX(box_succ_v[2]), frame_succ[1], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_succ[1]), scrollbar_succ[1]);
    //gtk_container_add(GTK_CONTAINER(scrollbar_succ[1]), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_succ[1]), gtk_grid_new());


    /* ----------------------------------------------------------------------------------- */
    /* MASKS PART */

    BOX_mask_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    BOX_mask_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
    BOX_mask_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));

    box_mask_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
    box_mask_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
    gtk_container_set_border_width(GTK_CONTAINER(box_mask_h[1]), 20);
    box_mask_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
    gtk_container_set_border_width(GTK_CONTAINER(box_mask_h[2]), 20);
    for(unsigned i=0; i<3; i++){
      box_mask_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));
      gtk_container_set_border_width(GTK_CONTAINER(box_mask_v[i]), 10);
    }

    table_mask.push_back(gtk_grid_new());
    gtk_container_set_border_width(GTK_CONTAINER(table_mask[0]), 10);

    txt_tmp = {(gchar*)"<b>Simulation</b>",
	       (gchar*)"<b>PFG masks</b>",
	       (gchar*)"<b>Disturbances masks</b>",
	       (gchar*)"<b>Fire masks</b>"};
    for(unsigned i=0; i<4; i++){
      label_tmp = gtk_label_new(NULL);
      frame_mask.push_back(gtk_frame_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_frame_set_label_widget(GTK_FRAME(frame_mask[i]),label_tmp);
      gtk_container_set_border_width(GTK_CONTAINER(frame_mask[i]), 10);
    }
    for(unsigned i=0; i<3; i++) { entry_mask.push_back(gtk_entry_new()); }

    button_mask.push_back(gtk_button_new_with_label("Select mask file"));
    g_signal_connect(G_OBJECT(button_mask[0]), "clicked", G_CALLBACK(selectMaskFile), entry_mask[0]);
    button_mask.push_back(gtk_button_new_with_label("Select array saving years file"));
    g_signal_connect(G_OBJECT(button_mask[1]), "clicked", G_CALLBACK(selectMaskFile), entry_mask[1]);
    button_mask.push_back(gtk_button_new_with_label("Select objects saving years file"));
    g_signal_connect(G_OBJECT(button_mask[2]), "clicked", G_CALLBACK(selectMaskFile), entry_mask[2]);

    button_mask.push_back(gtk_button_new_with_label("Generate PFG masks"));
    g_signal_connect(G_OBJECT(button_mask[3]), "clicked", G_CALLBACK(generateMaskPFG), BOX_mask_v[0]);
    button_mask.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_mask[4]),label_tmp);
    g_signal_connect(G_OBJECT(button_mask[4]), "clicked", G_CALLBACK(submitMask1Params), BOX_mask_v[0]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_mask[4]),FALSE);
    button_mask.push_back(gtk_button_new_with_label("Generate disturbances masks"));
    g_signal_connect(G_OBJECT(button_mask[5]), "clicked", G_CALLBACK(generateMaskDist), BOX_mask_v[1]);
    button_mask.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_mask[6]),label_tmp);
    g_signal_connect(G_OBJECT(button_mask[6]), "clicked", G_CALLBACK(submitMask2Params), frame_mask[2]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_mask[6]),FALSE);
    button_mask.push_back(gtk_button_new_with_label("Generate fire masks"));
    g_signal_connect(G_OBJECT(button_mask[7]), "clicked", G_CALLBACK(generateMaskFire), BOX_mask_v[2]);
    button_mask.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_mask[8]),label_tmp);
    g_signal_connect(G_OBJECT(button_mask[8]), "clicked", G_CALLBACK(submitMask3Params), frame_mask[3]);
    gtk_widget_set_sensitive(GTK_WIDGET(button_mask[8]),FALSE);

    txt_tmp = {(gchar*)"<span foreground=\"#A00000\"><b>Simulation &amp; PFGs</b></span>",
	       (gchar*)"<i>Disturbances</i>",
	       (gchar*)"<i>Fires</i>"};

    mask_notebook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(mask_notebook), true);
    gtk_container_set_border_width(GTK_CONTAINER(mask_notebook), 10);
    for(unsigned i=0; i<3; i++){
      label_tmp = gtk_label_new(NULL);
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_notebook_append_page(GTK_NOTEBOOK(mask_notebook), (GtkWidget*) BOX_mask_v[i], label_tmp);		
    }
    gtk_box_pack_start(GTK_BOX(BOX_MASK_v), mask_notebook, TRUE, TRUE, 0);

    /* ----------------------------------------------------------------------------------- */
    /* FIRST PANEL : simulation */
    gtk_box_pack_start(GTK_BOX(BOX_mask_v[0]), frame_mask[0],FALSE, FALSE, 0);
    gtk_container_add(GTK_CONTAINER(frame_mask[0]), box_mask_h[0]);
    gtk_box_pack_start(GTK_BOX(box_mask_h[0]), table_mask[0], FALSE, FALSE, 0);

    for(unsigned i=0; i<3; i++) {
      gtk_grid_attach(GTK_GRID(table_mask[0]), button_mask[i], i, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_mask[0]), entry_mask[i], i, 1, 1, 1);
    }

    gtk_container_add(GTK_CONTAINER(box_mask_h[0]), box_mask_v[0]);
    gtk_box_pack_start(GTK_BOX(box_mask_v[0]), button_mask[3], TRUE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_mask_v[0]), button_mask[4], TRUE, FALSE, 0);

    /* ----------------------------------------------------------------------------------- */
    /* SECOND PANEL : PFG masks */
    gtk_box_pack_start(GTK_BOX(BOX_mask_v[0]), frame_mask[1], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_mask[1]), gtk_grid_new());

    /* ----------------------------------------------------------------------------------- */
    /* THIRD PANEL : Disturbances masks */
    gtk_box_pack_start(GTK_BOX(BOX_mask_v[1]), box_mask_h[1], FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_mask_h[1]), button_mask[5], FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_mask_h[1]), button_mask[6], FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(BOX_mask_v[1]), frame_mask[2], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_mask[2]), gtk_grid_new());

    /* ----------------------------------------------------------------------------------- */
    /* FOURTH PANEL : Fire masks */
    gtk_box_pack_start(GTK_BOX(BOX_mask_v[2]), box_mask_h[2], FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_mask_h[2]), button_mask[7], FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_mask_h[2]), button_mask[8], FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(BOX_mask_v[2]), frame_mask[3], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_mask[3]), gtk_grid_new());

    /* ----------------------------------------------------------------------------------- */
    gtk_widget_show_all(main_NoteBook);
  }
}

/*############################################################################################################################*/

void submitSimulParams(GtkWidget* pButton, gpointer data)
{
  string temp_simulName, temp_succModel, temp_compiledFile, temp_savedState;
  unsigned temp_nbCPUs, temp_nbRep, temp_simulDuration, temp_nbStrata;
  vector<unsigned> temp_heightStrata;
  unsigned temp_seedTimeStep, temp_seedDuration, temp_nbDist=0, temp_nbRespStagesDist=0, temp_nbFires=0, temp_nbRespStagesFire=0, temp_nbRespStagesDrought=0, temp_nbHab=0;
  double temp_soilDefVal=0.0;

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_sim_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_sim[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_sim[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  for(unsigned i=0; i<7; i++) { ppList = g_list_next(ppList); }        //labels
  if((gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->data))>0))
    {
      temp_simulName = gtk_entry_get_text(GTK_ENTRY(ppList->data));
      temp_compiledFile = gtk_entry_get_text(GTK_ENTRY(ppList->next->data));
      temp_nbCPUs = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data)));
      temp_nbRep = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->data)));
      temp_succModel = gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->data));
      temp_savedState = gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->next->data));
      temp_simulDuration = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->next->next->data)));

      ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_sim[0]
      ppList = g_list_next(ppList);                                       //frame_sim[1]
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //box_sim_v[0]
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //box_sim_h[2]
      GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      if(gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0){
	temp_nbStrata = atoi(gtk_entry_get_text(GTK_ENTRY(pppList->next->data)));

	ppList = g_list_next(ppList);                                       //scrollbar_sim
	ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewport
	ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable?
	ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
	if(g_list_length(ppList)>0) {
	  ppList = g_list_reverse(ppList);
	  for(unsigned i=0; i<(temp_nbStrata+1); i++) { ppList = g_list_next(ppList); }
	  unsigned compt = 0;
	  pppList = ppList;
	  for(unsigned i=0; i<(temp_nbStrata+1); i++) {
	    if(gtk_entry_get_text_length(GTK_ENTRY(pppList->data))>0) { compt++; }
	    pppList = g_list_next(pppList);
	  }
	  if(compt==(temp_nbStrata+1)){
	    for(unsigned i=0; i<(temp_nbStrata+1); i++) {
	      temp_heightStrata.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	      ppList = g_list_next(ppList);
	    }

            ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_sim[0]
            ppList = g_list_next(ppList);                                       //frame_sim[1]
            ppList = g_list_next(ppList);                                       //frame_sim[2]
	    pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));  //box_sim_v[1]
	    pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));
	    if((gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0) &
	       (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->data))>0))
	      {
		temp_seedTimeStep = atoi(gtk_entry_get_text(GTK_ENTRY(pppList->next->data)));
		temp_seedDuration = atoi(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->data)));

		pList = g_list_next(pList);                                       //box_sim_h[1]
		ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));  //frame_sim[3]
		pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));  //table_sim[3]
		pppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));
		pppList = g_list_reverse(pppList);

		GList* ppppList;
		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->data))){
		  ppppList = pppList;
		  for(unsigned i=0; i<7; i++) { ppppList = g_list_next(ppppList); }
		  if((gtk_entry_get_text_length(GTK_ENTRY(ppppList->data))>0) &
		     (gtk_entry_get_text_length(GTK_ENTRY(ppppList->next->next->data))>0)) {
		    temp_nbDist = atoi(gtk_entry_get_text(GTK_ENTRY(ppppList->data)));
		    temp_nbRespStagesDist = atoi(gtk_entry_get_text(GTK_ENTRY(ppppList->next->next->data)));
		  } else {
		    gchar* message = g_locale_to_utf8("You must fill the \ndisturbance parameters !",-1,NULL,NULL,NULL);
		    errorMessage(message);
		    return;
		  }
		}

		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->data))){
		  ppppList = pppList;
		  for(unsigned i=0; i<11; i++) { ppppList = g_list_next(ppppList); }
		  if(gtk_entry_get_text_length(GTK_ENTRY(ppppList->data))>0){
		    temp_soilDefVal = atof(gtk_entry_get_text(GTK_ENTRY(ppppList->data)));
		  } else {
		    gchar* message = g_locale_to_utf8("You must fill the \nsoil parameters !",-1,NULL,NULL,NULL);
		    errorMessage(message);
		    return;
		  }
		}

		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->data))){
		  ppppList = pppList;
		  for(unsigned i=0; i<13; i++) { ppppList = g_list_next(ppppList); }
		  if((gtk_entry_get_text_length(GTK_ENTRY(ppppList->data))>0) &
		     (gtk_entry_get_text_length(GTK_ENTRY(ppppList->next->next->data))>0)) {
		    temp_nbFires = atoi(gtk_entry_get_text(GTK_ENTRY(ppppList->data)));
		    temp_nbRespStagesFire = atoi(gtk_entry_get_text(GTK_ENTRY(ppppList->next->next->data)));
		  } else {
		    gchar* message = g_locale_to_utf8("You must fill the \nfire parameters !",-1,NULL,NULL,NULL);
		    errorMessage(message);
		    return;
		  }
		}

		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->next->data))){
		  ppppList = pppList;
		  for(unsigned i=0; i<17; i++) { ppppList = g_list_next(ppppList); }
		  if(gtk_entry_get_text_length(GTK_ENTRY(ppppList->data))>0){
                    temp_nbHab = atoi(gtk_entry_get_text(GTK_ENTRY(ppppList->data)));
		  } else {
		    gchar* message = g_locale_to_utf8("You must fill the \nhabitat parameters !",-1,NULL,NULL,NULL);
		    errorMessage(message);
		    return;
		  }
		}

		if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->next->next->data))){
		  ppppList = pppList;
		  for(unsigned i=0; i<19; i++) { ppppList = g_list_next(ppppList); }
		  if(gtk_entry_get_text_length(GTK_ENTRY(ppppList->data))>0){
		    temp_nbRespStagesDrought = atoi(gtk_entry_get_text(GTK_ENTRY(ppppList->data)));
		  } else {
		    gchar* message = g_locale_to_utf8("You must fill the \ndrought parameters !",-1,NULL,NULL,NULL);
		    errorMessage(message);
		    return;
		  }
		}

		SimulName = temp_simulName;
		CompiledFile = temp_compiledFile;
		NbCPUs = temp_nbCPUs;
		NbRep = temp_nbRep;
		SuccModel = temp_succModel;
		SavedState = temp_savedState;
		if(temp_savedState.length()>0){ is_savedState = true; }
		SimulDuration = temp_simulDuration;
		NbStrata = temp_nbStrata;
		HeightStrata.clear();
		HeightStrata = temp_heightStrata;
		SeedTimeStep = temp_seedTimeStep;
		SeedDuration = temp_seedDuration;
		is_dist = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->data));
		is_soil = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->data));
		is_fire = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->data));
		is_hab = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->next->data));
		is_drought = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->next->next->data));
		is_aliens = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->next->next->next->next->next->data));
		NbDist = temp_nbDist;
		NbRespStagesDist = temp_nbRespStagesDist;
		SoilDefaultValue = temp_soilDefVal;
		NbFires = temp_nbFires;
		NbRespStagesFire = temp_nbRespStagesFire;
		NbHabitats = temp_nbHab;
		NbRespStagesDrought = temp_nbRespStagesDrought;

		submit_simul = TRUE;
                GtkWidget* label_tmp = gtk_label_new(NULL);
		gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
		gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
		gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);

	      } else {
	      gchar* message = g_locale_to_utf8("You must fill the \nseed parameters !",-1,NULL,NULL,NULL);
	      errorMessage(message);
	      return;
	    }
	  } else {
	    gchar* message = g_locale_to_utf8("You must fill the \nstrata parameters !",-1,NULL,NULL,NULL);
	    errorMessage(message);
	    return;
	  }
	} else {
	  gchar* message = g_locale_to_utf8("You must fill the \nstrata parameters !",-1,NULL,NULL,NULL);
	  errorMessage(message);
	  return;
	}
      } else {
	gchar* message = g_locale_to_utf8("You must fill the \nstrata parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
	return;
      }
    } else {
    gchar* message = g_locale_to_utf8("You must fill the \ncomputation parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
    return;
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //box_pfg_h[0]
  pList = g_list_next(pList);                                             //frame_pfg[2]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	  //table_pfg[2]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  pList = g_list_reverse(pList);

  /* Set sensitive=TRUE optional succession parameters */
  gtk_widget_set_sensitive(GTK_WIDGET(pList->data),is_dist);
  gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data),is_dist);
  ppList = pList;
  for(unsigned i=0; i<2; i++){ ppList = g_list_next(ppList); }
  for(unsigned i=0; i<6; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data),is_soil);
    ppList = g_list_next(ppList);
  }
  ppList = pList;
  for(unsigned i=0; i<8; i++){ ppList = g_list_next(ppList); }
  for(unsigned i=0; i<2; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data),is_fire);
    ppList = g_list_next(ppList);
  }
  ppList = pList;
  for(unsigned i=0; i<10; i++){ ppList = g_list_next(ppList); }
  for(unsigned i=0; i<14; i++){
    gtk_widget_set_sensitive(GTK_WIDGET(ppList->data),is_drought);
    ppList = g_list_next(ppList);
  }


  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  if(is_dist){
    GtkWidget* dist_NoteBook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(dist_NoteBook), true);
    gtk_box_pack_start(GTK_BOX(pList->data), dist_NoteBook, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(dist_NoteBook), 10);

    vector<GtkWidget*> new_box_v;
    DistNames.clear();
    DistFreq.clear();
    PropKilledProp.clear();
    PercentActivSeeds.clear();
    BreakAges.clear();
    ResprAges.clear();
    PropKilledInd.clear();
    PropResproutInd.clear();
    submit_dist.clear();

    DistNames.resize(NbDist);
    DistFreq.resize(NbDist);
    DistNames.resize(NbDist);
    PropKilledProp.resize(NbDist);
    PercentActivSeeds.resize(NbDist);
    BreakAges.resize(NbDist);
    ResprAges.resize(NbDist);
    PropKilledInd.resize(NbDist);
    PropResproutInd.resize(NbDist);
    submit_dist.resize(NbDist);

    for(unsigned i=0; i<NbDist; i++){
      new_box_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL,0));
      gtk_notebook_append_page(GTK_NOTEBOOK(dist_NoteBook), new_box_v[i], (GtkWidget*) gtk_label_new(("Dist"+IntToString(i)).c_str()));

      vector<GtkWidget*> box_dist_h;
      vector<GtkWidget*> box_dist_v;
      vector<GtkWidget*> frame_dist;
      vector<GtkWidget*> table_dist;
      vector<GtkWidget*> label_dist;
      vector<GtkWidget*> entry_dist;
      vector<GtkWidget*> check_dist;
      vector<GtkWidget*> button_dist;
      GtkWidget* scrollbar_dist;

      /* ----------------------------------------------------------------------------------- */
      /* DISTURBANCES PART */

      box_dist_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //Basicparametersbox
      gtk_container_set_border_width(GTK_CONTAINER(box_dist_h[0]), 10);
      box_dist_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttonsbox
      table_dist.push_back(gtk_grid_new());    //Requiredparameterstable
      gtk_container_set_border_width(GTK_CONTAINER(table_dist[0]), 10);

      GtkWidget* label_tmp;
      vector<gchar*> txt_tmp = {(gchar*)"<b>New disturbance parameters</b>",
				(gchar*)"<b>Response stages parameters</b>"};
      for(unsigned i=0; i<2; i++){
	label_tmp = gtk_label_new(NULL);
	frame_dist.push_back(gtk_frame_new(NULL));
	gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
	gtk_frame_set_label_widget(GTK_FRAME(frame_dist[i]),label_tmp);
	gtk_container_set_border_width(GTK_CONTAINER(frame_dist[i]), 10);
      }
      scrollbar_dist = gtk_scrolled_window_new(NULL, NULL);
      gtk_container_set_border_width(GTK_CONTAINER(scrollbar_dist), 10);
      gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_dist), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

      gchar* name_param_dist[] = {(gchar*)"NAME",(gchar*)"Frequency",(gchar*) g_locale_to_utf8("Proportion\n of killed\n propagules",-1,NULL,NULL,NULL),
				  (gchar*) g_locale_to_utf8("Percentage\n of activated\n seeds",-1,NULL,NULL,NULL)};
      for(unsigned j=0; j<4; j++){
	label_dist.push_back(gtk_label_new(name_param_dist[j]));
	gtk_label_set_justify(GTK_LABEL(label_dist[j]), GTK_JUSTIFY_CENTER);
	entry_dist.push_back(gtk_entry_new());
	gtk_entry_set_max_length(GTK_ENTRY(entry_dist[j]),8);
	gtk_entry_set_width_chars(GTK_ENTRY(entry_dist[j]),8);
      }

      button_dist.push_back(gtk_button_new_with_label("Generate response stages"));
      g_signal_connect(G_OBJECT(button_dist[0]), "clicked", G_CALLBACK(generateRespStages), dist_NoteBook);
      button_dist.push_back(gtk_button_new_with_label("Clear parameters"));
      g_signal_connect(G_OBJECT(button_dist[1]), "clicked", G_CALLBACK(setDistParamsZero), dist_NoteBook);
      button_dist.push_back(gtk_button_new_with_label(NULL));
      label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(button_dist[2]),label_tmp);
      g_signal_connect(G_OBJECT(button_dist[2]), "clicked", G_CALLBACK(submitPertParams), dist_NoteBook);
      gtk_widget_set_sensitive(button_dist[2],FALSE);

      /* ----------------------------------------------------------------------------------- */
      /* FIRST PANEL : BASIC PARAMETERS */
      gtk_box_pack_start(GTK_BOX(new_box_v[i]), box_dist_h[0], FALSE, FALSE, 0);

      gtk_box_pack_start(GTK_BOX(box_dist_h[0]), frame_dist[0], TRUE, FALSE, 0);
      gtk_container_add(GTK_CONTAINER(frame_dist[0]), table_dist[0]);

      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[0], 0, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[1], 1, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[2], 2, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[3], 3, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[0], 0, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[1], 1, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[2], 2, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[3], 3, 1, 1, 1);

      gtk_box_pack_start(GTK_BOX(box_dist_h[0]), box_dist_v[0], TRUE, FALSE, 0);
      vector<GtkWidget*> listToAdd = {button_dist[0],button_dist[1],button_dist[2]};
      addToBox(box_dist_v[0],listToAdd,TRUE,FALSE);
 
      /* ----------------------------------------------------------------------------------- */
      /* SECOND PANEL : RESPONSE STAGES PARAMETERS */
      gtk_box_pack_start(GTK_BOX(new_box_v[i]), frame_dist[1], TRUE, TRUE, 0);

      gtk_container_add(GTK_CONTAINER(frame_dist[1]), scrollbar_dist);
      //gtk_container_add(GTK_CONTAINER(scrollbar_dist), gtk_grid_new());
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_dist), gtk_grid_new());
    }
    gtk_widget_show_all(dist_NoteBook);
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  pList = g_list_next(pList);                                             //BOX_SOIL_V
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  if(is_soil){

    GtkWidget* new_box_v = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_box_pack_start(GTK_BOX(pList->data), new_box_v, TRUE, TRUE, 0);

    vector<GtkWidget*> box_soil_h;
    vector<GtkWidget*> frame_soil;
    vector<GtkWidget*> table_soil;
    vector<GtkWidget*> label_soil;
    vector<GtkWidget*> entry_soil;
    vector<GtkWidget*> button_soil;
    vector<GtkWidget*> scrollbar_soil;

    /* ----------------------------------------------------------------------------------- */
    /* SOIL PART */

    box_soil_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //StrataBox
    box_soil_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //ButtonBox

    table_soil.push_back(gtk_grid_new());    //NbStrata
    gtk_container_set_border_width(GTK_CONTAINER(table_soil[0]), 10);
    table_soil.push_back(gtk_grid_new());    //Buttons
    gtk_container_set_border_width(GTK_CONTAINER(table_soil[1]), 10);

    scrollbar_soil.push_back(gtk_scrolled_window_new(NULL, NULL));
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_soil[0]), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    scrollbar_soil.push_back(gtk_scrolled_window_new(NULL, NULL));
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar_soil[1]), 10);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_soil[1]), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    GtkWidget* label_tmp;
    vector<gchar*> txt_tmp = {(gchar*)"<b>Strata parameters</b>",(gchar*)"<b>Soil dependant parameters</b>"};
    for(unsigned i=0; i<2; i++){
      label_tmp = gtk_label_new(NULL);
      frame_soil.push_back(gtk_frame_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_frame_set_label_widget(GTK_FRAME(frame_soil[i]),label_tmp);
      gtk_container_set_border_width(GTK_CONTAINER(frame_soil[i]), 10);
    }

    button_soil.push_back(gtk_button_new_with_label("Generate strata heights"));
    g_signal_connect(G_OBJECT(button_soil[0]), "clicked", G_CALLBACK(generateStrataSoil), table_soil[0]);
    button_soil.push_back(gtk_button_new_with_label("Generate soil stages"));
    g_signal_connect(G_OBJECT(button_soil[1]), "clicked", G_CALLBACK(generateSoilStages), new_box_v);
    button_soil.push_back(gtk_button_new_with_label("Clear parameters"));
    g_signal_connect(G_OBJECT(button_soil[2]), "clicked", G_CALLBACK(setSoilParamsZero), new_box_v);
    button_soil.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_soil[3]),label_tmp);
    g_signal_connect(G_OBJECT(button_soil[3]), "clicked", G_CALLBACK(submitSoilParams), new_box_v);
    gtk_widget_set_sensitive(button_soil[3],FALSE);

    /* ----------------------------------------------------------------------------------- */
    gtk_box_pack_start(GTK_BOX(new_box_v), frame_soil[0], FALSE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(new_box_v), frame_soil[1], TRUE, TRUE, 0);
    gtk_box_pack_start(GTK_BOX(new_box_v), table_soil[1], FALSE, FALSE, 0);

    /* ----------------------------------------------------------------------------------- */
    /* FIRST PANEL : STRATA PARAMETERS */

    gtk_container_add(GTK_CONTAINER(frame_soil[0]), table_soil[0]);
    gtk_grid_attach(GTK_GRID(table_soil[0]), gtk_label_new("Nb strata"), 0, 0, 2, 3);
    gtk_grid_attach(GTK_GRID(table_soil[0]), gtk_entry_new(), 0, 3, 2, 1);
    gtk_grid_attach(GTK_GRID(table_soil[0]), button_soil[0], 2, 2, 2, 2);
    gtk_grid_attach(GTK_GRID(table_soil[0]), scrollbar_soil[0], 4, 0, 6, 4);
    //gtk_container_add(GTK_CONTAINER(scrollbar_soil[0]), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_soil[0]), gtk_grid_new());

    /* ----------------------------------------------------------------------------------- */
    /* SECOND PANEL : SOIL TOLERANCE PARAMETERS */

    gtk_container_add(GTK_CONTAINER(frame_soil[1]), scrollbar_soil[1]);
    //gtk_container_add(GTK_CONTAINER(scrollbar_soil[1]), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_soil[1]), gtk_grid_new());

    /* ----------------------------------------------------------------------------------- */
    /* THIRD PANEL : BUTTONS */

    gtk_grid_attach(GTK_GRID(table_soil[1]), button_soil[1], 0, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_soil[1]), button_soil[2], 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_soil[1]), button_soil[3], 2, 0, 1, 1);
    gtk_widget_show_all(GTK_WIDGET(new_box_v));
  }


  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  pList = g_list_next(pList);                                             //BOX_SOIL_V
  pList = g_list_next(pList);                                             //BOX_FIRE_V
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  if(is_fire){

    FireNames.clear();
    FireFreq.clear();
    FirePropKilledProp.clear();
    FirePercentActivSeeds.clear();
    FireBreakAges.clear();
    FireResprAges.clear();
    FirePropKilledInd.clear();
    FirePropResproutInd.clear();
    submit_fire.clear();

    FireNames.resize(NbFires);
    FireFreq.resize(NbFires);
    FirePropKilledProp.resize(NbFires);
    FirePercentActivSeeds.resize(NbFires);
    FireBreakAges.resize(NbFires);
    FireResprAges.resize(NbFires);
    FirePropKilledInd.resize(NbFires);
    FirePropResproutInd.resize(NbFires);
    submit_fire.resize(NbFires);

    GtkWidget* fire_NoteBook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(fire_NoteBook), true);
    gtk_box_pack_start(GTK_BOX(pList->data), fire_NoteBook, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(fire_NoteBook), 10);

    GtkWidget* new_box_v = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
    gtk_notebook_append_page(GTK_NOTEBOOK(fire_NoteBook), new_box_v, (GtkWidget*) gtk_label_new("Fire options"));

    vector<GtkWidget*> box_fire_h;
    vector<GtkWidget*> box_fire_v;
    vector<GtkWidget*> frame_fire;
    vector<GtkWidget*> table_fire;
    vector<GtkWidget*> label_fire;
    vector<GtkWidget*> entry_fire;
    vector<GtkWidget*> radio_fire;
    vector<GtkWidget*> button_fire;
    GtkWidget* scrollbar_fire;

    /* ----------------------------------------------------------------------------------- */
    /* FIRE PART */

    box_fire_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //IgnitionNeighbour
    box_fire_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //PropagationButtons
    box_fire_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttons
    gtk_container_set_border_width(GTK_CONTAINER(box_fire_v[0]), 30);

    table_fire.push_back(gtk_grid_new());    //Ignition
    gtk_container_set_border_width(GTK_CONTAINER(table_fire[0]), 10);
    table_fire.push_back(gtk_grid_new());    //Neighbour
    gtk_container_set_border_width(GTK_CONTAINER(table_fire[1]), 10);
    table_fire.push_back(gtk_grid_new());    //Quota
    gtk_container_set_border_width(GTK_CONTAINER(table_fire[2]), 10);
    table_fire.push_back(gtk_grid_new());    //Propagation
    gtk_container_set_border_width(GTK_CONTAINER(table_fire[3]), 10);
    table_fire.push_back(gtk_grid_new());    //IntensityProbfires
    gtk_container_set_border_width(GTK_CONTAINER(table_fire[4]), 10);

    scrollbar_fire = gtk_scrolled_window_new(NULL, NULL);
    gtk_container_set_border_width(GTK_CONTAINER(scrollbar_fire), 10);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_fire), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

    GtkWidget* label_tmp;
    vector<gchar*> txt_tmp = {(gchar*)"<b>Ignition option</b>",
			      (gchar*)"<b>Neighbour option</b>",
			      (gchar*)"<b>Quota option</b>",
			      (gchar*)"<b>Propagation option</b>",
			      (gchar*)"<b>Probability of dispersal</b>"};
    for(unsigned i=0; i<5; i++){
      label_tmp = gtk_label_new(NULL);
      frame_fire.push_back(gtk_frame_new(NULL));
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
      gtk_frame_set_label_widget(GTK_FRAME(frame_fire[i]),label_tmp);
      gtk_container_set_border_width(GTK_CONTAINER(frame_fire[i]), 10);
    }

    gchar* name_param_fire[] = {(gchar*)"Nb fires",(gchar*)"Select previous data",(gchar*)"Param1",(gchar*)"Param2",(gchar*)"Param3",
				(gchar*)"North",(gchar*)"East",(gchar*)"South",(gchar*)"West",
				(gchar*)"Param1",(gchar*)"Param2",(gchar*)"Param3"};
    for(unsigned j=0; j<12; j++){
      label_fire.push_back(gtk_label_new(name_param_fire[j]));
      entry_fire.push_back(gtk_entry_new());
      if(j!=1) gtk_entry_set_width_chars(GTK_ENTRY(entry_fire[j]),4);
    }
    for(unsigned j=1; j<12; j++){
      gtk_widget_set_sensitive(label_fire[j],FALSE);
      gtk_widget_set_sensitive(entry_fire[j],FALSE);
    }

    radio_fire.push_back(gtk_radio_button_new_with_label(NULL, "Random")); //0
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[0]), "Random from normal distribution")); //1
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[0]), "Random from previous data")); //2
    g_signal_connect(G_OBJECT(radio_fire[2]), "clicked", G_CALLBACK(initOptData), table_fire[0]);
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[0]), "Map")); //3
    g_signal_connect(G_OBJECT(radio_fire[3]), "clicked", G_CALLBACK(initOptMap), new_box_v);
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[0]), "Chao Li probability")); //4
    g_signal_connect(G_OBJECT(radio_fire[4]), "clicked", G_CALLBACK(initOptChao), new_box_v);

    radio_fire.push_back(gtk_radio_button_new_with_label(NULL, "8 neighbours")); //5
    g_signal_connect(G_OBJECT(radio_fire[5]), "clicked", G_CALLBACK(neighOpt8), new_box_v);
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[5]), "Extent randomly selected")); //6
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[5]), "Extent fixed")); //7

    radio_fire.push_back(gtk_radio_button_new_with_label(NULL, "Maximum number of steps")); //8
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[8]), "Maximum consumed")); //9
    g_signal_connect(G_OBJECT(radio_fire[9]), "clicked", G_CALLBACK(quotaOptCons), new_box_v);
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[8]), "Maximum number of cells")); //10
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[8]), "Keep going")); //11

    radio_fire.push_back(gtk_radio_button_new_with_label(NULL, "Fire intensity")); //12
    g_signal_connect(G_OBJECT(radio_fire[12]), "clicked", G_CALLBACK(propOptIntensity), box_fire_h[1]);
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[12]), "Percentage of fuel consumed")); //13
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[12]), "Maximum amount of fuel")); //14
    radio_fire.push_back(gtk_radio_button_new_with_label_from_widget(GTK_RADIO_BUTTON (radio_fire[12]), "Chao Li probability")); //15
    g_signal_connect(G_OBJECT(radio_fire[15]), "clicked", G_CALLBACK(initOptChao), new_box_v);

    button_fire.push_back(gtk_button_new_with_label("Select previous data file"));
    g_signal_connect(G_OBJECT(button_fire[0]), "clicked", G_CALLBACK(selectMaskFile), entry_fire[1]);
    gtk_widget_set_sensitive(button_fire[0],FALSE);
    button_fire.push_back(gtk_button_new_with_label("Clear parameters"));
    g_signal_connect(G_OBJECT(button_fire[1]), "clicked", G_CALLBACK(setFireParamsZero), pList->data);
    button_fire.push_back(gtk_button_new_with_label(NULL));
    label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(button_fire[2]),label_tmp);
    g_signal_connect(G_OBJECT(button_fire[2]), "clicked", G_CALLBACK(submitFire1Params), data);

    /* ----------------------------------------------------------------------------------- */
    /* FIRST PANEL : IGNITION PARAMETERS */
    gtk_box_pack_start(GTK_BOX(new_box_v), frame_fire[0], FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(frame_fire[0]), table_fire[0]);
    for(unsigned i=0; i<5; i++){
      gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(radio_fire[i]), 0, i, 1, 1);
    }
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(label_fire[0]), 1, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(entry_fire[0]), 2, 0, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(button_fire[0]), 2, 2, 2, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(entry_fire[1]), 5, 2, 2, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(label_fire[2]), 1, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(entry_fire[2]), 2, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(label_fire[3]), 3, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(entry_fire[3]), 4, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(label_fire[4]), 5, 4, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[0]), expandInGrid(entry_fire[4]), 6, 4, 1, 1);

    /* ----------------------------------------------------------------------------------- */
    /* SECOND PANEL : NEIGHBOUR PARAMETERS */
    gtk_box_pack_start(GTK_BOX(new_box_v), box_fire_h[0], FALSE, FALSE, 0);

    gtk_box_pack_start(GTK_BOX(box_fire_h[0]), frame_fire[1], TRUE, TRUE, 0);
    gtk_container_add(GTK_CONTAINER(frame_fire[1]), table_fire[1]);
    for(unsigned i=0; i<3; i++){
      gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(radio_fire[i+5]), 0, i, 1, 1);
    }
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(label_fire[5]), 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(entry_fire[5]), 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(label_fire[6]), 3, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(entry_fire[6]), 4, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(label_fire[7]), 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(entry_fire[7]), 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(label_fire[8]), 3, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[1]), expandInGrid(entry_fire[8]), 4, 2, 1, 1);

    /* ----------------------------------------------------------------------------------- */
    /* THIRD PANEL : QUOTA PARAMETERS */
    gtk_box_pack_start(GTK_BOX(box_fire_h[0]), frame_fire[2], FALSE, FALSE, 0);

    gtk_container_add(GTK_CONTAINER(frame_fire[2]), table_fire[2]);
    for(unsigned i=0; i<4; i++){
      gtk_grid_attach(GTK_GRID(table_fire[2]), expandInGrid(radio_fire[i+5+3]), 0, i, 1, 1);
    }

    /* ----------------------------------------------------------------------------------- */
    /* FOURTH PANEL : PROPAGATION PARAMETERS */
    gtk_box_pack_start(GTK_BOX(new_box_v), box_fire_h[1], FALSE, FALSE, 0);
    vector<GtkWidget*> listToAdd = {frame_fire[3],frame_fire[4],box_fire_v[0]};
    addToBox(box_fire_h[1],listToAdd,TRUE,TRUE);

    gtk_container_add(GTK_CONTAINER(frame_fire[3]), table_fire[3]);
    for(unsigned i=0; i<4; i++){
      gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(radio_fire[i+5+3+4]), 0, i, 1, 1);
    }
    gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(label_fire[9]), 1, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(entry_fire[9]), 2, 1, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(label_fire[10]), 1, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(entry_fire[10]), 2, 2, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(label_fire[11]), 1, 3, 1, 1);
    gtk_grid_attach(GTK_GRID(table_fire[3]), expandInGrid(entry_fire[11]), 2, 3, 1, 1);

    /* ----------------------------------------------------------------------------------- */
    /* FIFTH PANEL : BUTTONS PARAMETERS */
    gtk_container_add(GTK_CONTAINER(frame_fire[4]), scrollbar_fire);
    //gtk_container_add(GTK_CONTAINER(scrollbar_fire), table_fire[4]);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_fire), table_fire[4]);

    vector<GtkWidget*> pLabel, pEntry;
    for(unsigned i=0; i<NbFires; i++){
      pLabel.push_back(gtk_label_new(("Fire"+IntToString(i)).c_str()));
      pEntry.push_back(gtk_entry_new());
      gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),4);
    }
    for(unsigned i=0; i<NbFires; i++){
      gtk_grid_attach(GTK_GRID(table_fire[4]), expandInGrid(pLabel[i]), 0, i, 1, 1);
      gtk_grid_attach(GTK_GRID(table_fire[4]), expandInGrid(pEntry[i]), 1, i, 1, 1);
    }

    gtk_box_pack_start(GTK_BOX(box_fire_v[0]), button_fire[1], TRUE, FALSE, 0);
    gtk_box_pack_start(GTK_BOX(box_fire_v[0]), button_fire[2], TRUE, FALSE, 0);

    gtk_widget_show_all(fire_NoteBook);
  }

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  pList = g_list_next(pList);                                             //BOX_SOIL_V
  pList = g_list_next(pList);                                             //BOX_FIRE_V
  pList = g_list_next(pList);                                             //BOX_HAB_V
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  if(is_hab){
    gtk_box_pack_start(GTK_BOX(pList->data), gtk_label_new("Coucou"), TRUE, TRUE, 0);
  }
  
  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  pList = g_list_next(pList);                                             //BOX_SOIL_V
  pList = g_list_next(pList);                                             //BOX_FIRE_V
  pList = g_list_next(pList);                                             //BOX_HAB_V
  pList = g_list_next(pList);                                             //BOX_DROUGHT_V
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  if(is_drought){
    GtkWidget* drought_NoteBook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(drought_NoteBook), true);
    gtk_box_pack_start(GTK_BOX(pList->data), drought_NoteBook, TRUE, TRUE, 0);
    gtk_container_set_border_width(GTK_CONTAINER(drought_NoteBook), 10);

    vector<GtkWidget*> new_box_v;
    DroughtChrono.clear();
    DroughtFreq.clear();
    DroughtPropKilledProp.clear();
    DroughtPercentActivSeeds.clear();
    DroughtBreakAges.clear();
    DroughtResprAges.clear();
    DroughtPropKilledInd.clear();
    DroughtPropResproutInd.clear();
    submit_drought.clear();

    DroughtChrono.resize(2);
    DroughtFreq.resize(2);
    DroughtPropKilledProp.resize(2);
    DroughtPercentActivSeeds.resize(2);
    DroughtBreakAges.resize(2);
    DroughtResprAges.resize(2);
    DroughtPropKilledInd.resize(2);
    DroughtPropResproutInd.resize(2);
    submit_drought.resize(2);

    for(unsigned i=0; i<2; i++){
      new_box_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL,0));
      if(i==0){ gtk_notebook_append_page(GTK_NOTEBOOK(drought_NoteBook), new_box_v[i], (GtkWidget*) gtk_label_new("Post_drought"));
      } else if(i==1){ gtk_notebook_append_page(GTK_NOTEBOOK(drought_NoteBook), new_box_v[i], (GtkWidget*) gtk_label_new("Curr_drought")); }

      vector<GtkWidget*> box_drought_h;
      vector<GtkWidget*> box_drought_v;
      vector<GtkWidget*> frame_drought;
      vector<GtkWidget*> table_drought;
      vector<GtkWidget*> label_drought;
      vector<GtkWidget*> entry_drought;
      vector<GtkWidget*> check_drought;
      vector<GtkWidget*> button_drought;
      GtkWidget* scrollbar_drought;

      /* ----------------------------------------------------------------------------------- */
      /* DROUGHT PART */

      box_drought_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //Basicparametersbox
      gtk_container_set_border_width(GTK_CONTAINER(box_drought_h[0]), 10);
      box_drought_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttonsbox
      table_drought.push_back(gtk_grid_new());    //Requiredparameterstable
      gtk_container_set_border_width(GTK_CONTAINER(table_drought[0]), 10);

      GtkWidget* label_tmp;
      vector<gchar*> txt_tmp = {(gchar*)"<b>New disturbance parameters</b>",
				(gchar*)"<b>Response stages parameters</b>"};
      for(unsigned i=0; i<txt_tmp.size(); i++){
	label_tmp = gtk_label_new(NULL);
	frame_drought.push_back(gtk_frame_new(NULL));
	gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
	gtk_frame_set_label_widget(GTK_FRAME(frame_drought[i]),label_tmp);
	gtk_container_set_border_width(GTK_CONTAINER(frame_drought[i]), 10);
      }
      scrollbar_drought = gtk_scrolled_window_new(NULL, NULL);
      gtk_container_set_border_width(GTK_CONTAINER(scrollbar_drought), 10);
      gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_drought), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

      vector<gchar*> name_param_drought = {(gchar*)"NAME",(gchar*)"Frequency",(gchar*)"Proportion\nof killed\npropagules",
					   (gchar*)"Percentage\nof activated\nseeds",(gchar*)"Previous\nor post\nsuccession"};
      for(unsigned j=0; j<name_param_drought.size(); j++){
	label_drought.push_back(gtk_label_new(name_param_drought[j]));
	gtk_label_set_justify(GTK_LABEL(label_drought[j]), GTK_JUSTIFY_CENTER);
	entry_drought.push_back(gtk_entry_new());
	gtk_entry_set_max_length(GTK_ENTRY(entry_drought[j]),12);
	gtk_entry_set_width_chars(GTK_ENTRY(entry_drought[j]),12);
      }
      if(i==0){ gtk_entry_set_text(GTK_ENTRY(entry_drought[0]),"Post_drought");
      } else if(i==1){ gtk_entry_set_text(GTK_ENTRY(entry_drought[0]),"Curr_drought"); }
      gtk_widget_set_sensitive(GTK_WIDGET(entry_drought[0]),false);

      button_drought.push_back(gtk_button_new_with_label("Generate response stages"));
      g_signal_connect(G_OBJECT(button_drought[0]), "clicked", G_CALLBACK(generateRespStages), drought_NoteBook);
      button_drought.push_back(gtk_button_new_with_label("Clear parameters"));
      g_signal_connect(G_OBJECT(button_drought[1]), "clicked", G_CALLBACK(setDistParamsZero), drought_NoteBook);
      button_drought.push_back(gtk_button_new_with_label(NULL));
      label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(button_drought[2]),label_tmp);
      g_signal_connect(G_OBJECT(button_drought[2]), "clicked", G_CALLBACK(submitPertParams), drought_NoteBook);
      gtk_widget_set_sensitive(button_drought[2],FALSE);

      /* ----------------------------------------------------------------------------------- */
      /* FIRST PANEL : BASIC PARAMETERS */
      gtk_box_pack_start(GTK_BOX(new_box_v[i]), box_drought_h[0], FALSE, FALSE, 0);

      gtk_box_pack_start(GTK_BOX(box_drought_h[0]), frame_drought[0], TRUE, FALSE, 0);
      gtk_container_add(GTK_CONTAINER(frame_drought[0]), table_drought[0]);

      for(unsigned j=0; j<label_drought.size(); j++){
	gtk_grid_attach(GTK_GRID(table_drought[0]), label_drought[j], j, 0, 1, 1);
      }
      for(unsigned j=0; j<entry_drought.size(); j++){
	gtk_grid_attach(GTK_GRID(table_drought[0]), entry_drought[j], j, 1, 1, 1);
      }

      gtk_box_pack_start(GTK_BOX(box_drought_h[0]), box_drought_v[0], TRUE, FALSE, 0);
      vector<GtkWidget*> listToAdd = {button_drought[0],button_drought[1],button_drought[2]};
      addToBox(box_drought_v[0],listToAdd,TRUE,FALSE);

      /* ----------------------------------------------------------------------------------- */
      /* SECOND PANEL : RESPONSE STAGES PARAMETERS */
      gtk_box_pack_start(GTK_BOX(new_box_v[i]), frame_drought[1], TRUE, TRUE, 0);

      gtk_container_add(GTK_CONTAINER(frame_drought[1]), scrollbar_drought);
      //gtk_container_add(GTK_CONTAINER(scrollbar_drought), gtk_grid_new());
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_drought), gtk_grid_new());
    }
    gtk_widget_show_all(drought_NoteBook);
  }


  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  pList = g_list_next(pList);                                             //BOX_SOIL_V
  pList = g_list_next(pList);                                             //BOX_FIRE_V
  pList = g_list_next(pList);                                             //BOX_HAB_V
  pList = g_list_next(pList);                                             //BOX_DROUGHT_V
  pList = g_list_next(pList);                                             //BOX_ALIENS_V
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  if(is_aliens){
    gtk_box_pack_start(GTK_BOX(pList->data), gtk_label_new("Coucou"), TRUE, TRUE, 0);
  }
}

/*############################################################################################################################*/

void addNewPFG(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_pfg[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_pfg[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  bool is_dist_ok=FALSE, is_soil_ok=FALSE, is_fire_ok=FALSE, is_drought_ok=FALSE;

  for(unsigned i=0; i<9; i++) { ppList = g_list_next(ppList); }       //label_pfg

  GtkWidget* temp[18];
  temp[0] = GTK_WIDGET(ppList->data);
  temp[1] = GTK_WIDGET(ppList->next->data);
  temp[2] = GTK_WIDGET(ppList->next->next->data);
  temp[3] = GTK_WIDGET(ppList->next->next->next->data);
  temp[4] = GTK_WIDGET(ppList->next->next->next->next->data);
  temp[5] = GTK_WIDGET(ppList->next->next->next->next->next->data);
  temp[6] = GTK_WIDGET(ppList->next->next->next->next->next->next->data);
  temp[7] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->data);
  temp[8] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->data);
  temp[9] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->data);
  temp[10] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->data);

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));     //frame_pfg[0]
  ppList = g_list_next(ppList);                                        //frame_pfg[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));    //table_pfg[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  temp[11] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->data);
  temp[12] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->data);
  temp[13] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->data);
  temp[14] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->data);
  temp[15] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data);
  temp[16] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data);
  temp[17] = GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data);

  if((gtk_entry_get_text_length(GTK_ENTRY(temp[0]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[1]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[2]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[3]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[4]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[5]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[6]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[7]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[8]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[9]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[10]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[13]))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(temp[14]))>0)
     ){

    pList = g_list_next(pList);                                         //frame_pfg[2]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //table_pfg[2]
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
    ppList = g_list_reverse(ppList);

    if(is_dist){
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->next->data))) {
	is_dist_ok = TRUE;
      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe optional parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
	return;
      }
    } else { is_dist_ok = TRUE; }

    if(is_soil){
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->data))) {
	is_soil_ok = TRUE;
      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe optional parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
	return;
      }
    } else { is_soil_ok = TRUE; }

    if(is_fire){
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->data))) {
	is_fire_ok = TRUE;
      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe optional parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
	return;
      }
    } else { is_fire_ok = TRUE; }

    if(is_drought){
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)) &&
	 gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data))) {
	is_drought_ok = TRUE;
      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe optional parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
	return;
      }
    } else { is_drought_ok = TRUE; }

    if(is_dist_ok && is_soil_ok && is_fire_ok && is_drought_ok){
      Group.push_back(gtk_entry_get_text(GTK_ENTRY(temp[0])));
      Type.push_back(gtk_entry_get_text(GTK_ENTRY(temp[1])));
      Light.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[2]))));
      Height.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[3]))));
      Longevity.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[4]))));
      Maturity.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[5]))));
      DispDist50.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[6]))));
      DispDist99.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[7]))));
      DispDistLD.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[8]))));
      MaxAbund.push_back(calcMaxAbund(Type[Type.size()-1],Height[Height.size()-1]));
      ImmSize.push_back(calcImmSize(Type[Type.size()-1],Height[Height.size()-1]));
       
      WideDisp.push_back(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(temp[11])));
      SeedDorm.push_back(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(temp[12])));
      SeedLife_active.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[13]))));
      SeedLife_dormant.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(temp[14]))));
      if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(temp[15]))){
	ModeDisp.push_back(1);
      } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(temp[16]))){
	ModeDisp.push_back(2);
      } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(temp[17]))){
	ModeDisp.push_back(3);
      }

      //gtk_entry_set_text(GTK_ENTRY(ppList->next->data),IntToString(MaxAbund[MaxAbund.size()-1]).c_str());
      //gtk_entry_set_text(GTK_ENTRY(ppList->next->next->next->data),IntToString(ImmSize[ImmSize.size()-1]).c_str());
      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
      pList = g_list_next(pList);                                              //BOX_PFG_V
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));          //box_pfg_h[0]
      pList = g_list_next(pList);                                         //frame_pfg[2]
      ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //table_pfg[2]
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      ppList = g_list_reverse(ppList);

      if(is_dist) {
	Palatability.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->data)))));
      } else { Palatability.push_back(0); }
      if(is_soil) {
	SoilContrib.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->data)))));
	TolMin.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->data)))));
	TolMax.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->data)))));
      } else {
	SoilContrib.push_back(0.0);
	TolMin.push_back(0.0);
	TolMax.push_back(0.0);
      }
      if(is_fire) {
	Flammability.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->data)))));
      } else { Flammability.push_back(0); }
      if(is_drought) {
	SDminMod.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->data)))));
	SDmaxMod.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->data)))));
	SDminSev.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)))));
	SDmaxSev.push_back(atof(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)))));
	CountModToSev.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)))));
	CountSevMort.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)))));
	Recovery.push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(ppList->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->next->data)))));
      } else {
	SDminMod.push_back(0.0);
	SDmaxMod.push_back(0.0);
	SDminSev.push_back(0.0);
	SDmaxSev.push_back(0.0);
	CountModToSev.push_back(0);
	CountSevMort.push_back(0);
	Recovery.push_back(0);
      }

      /* WRITE THE NEW PFG INTO THE INFORMATION BOX */
      pList = g_list_next(pList);                                         //frame_pfg[3]
      ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //scrollbar_pfg
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewPort
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //box_pfg_v[1]
      GtkWidget* pppList = GTK_WIDGET(gtk_container_get_children(GTK_CONTAINER(ppList->data))->data);   //table_pfg[3]
      gint pSize = g_list_length(gtk_container_get_children(GTK_CONTAINER(gtk_container_get_children(GTK_CONTAINER(ppList->data))->data)));

      //GtkWidget* pTable = gtk_grid_new();
      //gtk_box_pack_start(GTK_BOX(GTK_WIDGET(ppList->data)), pTable, FALSE, FALSE, 0);

      vector<GtkWidget*> listToAdd = {gtk_check_button_new(),
                                      gtk_label_new(Group[Group.size()-1].c_str()),
                                      gtk_label_new(Type[Type.size()-1].c_str()),
                                      gtk_label_new(IntToString(Light[Light.size()-1]).c_str()),
                                      gtk_label_new(IntToString(Height[Height.size()-1]).c_str()),
                                      gtk_label_new(IntToString(Longevity[Longevity.size()-1]).c_str()),
                                      gtk_label_new(IntToString(Maturity[Maturity.size()-1]).c_str()),
                                      gtk_label_new(IntToString(DispDist50[DispDist50.size()-1]).c_str()),
                                      gtk_label_new(IntToString(DispDist99[DispDist99.size()-1]).c_str()),
                                      gtk_label_new(IntToString(DispDistLD[DispDistLD.size()-1]).c_str()),
                                      gtk_label_new(IntToString(MaxAbund[MaxAbund.size()-1]).c_str()),
                                      gtk_label_new(IntToString(ImmSize[ImmSize.size()-1]).c_str()),
                                      gtk_label_new(IntToString(WideDisp[WideDisp.size()-1]).c_str()),
                                      gtk_label_new(IntToString(SeedDorm[SeedDorm.size()-1]).c_str()),
                                      gtk_label_new(IntToString(SeedLife_active[SeedLife_active.size()-1]).c_str()),
                                      gtk_label_new(IntToString(SeedLife_dormant[SeedLife_dormant.size()-1]).c_str()),
                                      gtk_label_new(IntToString(ModeDisp[ModeDisp.size()-1]).c_str()),
                                      gtk_label_new(IntToString(Palatability[Palatability.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(SoilContrib[SoilContrib.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(TolMin[TolMin.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(TolMax[TolMax.size()-1]).c_str()),
                                      gtk_label_new(IntToString(Flammability[Flammability.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(SDminMod[SDminMod.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(SDmaxMod[SDmaxMod.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(SDminSev[SDminSev.size()-1]).c_str()),
                                      gtk_label_new(DoubleToString(SDmaxSev[SDmaxSev.size()-1]).c_str()),
                                      gtk_label_new(IntToString(CountModToSev[CountModToSev.size()-1]).c_str()),
                                      gtk_label_new(IntToString(CountSevMort[CountSevMort.size()-1]).c_str()),
                                      gtk_label_new(IntToString(Recovery[Recovery.size()-1]).c_str())};
      //gtk_grid_insert_next_to(GTK_GRID(pppList),gtk_grid_get_child_at(GTK_GRID(pppList),1,0),GTK_POS_BOTTOM);
      gtk_grid_insert_row(GTK_GRID(pppList),(gint)(pSize/27));
      addToGrid(GTK_WIDGET(pppList),listToAdd,(gint)(pSize/27));

      gtk_widget_show_all(GTK_WIDGET(ppList->data));
      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#FF8000\"><b>Add new PFG</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);

      cleanDelAddPFG(data);
      submit_succ1 = FALSE;
      submit_succ2 = FALSE;
      submit_maskPFG = FALSE;
    }

  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/
void removePFG(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
  pList = g_list_next(pList);                                                     //frame_pfg[2]
  pList = g_list_next(pList);                                                     //frame_pfg[3]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_v[1]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_pfg[3]
  GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  pppList = g_list_reverse(pppList);
  gint pSize = g_list_length(pppList);
  gint cells_passed = 0;

  vector<gint> toSuppr;
  for(unsigned i=0; i<27; i++){ pppList = g_list_next(pppList); cells_passed ++; } //first line
  while(cells_passed<pSize){
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->data))){
      gint nrow = (gint)(cells_passed / 27);
      toSuppr.push_back(nrow);
    }
    for(unsigned i=0; i<29; i++){ pppList = g_list_next(pppList); cells_passed ++; }
  }

  if(toSuppr.size()>0){
    GtkWidget* pTable = gtk_grid_new();
    GtkWidget* pLab;
    gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new("")), 0, 0, 1, 1);
    for(unsigned i=0; i<6; i++){
      pLab = gtk_label_new(gtk_label_get_text(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(ppList->data),i+1,0))));
      gtk_label_set_justify(GTK_LABEL(pLab), GTK_JUSTIFY_CENTER);
      gtk_label_set_angle(GTK_LABEL(pLab), 90);
      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLab), i+1, 0, 1, 1);
    }
    pLab = gtk_label_new(gtk_label_get_text(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(ppList->data),7,0))));
    gtk_label_set_justify(GTK_LABEL(pLab), GTK_JUSTIFY_CENTER);
    gtk_label_set_angle(GTK_LABEL(pLab), 90);
    gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLab), 7, 0, 3, 1);
    for(unsigned i=7; i<13; i++){
      pLab = gtk_label_new(gtk_label_get_text(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(ppList->data),i+3,0))));
      gtk_label_set_justify(GTK_LABEL(pLab), GTK_JUSTIFY_CENTER);
      gtk_label_set_angle(GTK_LABEL(pLab), 90);
      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLab), i+3, 0, 1, 1);
    }
    for(unsigned i=15; i<28; i++){
      pLab = gtk_label_new(gtk_label_get_text(GTK_LABEL(gtk_grid_get_child_at(GTK_GRID(ppList->data),i+1,0))));
      gtk_label_set_justify(GTK_LABEL(pLab), GTK_JUSTIFY_CENTER);
      gtk_label_set_angle(GTK_LABEL(pLab), 90);
      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLab), i+1, 0, 1, 1);
    }

    vector<unsigned> toKeep;
    for(unsigned i=0; i<Group.size(); i++){ toKeep.push_back(i); }
    for(unsigned row_i=toSuppr.size(); row_i>0; row_i--){ cout << "row_i " << row_i << endl; cout << "toSuppr[row_i] " << toSuppr[row_i] << endl; toKeep.erase(toKeep.begin() + (toSuppr[row_i-1]-1)); }

    vector<GtkWidget*> listToAdd;
    for(unsigned keep_i=0; keep_i<toKeep.size(); keep_i++){
      unsigned i = toKeep[keep_i];
      listToAdd = {gtk_check_button_new(),
		   gtk_label_new(Group[i].c_str()),
		   gtk_label_new(Type[i].c_str()),
		   gtk_label_new(IntToString(Light[i]).c_str()),
		   gtk_label_new(IntToString(Height[i]).c_str()),
		   gtk_label_new(IntToString(Longevity[i]).c_str()),
		   gtk_label_new(IntToString(Maturity[i]).c_str()),
		   gtk_label_new(IntToString(DispDist50[i]).c_str()),
		   gtk_label_new(IntToString(DispDist99[i]).c_str()),
		   gtk_label_new(IntToString(DispDistLD[i]).c_str()),
		   gtk_label_new(IntToString(MaxAbund[i]).c_str()),
		   gtk_label_new(IntToString(ImmSize[i]).c_str()),
		   gtk_label_new(IntToString(WideDisp[i]).c_str()),
		   gtk_label_new(IntToString(SeedDorm[i]).c_str()),
		   gtk_label_new(IntToString(SeedLife_active[i]).c_str()),
		   gtk_label_new(IntToString(SeedLife_dormant[i]).c_str()),
		   gtk_label_new(IntToString(ModeDisp[i]).c_str()),
		   gtk_label_new(IntToString(Palatability[i]).c_str()),
		   gtk_label_new(DoubleToString(SoilContrib[i]).c_str()),
		   gtk_label_new(DoubleToString(TolMin[i]).c_str()),
		   gtk_label_new(DoubleToString(TolMax[i]).c_str()),
		   gtk_label_new(IntToString(Flammability[i]).c_str()),
		   gtk_label_new(DoubleToString(SDminMod[i]).c_str()),
		   gtk_label_new(DoubleToString(SDmaxMod[i]).c_str()),
		   gtk_label_new(DoubleToString(SDminSev[i]).c_str()),
		   gtk_label_new(DoubleToString(SDmaxSev[i]).c_str()),
		   gtk_label_new(IntToString(CountModToSev[i]).c_str()),
		   gtk_label_new(IntToString(CountSevMort[i]).c_str()),
		   gtk_label_new(IntToString(Recovery[i]).c_str())};
      gint pSizeTable = g_list_length(gtk_container_get_children(GTK_CONTAINER(pTable)));
      gtk_grid_insert_row(GTK_GRID(pTable),(gint)(pSizeTable/27));
      addToGrid(pTable,listToAdd,(gint)(pSizeTable/27));
    }


    for(unsigned row_i=toSuppr.size(); row_i>0; row_i--){
      gint el = toSuppr[row_i - 1]-1;

      Group.erase(Group.begin()+el);
      Type.erase(Type.begin()+el);
      Light.erase(Light.begin()+el);
      Height.erase(Height.begin()+el);
      Longevity.erase(Longevity.begin()+el);
      Maturity.erase(Maturity.begin()+el);
      DispDist50.erase(DispDist50.begin()+el);
      DispDist99.erase(DispDist99.begin()+el);
      DispDistLD.erase(DispDistLD.begin()+el);
      MaxAbund.erase(MaxAbund.begin()+el);
      ImmSize.erase(ImmSize.begin()+el);
      WideDisp.erase(WideDisp.begin()+el);
      SeedDorm.erase(SeedDorm.begin()+el);
      SeedLife_active.erase(SeedLife_active.begin()+el);
      SeedLife_dormant.erase(SeedLife_dormant.begin()+el);
      ModeDisp.erase(ModeDisp.begin()+el);
      Palatability.erase(Palatability.begin()+el);
      SoilContrib.erase(SoilContrib.begin()+el);
      TolMin.erase(TolMin.begin()+el);
      TolMax.erase(TolMax.begin()+el);
      Flammability.erase(Flammability.begin()+el);
      SDminMod.erase(SDminMod.begin()+el);
      SDmaxMod.erase(SDmaxMod.begin()+el);
      SDminSev.erase(SDminSev.begin()+el);
      SDmaxSev.erase(SDmaxSev.begin()+el);
      CountModToSev.erase(CountModToSev.begin()+el);
      CountSevMort.erase(CountSevMort.begin()+el);
      Recovery.erase(Recovery.begin()+el);

      cleanDelAddPFG(data);
    }

    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
    pList = g_list_next(pList);                                                     //BOX_PFG_V
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
    pList = g_list_next(pList);                                                     //frame_pfg[2]
    pList = g_list_next(pList);                                                     //frame_pfg[3]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //viewport
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_v[1]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_pfg[3]

    gtk_container_remove(GTK_CONTAINER(pList->data),GTK_WIDGET(ppList->data));
    gtk_box_pack_start(GTK_BOX(pList->data), pTable, FALSE, FALSE, 0);
    gtk_widget_show_all(GTK_WIDGET(pList->data));
  }

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //pTable
  pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  pppList = g_list_reverse(pppList);
  pSize = g_list_length(pppList);
  if(pSize==27){
    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
    pList = g_list_next(pList);                                                     //BOX_PFG_V
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_pfg[0]
    pList = g_list_next(pList);                                                     //frame_pfg[1]
    pList = g_list_next(pList);                                                     //box_pfg_v[0]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    GtkWidget* label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Add new PFG</b></span>"; //background=\"#A00000\"
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(pList->next->data),label_tmp);
  }
}


/*void removePFG(GtkWidget* pButton, gpointer data) //OK
  {
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
  pList = g_list_next(pList);                                                     //frame_pfg[2]
  pList = g_list_next(pList);                                                     //frame_pfg[3]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_v[1]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_pfg[3]
  GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  pppList = g_list_reverse(pppList);
  gint pSize = g_list_length(pppList);
  gint cells_passed = 0;
  bool bool_stop = FALSE;

  for(unsigned i=0; i<27; i++){ pppList = g_list_next(pppList); cells_passed ++; } //first line
  while((cells_passed<pSize) & (pSize>27)){
  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(pppList->data))){
  //gtk_container_remove(GTK_CONTAINER(pList->data),GTK_WIDGET(pppList->data));
  gint nrow = (gint)(cells_passed / 27);
  gint el = nrow-1;
  //gtk_grid_remove_row(GTK_GRID(ppList->data),nrow); //Only with GTK 3 version 10
      
  Group.erase(Group.begin()+el);
  Type.erase(Type.begin()+el);
  Light.erase(Light.begin()+el);
  Height.erase(Height.begin()+el);
  Longevity.erase(Longevity.begin()+el);
  Maturity.erase(Maturity.begin()+el);
  DispDist50.erase(DispDist50.begin()+el);
  DispDist99.erase(DispDist99.begin()+el);
  DispDistLD.erase(DispDistLD.begin()+el);
  MaxAbund.erase(MaxAbund.begin()+el);
  ImmSize.erase(ImmSize.begin()+el);
  WideDisp.erase(WideDisp.begin()+el);
  SeedDorm.erase(SeedDorm.begin()+el);
  SeedLife_active.erase(SeedLife_active.begin()+el);
  SeedLife_dormant.erase(SeedLife_dormant.begin()+el);
  ModeDisp.erase(ModeDisp.begin()+el);
  Palatability.erase(Palatability.begin()+el);
  SoilContrib.erase(SoilContrib.begin()+el);
  TolMin.erase(TolMin.begin()+el);
  TolMax.erase(TolMax.begin()+el);
  Flammability.erase(Flammability.begin()+el);
  SDminMod.erase(SDminMod.begin()+el);
  SDmaxMod.erase(SDmaxMod.begin()+el);
  SDminSev.erase(SDminSev.begin()+el);
  SDmaxSev.erase(SDmaxSev.begin()+el);
  CountModToSev.erase(CountModToSev.begin()+el);
  CountSevMort.erase(CountSevMort.begin()+el);
  Recovery.erase(Recovery.begin()+el);

  cleanDelAddPFG(data);
  bool_stop = TRUE;
  }

  if(!bool_stop){
  for(unsigned i=0; i<29; i++){ pppList = g_list_next(pppList); cells_passed ++; }
  } else {
  pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  pppList = g_list_reverse(pppList);
  pSize = g_list_length(pppList);
  cells_passed = 0;
  for(unsigned i=0; i<27; i++){ pppList = g_list_next(pppList); cells_passed ++; } //first line
  bool_stop = FALSE;
  }
  }

  if(pSize==27){
  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_pfg[0]
  pList = g_list_next(pList);                                                     //frame_pfg[1]
  pList = g_list_next(pList);                                                     //box_pfg_v[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  GtkWidget* label_tmp = gtk_label_new(NULL);
  gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Add new PFG</b></span>"; //background=\"#A00000\"
  gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
  gtk_button_set_image(GTK_BUTTON(pList->next->data),label_tmp);
  }
  }*/

/*############################################################################################################################*/

void removeAllPFG(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
  pList = g_list_next(pList);                                                     //frame_pfg[2]
  pList = g_list_next(pList);                                                     //frame_pfg[3]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_v[1]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //table_pfg[3]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  gint cells_passed = g_list_length(ppList);
  GList* pppList;

  while(cells_passed>27){
    pppList = ppList;
    ppList = g_list_next(ppList);
    gtk_container_remove(GTK_CONTAINER(pList->data),GTK_WIDGET(pppList->data));
    cells_passed--;
  }
  cleanDelAddPFG(data);
  
  Group.clear();
  Type.clear();
  Light.clear();
  Height.clear();
  Longevity.clear();
  Maturity.clear();
  DispDist50.clear();
  DispDist99.clear();
  DispDistLD.clear();
  MaxAbund.clear();
  ImmSize.clear();
  WideDisp.clear();
  SeedDorm.clear();
  SeedLife_active.clear();
  SeedLife_dormant.clear();
  ModeDisp.clear();
  Palatability.clear();
  SoilContrib.clear();
  TolMin.clear();
  TolMax.clear();
  Flammability.clear();
  SDminMod.clear();
  SDmaxMod.clear();
  SDminSev.clear();
  SDmaxSev.clear();
  CountModToSev.clear();
  CountSevMort.clear();
  Recovery.clear();

  pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //BOX_SIM_v
  pList = g_list_next(pList);                                                     //BOX_PFG_V
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_pfg_h[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_pfg[0]
  pList = g_list_next(pList);                                                     //frame_pfg[1]
  pList = g_list_next(pList);                                                     //box_pfg_v[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  GtkWidget* label_tmp = gtk_label_new(NULL);
  gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Add new PFG</b></span>"; //background=\"#A00000\"
  gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
  gtk_button_set_image(GTK_BUTTON(pList->next->data),label_tmp);
}

/*############################################################################################################################*/

void generateStrataAges(GtkWidget* pButton, gpointer data) //OK
{
  unsigned nb_PFG = Group.size();
  
  if(NbStrata>0){
    if(nb_PFG>0){
      GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_succ_v[1]
      pList = g_list_next(pList);                                                     //frame_succ[0]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar_succ[0]
      GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //viewport
      if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

      GtkWidget* pTable = gtk_grid_new();
      //gtk_container_add(GTK_CONTAINER(pList->data), pTable);
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(pList->data), pTable);
      gtk_container_set_border_width(GTK_CONTAINER(pTable), 5);

      vector<GtkWidget*> pEntry;
      for(unsigned i=0; i<((NbStrata+1)*nb_PFG); i++){
	pEntry.push_back(gtk_entry_new());
	gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),4);
      }
      for(unsigned i=0; i<(NbStrata+1); i++){
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(("Strata"+IntToString(i)+IntToString(i+1)).c_str())),i+1,0,1,1);
      }
      for(unsigned i=0; i<nb_PFG; i++){
	gtk_grid_attach(GTK_GRID(pTable), gtk_label_new(Group[i].c_str()),0,i+1,1,1);
      }
      for(unsigned j=0; j<nb_PFG; j++){
	for(unsigned i=0; i<(NbStrata+1); i++){
	  gtk_grid_attach(GTK_GRID(pTable), pEntry[i+j*(NbStrata+1)],i+1,j+1,1,1);
	}
      }

      gtk_widget_show_all(pTable);
      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_succ_v[1]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
      gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data),TRUE);
      gtk_widget_set_sensitive(GTK_WIDGET(pList->next->next->data),TRUE);
    } else {
      gchar* message = g_locale_to_utf8("You must add \nat least one PFG !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must have \na positive number of strata !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void setSucc1ParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_succ_v[1]
  pList = g_list_next(pList);                                                     //frame_succ[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar_succ[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //pTable?
  if(g_list_length(pList)>0) {
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    pList = g_list_reverse(pList);
    for(unsigned i=0; i<(NbStrata+1+Group.size()); i++) { pList = g_list_next(pList); }
    for(unsigned i=0; i<((NbStrata+1)*Group.size()); i++) {
      gtk_entry_set_text(GTK_ENTRY(GTK_WIDGET(pList->data)),"");
      pList = g_list_next(pList);
    }

    /*
      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_succ_v[1]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));

      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pList->next->data),label_tmp);
    */
  }
}

/*############################################################################################################################*/

void submitSucc1Params(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //pTable?
  if(g_list_length(pList)>0) {
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    pList = g_list_reverse(pList);
    for(unsigned i=0; i<(NbStrata+1+Group.size()); i++) { pList = g_list_next(pList); }
    GList* ppList = pList;
    unsigned compt = 0;
    for(unsigned i=0; i<((NbStrata+1)*Group.size()); i++) {
      if(gtk_entry_get_text_length(GTK_ENTRY(GTK_WIDGET(ppList->data)))>0){ compt++; }
      ppList = g_list_next(ppList);
    }

    if(compt==((NbStrata+1)*Group.size())){
      ChangeStrataAges.clear();
      ChangeStrataAges.resize(Group.size());
      for(unsigned i=0; i<Group.size(); i++) {
	for(unsigned j=0; j<(NbStrata+1); j++) {
	  ChangeStrataAges[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(pList->data)))));
	  pList = g_list_next(pList);
	}
      }
      submit_succ1 = TRUE;
      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);
    } else {
      gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  }
}

/*############################################################################################################################*/

void generateLightStages(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_succ_v[3]
  pList = g_list_next(pList);                                                     //frame_succ[1]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //scrollbar_succ[1]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //viewport
  if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

  unsigned nb_PFG = Group.size();

  if(nb_PFG>0){
    GtkWidget* pTable = gtk_grid_new();

    //gtk_container_add(GTK_CONTAINER(pList->data), pTable);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(pList->data), pTable);
    gtk_container_set_border_width(GTK_CONTAINER(pTable), 5);

    vector<GtkWidget*> pEntry;
    for(unsigned i=0; i<(12*nb_PFG); i++){
      pEntry.push_back(gtk_entry_new());
      gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),4);
    }
    vector<GtkWidget*> pLabel;
    pLabel.push_back(gtk_label_new(g_locale_to_utf8("Active \ngerminant",-1,NULL,NULL,NULL)));
    pLabel.push_back(gtk_label_new(g_locale_to_utf8("Shade tolerance \nfor Germinant",-1,NULL,NULL,NULL)));
    pLabel.push_back(gtk_label_new(g_locale_to_utf8("Shade tolerance \nfor Immature",-1,NULL,NULL,NULL)));
    pLabel.push_back(gtk_label_new(g_locale_to_utf8("Shade tolerance \nfor Mature",-1,NULL,NULL,NULL)));
    for(unsigned i=0; i<4; i++){
      gtk_label_set_justify(GTK_LABEL(pLabel[i]), GTK_JUSTIFY_CENTER);
    }

    gtk_grid_attach(GTK_GRID(pTable), pLabel[0], 0, 1, 1, 4);
    gtk_grid_attach(GTK_GRID(pTable), pLabel[1], 0, 4, 1, 4);
    gtk_grid_attach(GTK_GRID(pTable), pLabel[2], 0, 7, 1, 4);
    gtk_grid_attach(GTK_GRID(pTable), pLabel[3], 0, 10, 1, 4);

    for(unsigned i=0; i<4; i++){
      gtk_grid_attach(GTK_GRID(pTable), gtk_label_new("LOW"), 1, 1+i*3, 1, 1);
      gtk_grid_attach(GTK_GRID(pTable), gtk_label_new("MEDIUM"), 1, 2+i*3, 1, 1);
      gtk_grid_attach(GTK_GRID(pTable), gtk_label_new("HIGH"), 1, 3+i*3, 1, 1);
    }
    for(unsigned i=0; i<nb_PFG; i++){
      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(Group[i].c_str())), 2+i, 0, 1, 1);
    }
    for(unsigned j=0; j<nb_PFG; j++){
      for(unsigned i=0; i<12; i++){
	gtk_grid_attach(GTK_GRID(pTable), pEntry[i+j*12], j+2, i+1, 1, 1);
      }
    }

    gtk_widget_show_all(pTable);
    GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_succ_v[3]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data),TRUE);
    gtk_widget_set_sensitive(GTK_WIDGET(pList->next->next->data),TRUE);

  } else {
    gchar* message = g_locale_to_utf8("You must add \nat least one PFG !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void setSucc2ParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //pTable?
  if(g_list_length(pList)>0) {
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    pList = g_list_reverse(pList);
    for(unsigned i=0; i<(16+Group.size()); i++) { pList = g_list_next(pList); }
    for(unsigned i=0; i<(12*Group.size()); i++) {
      gtk_entry_set_text(GTK_ENTRY(GTK_WIDGET(pList->data)),"");
      pList = g_list_next(pList);
    }
  }
}

/*############################################################################################################################*/

void submitSucc2Params(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //pTable?
  if(g_list_length(pList)>0) {
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    pList = g_list_reverse(pList);
    for(unsigned i=0; i<(16+Group.size()); i++) { pList = g_list_next(pList); }

    GList* ppList = pList;
    unsigned compt = 0;
    for(unsigned i=0; i<(12*Group.size()); i++) {
      if(gtk_entry_get_text_length(GTK_ENTRY(GTK_WIDGET(ppList->data)))>0){ compt++; }
      ppList = g_list_next(ppList);
    }

    if(compt==(12*Group.size())){
      ActiveGerm.clear();
      ShadeTolGerm.clear();
      ShadeTolImm.clear();
      ShadeTolMat.clear();
      ActiveGerm.resize(Group.size());
      ShadeTolGerm.resize(Group.size());
      ShadeTolImm.resize(Group.size());
      ShadeTolMat.resize(Group.size());
      for(unsigned i=0; i<Group.size(); i++) {
	for(unsigned j=0; j<3; j++) {
	  ActiveGerm[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(pList->data)))));
	  pList = g_list_next(pList);
	}
	for(unsigned j=0; j<3; j++) {
	  ShadeTolGerm[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(pList->data)))));
	  pList = g_list_next(pList);
	}
	for(unsigned j=0; j<3; j++) {
	  ShadeTolImm[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(pList->data)))));
	  pList = g_list_next(pList);
	}
	for(unsigned j=0; j<3; j++) {
	  ShadeTolMat[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(GTK_WIDGET(pList->data)))));
	  pList = g_list_next(pList);
	}
      }
      submit_succ2 = TRUE;
      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);
    } else {
      gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  }
}

/*############################################################################################################################*/

void generateRespStages(GtkWidget* pButton, gpointer data) //OK
{
  unsigned nbDIST = gtk_notebook_get_current_page(GTK_NOTEBOOK((GtkWidget*) data));
  const gchar* name_dist = gtk_notebook_get_tab_label_text(GTK_NOTEBOOK((GtkWidget*) data), gtk_notebook_get_nth_page(GTK_NOTEBOOK((GtkWidget*) data), nbDIST));
  unsigned nbRespStages = 0, nbLabels = 4, comptLabels = 0;
  if(strcmp(name_dist,("Dist"+IntToString(nbDIST)).c_str())==0){ nbRespStages = NbRespStagesDist; }
  if(strcmp(name_dist,("Fire"+IntToString(nbDIST-1)).c_str())==0){ nbRespStages = NbRespStagesFire; }
  if((strcmp(name_dist,"Post_drought")==0) || (strcmp(name_dist,"Curr_drought")==0)){
    nbRespStages = NbRespStagesDrought;
    nbLabels = 5;
  }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //new_box_v[0]
  for(unsigned i=0; i<nbDIST; i++) { pList = g_list_next(pList); }
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_dist_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<nbLabels; i++) { ppList = g_list_next(ppList); }      //parametersLabels
  for(unsigned i=0; i<nbLabels; i++) {
    if(gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0){ comptLabels++; }
    ppList = g_list_next(ppList);
  }

  if(comptLabels==nbLabels) {
    if(Group.size()>0) {
      ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_dist[0]
      ppList = g_list_next(ppList);                                       //box_dist_v[0]
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      gtk_widget_set_sensitive(GTK_WIDGET(ppList->next->next->data),TRUE);

      pList = g_list_next(pList);                                                 //frame_dist[1]
      ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));            //scrollbar_dist
      GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewport
      if(g_list_length(pppList)>0) { gtk_widget_destroy(GTK_WIDGET(pppList->data)); }

      GtkWidget* pTable = gtk_grid_new();
      vector<GtkWidget*> pEntry;
      for(unsigned i=0; i<(nbRespStages*(2+2*Group.size())); i++){
	pEntry.push_back(gtk_entry_new());
	gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),8);
      }

      //gtk_container_add(GTK_CONTAINER(ppList->data), pTable);
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(ppList->data), pTable);
      gtk_container_set_border_width(GTK_CONTAINER(pTable), 10);

      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new("Break ages")), 1, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new("Resprouting ages")), 1, 2, 1, 1);
      for(unsigned j=0; j<nbRespStages; j++){
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(("Stage"+IntToString(j)).c_str())), 2+j, 0, 1, 1);
      }
      for(unsigned i=0; i<Group.size(); i++){
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(Group[i].c_str())), 0, 3+i*2, 1, 2);
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new("Proportion killed")), 1, 3+i*2, 1, 1);
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new("Proportion resprouting")), 1, 4+i*2, 1, 1);
      }
      unsigned compt = 0;
      for(unsigned j=0; j<nbRespStages; j++){
	gtk_grid_attach(GTK_GRID(pTable), pEntry[compt], 2+j, 1, 1, 1);
	compt++;
	gtk_grid_attach(GTK_GRID(pTable), pEntry[compt], 2+j, 2, 1, 1);
	compt++;
	for(unsigned i=0; i<Group.size(); i++){
	  gtk_grid_attach(GTK_GRID(pTable), pEntry[compt], 2+j, 3+i*2, 1, 1);
	  compt++;
	  gtk_grid_attach(GTK_GRID(pTable), pEntry[compt], 2+j, 4+i*2, 1, 1);
	  compt++;
	}
      }

      gtk_widget_show_all(pTable);

    } else {
      gchar* message = g_locale_to_utf8("You must add \nat least one PFG !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void setDistParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  unsigned nbDIST = gtk_notebook_get_current_page(GTK_NOTEBOOK((GtkWidget*) data));
  const gchar* name_dist = gtk_notebook_get_tab_label_text(GTK_NOTEBOOK((GtkWidget*) data), gtk_notebook_get_nth_page(GTK_NOTEBOOK((GtkWidget*) data), nbDIST));
  unsigned nbRespStages = 0, nbLabels = 4;
  if(strcmp(name_dist,("Dist"+IntToString(nbDIST)).c_str())==0){ nbRespStages = NbRespStagesDist; }
  if(strcmp(name_dist,("Fire"+IntToString(nbDIST-1)).c_str())==0){ nbRespStages = NbRespStagesFire; }
  if((strcmp(name_dist,"Post_drought")==0) || (strcmp(name_dist,"Curr_drought")==0)){
    nbRespStages = NbRespStagesDrought;
    nbLabels = 5;
  }
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //new_box_v[0]
  for(unsigned i=0; i<nbDIST; i++) { pList = g_list_next(pList); }
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_dist_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<nbLabels; i++) { ppList = g_list_next(ppList); }      //parametersLabels
  for(unsigned i=0; i<nbLabels; i++) {
    if((i!=0) || ((strcmp(name_dist,"Post_drought")!=0) && (strcmp(name_dist,"Curr_drought")!=0))){ gtk_entry_set_text(GTK_ENTRY(ppList->data),""); }
    ppList = g_list_next(ppList);
  }

  ppList = g_list_next(pList);                                         //frame_dist[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //scrollbar_dist
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewport
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  if(g_list_length(ppList)>0) {
    ppList = g_list_reverse(ppList);
    for(unsigned i=0; i<(2+nbRespStages+3*Group.size()); i++) { ppList = g_list_next(ppList); }
    for(unsigned i=0; i<(nbRespStages*(2+Group.size()*2)); i++) {
      gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
      ppList = g_list_next(ppList);
    }
  }
}

/*############################################################################################################################*/

void submitPertParams(GtkWidget* pButton, gpointer data) //PASOK - OK pour Dist
{
  string temp_name, temp_chrono;
  unsigned temp_freq, temp_killProp, temp_activSeed;
  unsigned nbDIST = gtk_notebook_get_current_page(GTK_NOTEBOOK((GtkWidget*) data));
  const gchar* name_dist = gtk_notebook_get_tab_label_text(GTK_NOTEBOOK((GtkWidget*) data), gtk_notebook_get_nth_page(GTK_NOTEBOOK((GtkWidget*) data), nbDIST));
  unsigned nbRespStages = 0, nbLabels = 4, comptLabels = 0;
  if(strcmp(name_dist,("Dist"+IntToString(nbDIST)).c_str())==0){ nbRespStages = NbRespStagesDist; }
  if(strcmp(name_dist,("Fire"+IntToString(nbDIST-1)).c_str())==0){ nbRespStages = NbRespStagesFire; }
  if((strcmp(name_dist,"Post_drought")==0) || (strcmp(name_dist,"Curr_drought")==0)){
    nbRespStages = NbRespStagesDrought;
    nbLabels = 5;
  }

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //new_box_v[0]
  for(unsigned i=0; i<nbDIST; i++) { pList = g_list_next(pList); }
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_dist_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<nbLabels; i++) { ppList = g_list_next(ppList); }
  GList* pppList = ppList;
  for(unsigned i=0; i<nbLabels; i++) {
    if(gtk_entry_get_text_length(GTK_ENTRY(pppList->data))>0){ comptLabels++; }
    pppList = g_list_next(pppList);
  }

  if(comptLabels==nbLabels) {
    temp_name = gtk_entry_get_text(GTK_ENTRY(ppList->data));
    temp_freq = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->data)));
    temp_killProp = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data)));
    temp_activSeed = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->data)));
    if(nbLabels==5){ temp_chrono = gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->data)); }

    pList = g_list_next(pList);                                         //frame_dist[1]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //scrollbar
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewport
    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
    pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
    pppList = g_list_reverse(pppList);
    for(unsigned i=0; i<(2+nbRespStages+3*Group.size()); i++) { pppList = g_list_next(pppList); }

    unsigned compt = 0;
    for(unsigned i=0; i<((2+2*Group.size())*nbRespStages); i++) {
      if(gtk_entry_get_text_length(GTK_ENTRY(pppList->data))){ compt++; }
      pppList = g_list_next(pppList);
    }

    if(compt==(2*nbRespStages+2*nbRespStages*Group.size())){
      if(strcmp(name_dist,("Dist"+IntToString(nbDIST)).c_str())==0){
	DistNames[nbDIST] = temp_name;
	DistFreq[nbDIST] = temp_freq;
	PropKilledProp[nbDIST] = temp_killProp;
	PercentActivSeeds[nbDIST] = temp_activSeed;
      } else if(strcmp(name_dist,("Fire"+IntToString(nbDIST-1)).c_str())==0){
	FireNames[nbDIST] = temp_name;
	FireFreq[nbDIST] = temp_freq;
	FirePropKilledProp[nbDIST] = temp_killProp;
	FirePercentActivSeeds[nbDIST] = temp_activSeed;
      } else if((strcmp(name_dist,"Post_drought")==0) || (strcmp(name_dist,"Curr_drought")==0)){
	DroughtChrono[nbDIST] = temp_chrono;
	DroughtFreq[nbDIST] = temp_freq;
	DroughtPropKilledProp[nbDIST] = temp_killProp;
	DroughtPercentActivSeeds[nbDIST] = temp_activSeed;
      }

      pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      pppList = g_list_reverse(pppList);
      for(unsigned i=0; i<(2+nbRespStages+3*Group.size()); i++) { pppList = g_list_next(pppList); }

      if(strcmp(name_dist,("Dist"+IntToString(nbDIST)).c_str())==0){
	PropKilledInd[nbDIST].clear();
	PropResproutInd[nbDIST].clear();
	PropKilledInd[nbDIST].resize(Group.size());
	PropResproutInd[nbDIST].resize(Group.size());
	for(unsigned i=0; i<nbRespStages; i++) {
	  BreakAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  ResprAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  for(unsigned j=0; j<Group.size(); j++) {
	    PropKilledInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	    PropResproutInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	  }
	}
	submit_dist[nbDIST] = TRUE;
      } else if(strcmp(name_dist,("Fire"+IntToString(nbDIST-1)).c_str())==0){
	FirePropKilledInd[nbDIST].clear();
	FirePropResproutInd[nbDIST].clear();
	FirePropKilledInd[nbDIST].resize(Group.size());
	FirePropResproutInd[nbDIST].resize(Group.size());
	for(unsigned i=0; i<nbRespStages; i++) {
	  FireBreakAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  FireResprAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  for(unsigned j=0; j<Group.size(); j++) {
	    FirePropKilledInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	    FirePropResproutInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	  }
	}
	submit_fire[nbDIST] = TRUE;
      } else if((strcmp(name_dist,"Post_drought")==0) || (strcmp(name_dist,"Curr_drought")==0)){
	DroughtPropKilledInd[nbDIST].clear();
	DroughtPropResproutInd[nbDIST].clear();
	DroughtPropKilledInd[nbDIST].resize(Group.size());
	DroughtPropResproutInd[nbDIST].resize(Group.size());
	for(unsigned i=0; i<nbRespStages; i++) {
	  DroughtBreakAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  DroughtResprAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  for(unsigned j=0; j<Group.size(); j++) {
	    DroughtPropKilledInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	    DroughtPropResproutInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	  }
	}
	submit_drought[nbDIST] = TRUE;
      }
      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);

    } else {
      gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}


/*############################################################################################################################*/

void generateStrataSoil(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));
  pList = g_list_reverse(pList);
  if(gtk_entry_get_text_length(GTK_ENTRY(pList->next->data))>0){
    unsigned nb_strata = atoi(gtk_entry_get_text(GTK_ENTRY(pList->next->data)));

    pList = g_list_next(pList);                                         //entry
    pList = g_list_next(pList);                                         //button
    pList = g_list_next(pList);                                         //scrollbar_sim
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));     //viewport
    GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

    GtkWidget* pTable = gtk_grid_new();
    vector<GtkWidget*> pEntry;
    for(unsigned i=0; i<(nb_strata+1); i++){
      pEntry.push_back(gtk_entry_new());
      gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),8);
    }
    gtk_entry_set_text(GTK_ENTRY(pEntry[0]),"0");
    gtk_widget_set_sensitive(GTK_WIDGET(pEntry[0]),FALSE);

    //gtk_container_add(GTK_CONTAINER(GTK_WIDGET(pList->data)), pTable);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(pList->data), pTable);
    gtk_container_set_border_width(GTK_CONTAINER(pTable), 5);

    for(unsigned i=0; i<(nb_strata+1); i++){
      gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(("Strata"+IntToString(i)+IntToString(i+1)).c_str())), i, 0, 1, 1);
    }
    for(unsigned i=0; i<(nb_strata+1); i++){
      gtk_grid_attach(GTK_GRID(pTable), pEntry[i], i, 1, 1, 1);
    }
    gtk_widget_show_all(pTable);
  } else {
    gchar* message = g_locale_to_utf8("You must fill the \nstrata parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
    return;
  }
}

/*############################################################################################################################*/

void generateSoilStages(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_soil[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));	  //table_soil[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  unsigned nb_strata = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->data)));

  if(nb_strata>0){

    pList = g_list_next(pList);                                   	//frame_soil[1]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	//scrollbar_soil[1]
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //viewport
    if(g_list_length(ppList)>0) { gtk_widget_destroy(GTK_WIDGET(ppList->data)); }

    unsigned nb_PFG = Group.size();

    if(nb_PFG>0){
      GtkWidget* pTable = gtk_grid_new();

      //gtk_container_add(GTK_CONTAINER(pList->data), pTable);
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(pList->data), pTable);
      gtk_container_set_border_width(GTK_CONTAINER(pTable), 5);

      vector<GtkWidget*> pEntry;
      for(unsigned i=0; i<(nb_PFG*3*(nb_strata+1)); i++){
	pEntry.push_back(gtk_entry_new());
	gtk_entry_set_width_chars(GTK_ENTRY(pEntry[i]),4);
      }
      vector<GtkWidget*> pLabel;
      for(unsigned i=0; i<nb_PFG; i++){
	pLabel.push_back(gtk_label_new(g_locale_to_utf8("Soil tolerance \nfor Germinant",-1,NULL,NULL,NULL)));
	pLabel.push_back(gtk_label_new(g_locale_to_utf8("Soil tolerance \nfor Immature",-1,NULL,NULL,NULL)));
	pLabel.push_back(gtk_label_new(g_locale_to_utf8("Soil tolerance \nfor Mature",-1,NULL,NULL,NULL)));
      }
      for(unsigned i=0; i<(nb_PFG*3); i++){
	gtk_label_set_justify(GTK_LABEL(pLabel[i]), GTK_JUSTIFY_CENTER);
      }

      for(unsigned i=0; i<(nb_strata+1); i++){
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(("Strata"+IntToString(i)).c_str())), 2+i, 0, 1, 1);
      }
      for(unsigned i=0; i<nb_PFG; i++){
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(gtk_label_new(Group[i].c_str())), 0, 1+i*3, 1, 3);
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLabel[i*3]), 1, 1+i*3, 1, 1);
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLabel[i*3+1]), 1, 2+i*3, 1, 1);
	gtk_grid_attach(GTK_GRID(pTable), expandInGrid(pLabel[i*3+2]), 1, 3+i*3, 1, 1);
      }
      for(unsigned j=0; j<(nb_strata+1); j++){
	for(unsigned i=0; i<(nb_PFG*3); i++){
	  gtk_grid_attach(GTK_GRID(pTable), pEntry[i+j*(nb_PFG*3)], 2+j, 1+i, 1, 1);
	}
      }

      gtk_widget_show_all(pTable);

      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));	//frame_soil[0]
      pList = g_list_next(pList);						//frame_soil[1]
      pList = g_list_next(pList);						//table_soil[1]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
      pList = g_list_reverse(pList);
      gtk_widget_set_sensitive(GTK_WIDGET(pList->next->next->data),TRUE);

    } else {
      gchar* message = g_locale_to_utf8("You must add \nat least one PFG !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void setSoilParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_soil[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));	    //table_soil[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);		//label
  gtk_entry_set_text(GTK_ENTRY(ppList->next->data),"");

  ppList = g_list_next(ppList);						//entry
  ppList = g_list_next(ppList);						//button
  ppList = g_list_next(ppList);						//scrollbar_soil[0]
  GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));     	//viewport
  GList* ppppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));     	//pTable?
  ppppList = gtk_container_get_children(GTK_CONTAINER(ppppList->data));
  if(g_list_length(ppppList)>0) {
    gtk_widget_destroy(GTK_WIDGET(pppList->data));
    //gtk_container_add(GTK_CONTAINER(ppList->data), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(ppList->data), gtk_grid_new());
  }

  pList = g_list_next(pList);                                   	//frame_soil[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));	//scrollbar_soil[1]
  pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));     //viewport
  ppppList = gtk_container_get_children(GTK_CONTAINER(pppList->data));     //pTable?
  ppppList = gtk_container_get_children(GTK_CONTAINER(ppppList->data));
  if(g_list_length(ppppList)>0) {
    gtk_widget_destroy(GTK_WIDGET(pppList->data));
    //gtk_container_add(GTK_CONTAINER(ppList->data), gtk_grid_new());
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(ppList->data), gtk_grid_new());
  }

  pList = g_list_next(pList);						//table_soil[1]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  pList = g_list_reverse(pList);
  gtk_widget_set_sensitive(GTK_WIDGET(pList->next->next->data),FALSE);
  /*GtkWidget* label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(pList->next->next->data),label_tmp);*/

}

/*############################################################################################################################*/

void submitSoilParams(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));	//frame_soil[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//table_soil[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  pList = g_list_reverse(pList);
  string temp_nbStrata = gtk_entry_get_text(GTK_ENTRY(pList->next->data));

  pList = g_list_next(pList);						//entry
  pList = g_list_next(pList);						//button
  pList = g_list_next(pList);						//scrollbar_soil[0]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	//viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	//pTable?
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));

  if(temp_nbStrata.length()>0 && g_list_length(pList)>0) {
    pList = g_list_reverse(pList);
    for(int i=0; i<(atoi(temp_nbStrata.c_str())+1); i++){ pList = g_list_next(pList); }
    unsigned compt = 0;
    GList* ppList = pList;
    for(int i=0; i<(atoi(temp_nbStrata.c_str())+1); i++) {
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) { compt++; }
      ppList = g_list_next(ppList);
    }

    if((int)compt==(atoi(temp_nbStrata.c_str())+1)){

      vector< vector<unsigned> > tmp_strataAges;
      tmp_strataAges.resize(atoi(temp_nbStrata.c_str())+1);
      for(int i=0; i<(atoi(temp_nbStrata.c_str())+1); i++){
	tmp_strataAges[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pList->data))));
	pList = g_list_next(pList);
      }

      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));	//frame_soil[0]	
      pList = g_list_next(pList);							//frame_soil[1]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//scrollbar_soil[1]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//viewport
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		//pTable?
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
      pList = g_list_reverse(pList);

      ppList = pList;
      compt = 0;
      for(unsigned i=0; i<(atoi(temp_nbStrata.c_str())+1+Group.size()*4); i++){ ppList = g_list_next(ppList); }
      for(unsigned i=0; i<((atoi(temp_nbStrata.c_str())+1)*Group.size()*3); i++){
	if(gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0){ compt++; }
	ppList = g_list_next(ppList);
      }

      if(compt==((atoi(temp_nbStrata.c_str())+1)*Group.size()*3)){
	ppList = pList;

	NbSoilStrata = atoi(temp_nbStrata.c_str());
	ChangeSoilStrataAges.clear();
	ChangeSoilStrataAges.resize(NbSoilStrata+1);
	ChangeSoilStrataAges = tmp_strataAges;

	SoilTolGerm.clear();
	SoilTolImm.clear();
	SoilTolMat.clear();
	SoilTolGerm.resize(Group.size());
	SoilTolImm.resize(Group.size());
	SoilTolMat.resize(Group.size());
	for(unsigned i=0; i<(NbSoilStrata+1+Group.size()*4); i++){ ppList = g_list_next(ppList); }
	for(unsigned i=0; i<Group.size(); i++) {
	  for(unsigned j=0; j<(NbStrata+1); j++) {
	    SoilTolGerm[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	    ppList = g_list_next(ppList);
	    SoilTolImm[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	    ppList = g_list_next(ppList);
	    SoilTolMat[i].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	    ppList = g_list_next(ppList);
	  }
	}
	submit_soil = TRUE;
	GtkWidget* label_tmp = gtk_label_new(NULL);
	gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
	gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
	gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);
      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
      }
    } else {
      gchar* message = g_locale_to_utf8("You must fill all \nthe strata parameters !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe strata parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void setFireParamsZero(GtkWidget* pButton, gpointer data) //OK
{
  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //fire_NoteBook
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //new_box_v
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_fire[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_fire[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),TRUE);
  for(unsigned i=0; i<6; i++) { ppList = g_list_next(ppList); }
  gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  for(unsigned i=0; i<4; i++) {
    for(unsigned i=0; i<2; i++) { ppList = g_list_next(ppList); }
    gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  }

  pList = g_list_next(pList);                                         //box_fire_h[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),TRUE);
  for(unsigned i=0; i<4; i++) { ppList = g_list_next(ppList); }
  gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  gtk_entry_set_text(GTK_ENTRY(ppList->next->next->data),"");
  gtk_entry_set_text(GTK_ENTRY(ppList->next->next->next->next->data),"");
  gtk_entry_set_text(GTK_ENTRY(ppList->next->next->next->next->next->next->data),"");

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = g_list_next(ppList);                                       //frame_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),TRUE);

  pList = g_list_next(pList);                                         //box_fire_h[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(ppList->data),TRUE);
  for(unsigned i=0; i<5; i++) { ppList = g_list_next(ppList); }
  gtk_entry_set_text(GTK_ENTRY(ppList->data),"");
  gtk_entry_set_text(GTK_ENTRY(ppList->next->next->data),"");
  gtk_entry_set_text(GTK_ENTRY(ppList->next->next->next->next->data),"");

  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
  pList = g_list_next(pList);                                       //frame_fire[4]
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));   //scrollbar
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));   //viewport
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));   //pTable
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));
  pList = g_list_reverse(pList);
  for(unsigned i=0; i<NbFires; i++) {
    pList = g_list_next(pList);
    gtk_entry_set_text(GTK_ENTRY(pList->data),"");
    pList = g_list_next(pList);
  }
}

/*############################################################################################################################*/

void submitFire1Params(GtkWidget* pButton, gpointer data) //PRESQUEOK
{
  unsigned init=0, neigh=0, quota=0, propag=0;
  unsigned temp_initNbFires=0;
  string temp_prevData;
  vector<double> temp_initLogis, temp_propLogis, temp_probFires;
  vector<unsigned> temp_ccExtent;
  bool is_init = TRUE, is_neigh = TRUE, is_prop = TRUE, is_prob = TRUE;

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
  pList = g_list_next(pList);                                             //BOX_PFG_V
  pList = g_list_next(pList);                                             //BOX_SUCC_V
  pList = g_list_next(pList);                                             //BOX_MASK_V
  pList = g_list_next(pList);                                             //BOX_DIST_V
  pList = g_list_next(pList);                                             //BOX_SOIL_V
  pList = g_list_next(pList);                                             //BOX_FIRE_V


  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));    		//fire_NoteBook
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //new_box_v
  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //frame_fire[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //table_fire[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  if((gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))) |
     (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->data))) |
     (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))))
    {
      if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))) { init = 0;}
      if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->data))) { init = 1;}
      if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))) { init = 2;}

      for(unsigned i=0; i<6; i++) { ppList = g_list_next(ppList); }
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0){
	temp_initNbFires = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->data)));
      } else { is_init = FALSE; }

      if(init==2){
	for(unsigned i=0; i<2; i++) { ppList = g_list_next(ppList); }
	if(gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0){
	  temp_prevData = gtk_entry_get_text(GTK_ENTRY(ppList->data));
	} else { is_init = FALSE; }
      }

    } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->data))){
    init = 3;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->next->data))){
    init = 4;
    for(unsigned i=0; i<10; i++) { ppList = g_list_next(ppList); }
    if((gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->data))>0))
      {
	temp_initLogis.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	temp_initLogis.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data))));
	temp_initLogis.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->data))));
      } else { is_init = FALSE; }
  }

  pList = g_list_next(pList);                                         //box_fire_h[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))){
    neigh = 0;
  } else {
    if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->data))){
      neigh = 1;
    } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))){
      neigh = 2;
    }
    for(unsigned i=0; i<4; i++) { ppList = g_list_next(ppList); }
    if((gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->next->next->data))>0))
      {
	temp_ccExtent.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	temp_ccExtent.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data))));
	temp_ccExtent.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->data))));
	temp_ccExtent.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->next->next->data))));
      } else { is_neigh = FALSE; }
  }

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[1]
  ppList = g_list_next(ppList);                                       //frame_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[2]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))){
    quota = 0;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->data))){
    quota = 1;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))){
    quota = 2;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->data))){
    quota = 3;
  }

  pList = g_list_next(pList);                                         //box_fire_h[1]
  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //table_fire[3]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->data))){
    propag = 0;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->data))){
    propag = 1;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->data))){
    propag = 2;
  } else if(gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(ppList->next->next->next->data))){
    propag = 3;
    for(unsigned i=0; i<5; i++) { ppList = g_list_next(ppList); }
    if((gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->data))>0) &
       (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->data))>0))
      {
	temp_propLogis.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
	temp_propLogis.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data))));
	temp_propLogis.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->data))));
      } else { is_prop = FALSE; }
  }

  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //frame_fire[3]
  ppList = g_list_next(ppList);                                       //frame_fire[4]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //scrollbar
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewport
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);

  if((propag==0) && (init<=2)){
    for(unsigned i=0; i<NbFires; i++){
      ppList = g_list_next(ppList);
      if(gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0){
	temp_probFires.push_back(atof(gtk_entry_get_text(GTK_ENTRY(ppList->data))));
      } else { is_prob = FALSE; }
      ppList = g_list_next(ppList);
    }
  }

  if(!is_init | !is_neigh | !is_prop | !is_prob){
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  } else {

    initOption = init;
    if((init==0) | (init==1) | (init==2)){ initNbFires = temp_initNbFires; }
    if(init==2){ prevDataFile = temp_prevData; }
    if(init==4){ initLogis = temp_initLogis; }
    neighOption = neigh;
    if(neigh!=0){ ccExtent = temp_ccExtent; }
    quotaOption = quota;
    propagationOption = propag;
    if(propag==0){ probFires = temp_probFires; }
    if(propag==3){ propLogis = temp_propLogis; }
    submit_fire1 = true;
    GtkWidget* label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);

    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));   //BOX_SIM_v
    pList = g_list_next(pList);                                             //BOX_PFG_V
    pList = g_list_next(pList);                                             //BOX_SUCC_V
    pList = g_list_next(pList);                                             //BOX_MASK_V
    //cleanMask(gtk_container_get_children(GTK_CONTAINER(pList->data))->data,2);
    pList = g_list_next(pList);                                             //BOX_DIST_V
    pList = g_list_next(pList);                                             //BOX_SOIL_V
    pList = g_list_next(pList);                                             //BOX_FIRE_V

    vector<GtkWidget*> new_box_v;
    FireNames.clear();
    FireFreq.clear();
    FirePropKilledProp.clear();
    FirePercentActivSeeds.clear();
    FireBreakAges.clear();
    FireResprAges.clear();
    FirePropKilledInd.clear();
    FirePropResproutInd.clear();
    submit_fire.clear();

    FireNames.resize(NbFires);
    FireFreq.resize(NbFires);
    FireNames.resize(NbFires);
    FirePropKilledProp.resize(NbFires);
    FirePercentActivSeeds.resize(NbFires);
    FireBreakAges.resize(NbFires);
    FireResprAges.resize(NbFires);
    FirePropKilledInd.resize(NbFires);
    FirePropResproutInd.resize(NbFires);
    submit_fire.resize(NbFires);


    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //fire_NoteBook
    ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));
    if(g_list_length(ppList)>1){
      for(unsigned i=1; i<g_list_length(ppList); i++) {
	gtk_notebook_remove_page(GTK_NOTEBOOK(pList->data),1);
      }
    }

    for(unsigned i=0; i<NbFires; i++){
      new_box_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL,0));
      gtk_notebook_append_page(GTK_NOTEBOOK(pList->data), new_box_v[i], (GtkWidget*) gtk_label_new(("Fire"+IntToString(i)).c_str()));

      vector<GtkWidget*> box_dist_h;
      vector<GtkWidget*> box_dist_v;
      vector<GtkWidget*> frame_dist;
      vector<GtkWidget*> table_dist;
      vector<GtkWidget*> label_dist;
      vector<GtkWidget*> entry_dist;
      vector<GtkWidget*> check_dist;
      vector<GtkWidget*> button_dist;
      GtkWidget* scrollbar_dist;

      /* ----------------------------------------------------------------------------------- */
      /* DISTURBANCES PART */

      box_dist_h.push_back(gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));     //Basicparametersbox
      gtk_container_set_border_width(GTK_CONTAINER(box_dist_h[0]), 10);
      box_dist_v.push_back(gtk_box_new(GTK_ORIENTATION_VERTICAL, 0));     //Buttonsbox
      table_dist.push_back(gtk_grid_new());    //Requiredparameterstable
      gtk_container_set_border_width(GTK_CONTAINER(table_dist[0]), 10);

      GtkWidget* label_tmp;
      vector<gchar*> txt_tmp = {(gchar*)"<b>New disturbance parameters</b>",
				(gchar*)"<b>Response stages parameters</b>"};
      for(unsigned i=0; i<2; i++){
	label_tmp = gtk_label_new(NULL);
	frame_dist.push_back(gtk_frame_new(NULL));
	gtk_label_set_markup(GTK_LABEL(label_tmp),txt_tmp[i]);
	gtk_frame_set_label_widget(GTK_FRAME(frame_dist[i]),label_tmp);
	gtk_container_set_border_width(GTK_CONTAINER(frame_dist[i]), 10);
      }
      scrollbar_dist = gtk_scrolled_window_new(NULL, NULL);
      gtk_container_set_border_width(GTK_CONTAINER(scrollbar_dist), 10);
      gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar_dist), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);

      gchar* name_param_dist[] = {(gchar*)"NAME",(gchar*)"Frequency",(gchar*) g_locale_to_utf8("Proportion\n of killed\n propagules",-1,NULL,NULL,NULL),
				  (gchar*) g_locale_to_utf8("Percentage\n of activated\n seeds",-1,NULL,NULL,NULL)};
      for(unsigned j=0; j<4; j++){
	label_dist.push_back(gtk_label_new(name_param_dist[j]));
	gtk_label_set_justify(GTK_LABEL(label_dist[j]), GTK_JUSTIFY_CENTER);
	entry_dist.push_back(gtk_entry_new());
	gtk_entry_set_max_length(GTK_ENTRY(entry_dist[j]),8);
	gtk_entry_set_width_chars(GTK_ENTRY(entry_dist[j]),8);
      }

      button_dist.push_back(gtk_button_new_with_label("Generate response stages"));
      g_signal_connect(G_OBJECT(button_dist[0]), "clicked", G_CALLBACK(generateRespStages), pList->data);
      button_dist.push_back(gtk_button_new_with_label("Clear parameters"));
      g_signal_connect(G_OBJECT(button_dist[1]), "clicked", G_CALLBACK(setDistParamsZero), pList->data);
      button_dist.push_back(gtk_button_new_with_label(NULL));
      label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>";
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(button_dist[2]),label_tmp);
      g_signal_connect(G_OBJECT(button_dist[2]), "clicked", G_CALLBACK(submitFire2Params), pList->data);
      gtk_widget_set_sensitive(button_dist[2],FALSE);

      /* ----------------------------------------------------------------------------------- */
      /* FIRST PANEL : BASIC PARAMETERS */
      gtk_box_pack_start(GTK_BOX(new_box_v[i]), box_dist_h[0], FALSE, FALSE, 0);

      gtk_box_pack_start(GTK_BOX(box_dist_h[0]), frame_dist[0], TRUE, FALSE, 0);
      gtk_container_add(GTK_CONTAINER(frame_dist[0]), table_dist[0]);

      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[0], 0, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[1], 1, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[2], 2, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), label_dist[3], 3, 0, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[0], 0, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[1], 1, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[2], 2, 1, 1, 1);
      gtk_grid_attach(GTK_GRID(table_dist[0]), entry_dist[3], 3, 1, 1, 1);

      gtk_box_pack_start(GTK_BOX(box_dist_h[0]), box_dist_v[0], TRUE, FALSE, 0);
      vector<GtkWidget*> listToAdd = {button_dist[0],button_dist[1],button_dist[2]};
      addToBox(box_dist_v[0],listToAdd,TRUE,FALSE);

      /* ----------------------------------------------------------------------------------- */
      /* SECOND PANEL : RESPONSE STAGES PARAMETERS */
      gtk_box_pack_start(GTK_BOX(new_box_v[i]), frame_dist[1], TRUE, TRUE, 0);

      gtk_container_add(GTK_CONTAINER(frame_dist[1]), scrollbar_dist);
      //gtk_container_add(GTK_CONTAINER(scrollbar_dist), gtk_grid_new());
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar_dist), gtk_grid_new());
    }
    gtk_widget_show_all(GTK_WIDGET(pList->data));
  }
}

/*############################################################################################################################*/

void submitFire2Params(GtkWidget* pButton, gpointer data) //OK
{
  string temp_name;
  unsigned temp_freq, temp_killProp, temp_activSeed;
  unsigned nbDIST = gtk_notebook_get_current_page(GTK_NOTEBOOK((GtkWidget*) data));

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //new_box_v[0]
  for(unsigned i=0; i<nbDIST; i++) { pList = g_list_next(pList); }

  pList = gtk_container_get_children(GTK_CONTAINER(pList->data));                 //box_dist_h[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //frame_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_dist[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  for(unsigned i=0; i<4; i++) { ppList = g_list_next(ppList); }

  if((gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->data))>0))
    {

      temp_name = gtk_entry_get_text(GTK_ENTRY(ppList->data));
      temp_freq = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->data)));
      temp_killProp = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data)));
      temp_activSeed = atoi(gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->data)));

      pList = g_list_next(pList);                                         //frame_dist[1]
      ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));    //scrollbar
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //viewport
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
      GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      pppList = g_list_reverse(pppList);
      for(unsigned i=0; i<(2+NbRespStagesFire+3*Group.size()); i++) { pppList = g_list_next(pppList); }

      unsigned compt = 0;
      for(unsigned i=0; i<((2+2*Group.size())*NbRespStagesFire); i++) {
	if(gtk_entry_get_text_length(GTK_ENTRY(pppList->data))){ compt++; }
	pppList = g_list_next(pppList);
      }

      if(compt==(2*NbRespStagesFire+2*NbRespStagesFire*Group.size())){
	nbDIST = nbDIST-1;

	FireNames[nbDIST] = temp_name;
	FireFreq[nbDIST] = temp_freq;
	FirePropKilledProp[nbDIST] = temp_killProp;
	FirePercentActivSeeds[nbDIST] = temp_activSeed;

	pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
	pppList = g_list_reverse(pppList);
	for(unsigned i=0; i<(2+NbRespStagesFire+3*Group.size()); i++) { pppList = g_list_next(pppList); }

	FirePropKilledInd[nbDIST].clear();
	FirePropResproutInd[nbDIST].clear();

	FirePropKilledInd[nbDIST].resize(Group.size());
	FirePropResproutInd[nbDIST].resize(Group.size());

	for(unsigned i=0; i<NbRespStagesFire; i++) {
	  FireBreakAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  FireResprAges[nbDIST].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	  pppList = g_list_next(pppList);
	  for(unsigned j=0; j<Group.size(); j++) {
	    FirePropKilledInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	    FirePropResproutInd[nbDIST][j].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->data))));
	    pppList = g_list_next(pppList);
	  }
	}

	submit_fire[nbDIST] = TRUE;
	GtkWidget* label_tmp = gtk_label_new(NULL);
	gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
	gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
	gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);

      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
      }
    } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void generateMaskPFG(GtkWidget* pButton, gpointer data) //OK
{
  if(Group.size()>0){

    GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_mask[0]
    pList = g_list_next(pList);							  //frame_mask[1]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		  //pTable ?
    if(g_list_length(pList)>0) { gtk_widget_destroy(GTK_WIDGET(pList->data)); }

    GtkWidget* mask_NoteBook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(mask_NoteBook), true);
    gtk_container_set_border_width(GTK_CONTAINER(mask_NoteBook), 10);

    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_mask[0]
    pList = g_list_next(pList);							  //frame_mask[1]
    gtk_container_add(GTK_CONTAINER(pList->data), mask_NoteBook);

    for(unsigned fg=0; fg<Group.size(); fg++){
      GtkWidget* pBox_h_fg = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
      GtkWidget* scrollbar = gtk_scrolled_window_new(NULL, NULL);
      gtk_container_set_border_width(GTK_CONTAINER(scrollbar), 10);
      gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW(scrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
      GtkWidget* pBox_v_fg = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
      GtkWidget* pTable = gtk_grid_new();
      gtk_container_set_border_width(GTK_CONTAINER(pTable), 10);
      gtk_grid_set_column_spacing(GTK_GRID(pTable), 30);

      gtk_notebook_append_page(GTK_NOTEBOOK(mask_NoteBook), pBox_h_fg, (GtkWidget*) gtk_label_new((Group[fg]+" suitability masks").c_str()));
      gtk_box_pack_start(GTK_BOX(pBox_h_fg), pTable, FALSE, FALSE, 0);
      gtk_box_pack_start(GTK_BOX(pBox_h_fg), scrollbar, TRUE, TRUE, 0);
      //gtk_container_add(GTK_CONTAINER(scrollbar), pBox_v_fg);
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar), pBox_v_fg);

      GtkWidget* pEntry = gtk_entry_new();
      GtkWidget* pButton = gtk_button_new_with_label("Select INIT map");
      g_signal_connect(G_OBJECT(pButton), "clicked", G_CALLBACK(selectMaskFile), pEntry);
      GtkToolItem* ButtAdd = gtk_tool_button_new(gtk_image_new_from_icon_name("list-add",GTK_ICON_SIZE_MENU),"ADD");
      gtk_tool_item_set_homogeneous(ButtAdd,TRUE);
      gtk_tool_item_set_expand(ButtAdd,TRUE);
      g_signal_connect(G_OBJECT(ButtAdd), "clicked", G_CALLBACK(generateNewMask), pBox_v_fg);
      GtkToolItem* ButtDel = gtk_tool_button_new(gtk_image_new_from_icon_name("list-remove",GTK_ICON_SIZE_MENU),"REMOVE");
      gtk_tool_item_set_homogeneous(ButtDel,TRUE);
      gtk_tool_item_set_expand(ButtDel,TRUE);
      g_signal_connect(G_OBJECT(ButtDel), "clicked", G_CALLBACK(removeNewMask), pBox_v_fg);

      gtk_grid_attach(GTK_GRID(pTable), pButton, 0, 0, 2, 1);
      gtk_grid_attach(GTK_GRID(pTable), pEntry, 0, 1, 2, 1);
      gtk_grid_attach(GTK_GRID(pTable), GTK_WIDGET(ButtAdd), 0, 2, 1, 1);
      gtk_grid_attach(GTK_GRID(pTable), GTK_WIDGET(ButtDel), 1, 2, 1, 1);
    }

    gtk_widget_show_all(mask_NoteBook);
    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_mask[0]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	   //box_mask_h[0]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	   //table_mask[0]
    pList = g_list_next(pList);						   //box_mask_v[0]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	   //button_generate
    gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data),TRUE);
    GtkWidget* label_tmp = gtk_label_new(NULL);
    gchar* txt_col = (gchar*)"<span foreground=\"#A00000\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
    gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
    gtk_button_set_image(GTK_BUTTON(pList->next->data),label_tmp);

  } else {
    gchar* message = g_locale_to_utf8("You must add \nat least one PFG !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void generateMaskDist(GtkWidget* pButton, gpointer data) //OK
{
  if(NbDist>0){

    GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_mask_h[1]
    pList = g_list_next(pList);							  //frame_mask[2]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		  //pTable ?
    if(g_list_length(pList)>0) { gtk_widget_destroy(GTK_WIDGET(pList->data)); }

    GtkWidget* mask_NoteBook = gtk_notebook_new();
    gtk_notebook_set_scrollable(GTK_NOTEBOOK(mask_NoteBook), true);
    gtk_container_set_border_width(GTK_CONTAINER(mask_NoteBook), 10);

    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_mask_h[1]
    pList = g_list_next(pList);						   //frame_mask[2]
    gtk_container_add(GTK_CONTAINER(pList->data), mask_NoteBook);

    for(unsigned fg=0; fg<NbDist; fg++){
      GtkWidget* pBox_h_fg = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
      GtkWidget* scrollbar = gtk_scrolled_window_new(NULL, NULL);
      gtk_container_set_border_width(GTK_CONTAINER(scrollbar), 10);
      gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
      GtkWidget* pBox_v_fg = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
      GtkWidget* pTable = gtk_grid_new();
      gtk_container_set_border_width(GTK_CONTAINER(pTable), 10);
      gtk_grid_set_column_spacing(GTK_GRID(pTable), 30);

      gtk_notebook_append_page(GTK_NOTEBOOK(mask_NoteBook), pBox_h_fg, (GtkWidget*) gtk_label_new(("Dist"+IntToString(fg)+" occurence masks").c_str()));
      gtk_box_pack_start(GTK_BOX(pBox_h_fg), pTable, FALSE, FALSE, 0);
      gtk_box_pack_start(GTK_BOX(pBox_h_fg), scrollbar, TRUE, TRUE, 0);
      //gtk_container_add(GTK_CONTAINER(scrollbar), pBox_v_fg);
	  gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar), pBox_v_fg);

      GtkWidget* pEntry = gtk_entry_new();
      GtkWidget* pButton = gtk_button_new_with_label("Select INIT map");
      g_signal_connect(G_OBJECT(pButton), "clicked", G_CALLBACK(selectMaskFile), pEntry);
      GtkToolItem* ButtAdd = gtk_tool_button_new(gtk_image_new_from_icon_name("list-add",GTK_ICON_SIZE_MENU),"ADD");
      gtk_tool_item_set_homogeneous(ButtAdd,TRUE);
      gtk_tool_item_set_expand(ButtAdd,TRUE);
      g_signal_connect(G_OBJECT(ButtAdd), "clicked", G_CALLBACK(generateNewMask), pBox_v_fg);
      GtkToolItem* ButtDel = gtk_tool_button_new(gtk_image_new_from_icon_name("list-remove",GTK_ICON_SIZE_MENU),"REMOVE");
      gtk_tool_item_set_homogeneous(ButtDel,TRUE);
      gtk_tool_item_set_expand(ButtDel,TRUE);
      g_signal_connect(G_OBJECT(ButtDel), "clicked", G_CALLBACK(removeNewMask), pBox_v_fg);

      gtk_grid_attach(GTK_GRID(pTable), pButton, 0, 0, 2, 1);
      gtk_grid_attach(GTK_GRID(pTable), pEntry, 0, 1, 2, 1);
      gtk_grid_attach(GTK_GRID(pTable), GTK_WIDGET(ButtAdd), 0, 2, 1, 1);
      gtk_grid_attach(GTK_GRID(pTable), GTK_WIDGET(ButtDel), 1, 2, 1, 1);
    }

    gtk_widget_show_all(mask_NoteBook);
    pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_mask_h[1]
    pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	   //button_generate
    gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data),TRUE);

  } else {
    gchar* message = g_locale_to_utf8("You must add \nat least one disturbance !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}


/*############################################################################################################################*/

void generateMaskFire(GtkWidget* pButton, gpointer data) //OK
{
  if(NbFires>0){
    if(submit_fire1){

      GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_mask_h[2]
      pList = g_list_next(pList);						//frame_mask[3]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));		  //pTable ?
      if(g_list_length(pList)>0) { gtk_widget_destroy(GTK_WIDGET(pList->data)); }

      GtkWidget* mask_NoteBook = gtk_notebook_new();
      gtk_notebook_set_scrollable(GTK_NOTEBOOK(mask_NoteBook), true);
      gtk_container_set_border_width(GTK_CONTAINER(mask_NoteBook), 10);

      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_mask_h[2]
      pList = g_list_next(pList);						   //frame_mask[3]
      gtk_container_add(GTK_CONTAINER(pList->data), mask_NoteBook);

      for(unsigned fg=0; fg<NbFires; fg++){
	GtkWidget* pBox_h_fg = gtk_box_new(GTK_ORIENTATION_HORIZONTAL,0);
	GtkWidget* scrollbar = gtk_scrolled_window_new(NULL, NULL);
	gtk_container_set_border_width(GTK_CONTAINER(scrollbar), 10);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollbar), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	GtkWidget* pBox_v_fg = gtk_box_new(GTK_ORIENTATION_VERTICAL,0);
	GtkWidget* pTable = gtk_grid_new();
	gtk_container_set_border_width(GTK_CONTAINER(pTable), 10);
	gtk_grid_set_column_spacing(GTK_GRID(pTable), 30);

	gtk_notebook_append_page(GTK_NOTEBOOK(mask_NoteBook), pBox_h_fg, (GtkWidget*) gtk_label_new(("Fire"+IntToString(fg)+" masks").c_str()));
	gtk_box_pack_start(GTK_BOX(pBox_h_fg), pTable, FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(pBox_h_fg), scrollbar, TRUE, TRUE, 0);
	//gtk_container_add(GTK_CONTAINER(scrollbar), pBox_v_fg);
	gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(scrollbar), pBox_v_fg);

	vector<GtkWidget*> pEntry, pButton;
	for(unsigned i=0; i<4; i++){ pEntry.push_back(gtk_entry_new()); }
	pButton.push_back(gtk_button_new_with_label("Select INIT Fire map"));
	g_signal_connect(G_OBJECT(pButton[0]), "clicked", G_CALLBACK(selectMaskFile), pEntry[0]);
	if(initOption!=3){
	  gtk_widget_set_sensitive(pButton[0],FALSE);
	  gtk_widget_set_sensitive(pEntry[0],FALSE);
	}
	pButton.push_back(gtk_button_new_with_label("Select INIT Drought map"));
	g_signal_connect(G_OBJECT(pButton[1]), "clicked", G_CALLBACK(selectMaskFile), pEntry[1]);
	pButton.push_back(gtk_button_new_with_label("Select INIT Slope map"));
	g_signal_connect(G_OBJECT(pButton[2]), "clicked", G_CALLBACK(selectMaskFile), pEntry[2]);
	pButton.push_back(gtk_button_new_with_label("Select INIT Elevation map"));
	g_signal_connect(G_OBJECT(pButton[3]), "clicked", G_CALLBACK(selectMaskFile), pEntry[3]);
	if(initOption!=4){
	  for(unsigned i=1; i<4; i++){
	    gtk_widget_set_sensitive(pButton[i],FALSE);
	    gtk_widget_set_sensitive(pEntry[i],FALSE);
	  }
	}

	GtkToolItem* ButtAdd = gtk_tool_button_new(gtk_image_new_from_icon_name("list-add",GTK_ICON_SIZE_MENU),"ADD");
	gtk_tool_item_set_homogeneous(ButtAdd,TRUE);
	gtk_tool_item_set_expand(ButtAdd,TRUE);
	g_signal_connect(G_OBJECT(ButtAdd), "clicked", G_CALLBACK(generateNewMaskFire), pBox_v_fg);
	GtkToolItem* ButtDel = gtk_tool_button_new(gtk_image_new_from_icon_name("list-remove",GTK_ICON_SIZE_MENU),"REMOVE");
	gtk_tool_item_set_homogeneous(ButtDel,TRUE);
	gtk_tool_item_set_expand(ButtDel,TRUE);
	g_signal_connect(G_OBJECT(ButtDel), "clicked", G_CALLBACK(removeNewMaskFire), pBox_v_fg);

	gtk_grid_attach(GTK_GRID(pTable), pButton[0], 0, 0, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pEntry[0], 0, 1, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pButton[1], 0, 2, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pEntry[1], 0, 3, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pButton[2], 0, 4, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pEntry[2], 0, 5, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pButton[3], 0, 6, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), pEntry[3], 0, 7, 2, 1);
	gtk_grid_attach(GTK_GRID(pTable), GTK_WIDGET(ButtAdd), 0, 8, 1, 1);
	gtk_grid_attach(GTK_GRID(pTable), GTK_WIDGET(ButtDel), 1, 8, 1, 1);
      }

      gtk_widget_show_all(mask_NoteBook);
      pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //box_mask_h[2]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));	   //button_generate
      gtk_widget_set_sensitive(GTK_WIDGET(pList->next->data),TRUE);

    } else {
      gchar* message = g_locale_to_utf8("You must give the basic\nparameters for fire disturbances !",-1,NULL,NULL,NULL);
      errorMessage(message);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must add \nat least one fire disturbance !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

void generateNewMaskFire(GtkWidget* pButton, gpointer data) //OK
{
  GtkWidget* pTable = gtk_grid_new();
  gtk_container_set_border_width(GTK_CONTAINER(pTable), 10);

  GtkWidget* pEntry1 = gtk_entry_new();
  GtkWidget* Butt1 = gtk_button_new_with_label("Select NEW Fire map");
  g_signal_connect(G_OBJECT(Butt1), "clicked", G_CALLBACK(selectMaskFile), pEntry1);
  if(initOption!=3){
    gtk_widget_set_sensitive(pEntry1,FALSE);
    gtk_widget_set_sensitive(Butt1,FALSE);
  }
  GtkWidget* pEntry2 = gtk_entry_new();
  GtkWidget* Butt2 = gtk_button_new_with_label("Select NEW Drought map");
  g_signal_connect(G_OBJECT(Butt2), "clicked", G_CALLBACK(selectMaskFile), pEntry2);
  if(initOption!=4){
    gtk_widget_set_sensitive(pEntry2,FALSE);
    gtk_widget_set_sensitive(Butt2,FALSE);
  }

  gtk_grid_attach(GTK_GRID(pTable), gtk_label_new("New date"), 0, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), gtk_entry_new(), 0, 1, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), Butt1, 1, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), pEntry1, 1, 1, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), Butt2, 2, 0, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), pEntry2, 2, 1, 1, 1);
  gtk_grid_attach(GTK_GRID(pTable), gtk_check_button_new(), 3, 0, 1, 2);

  gtk_box_pack_start(GTK_BOX((GtkWidget*) data), pTable, FALSE, FALSE, 0);
  gtk_widget_show_all(pTable);
}

/*############################################################################################################################*/

void submitMask1Params(GtkWidget* pButton, gpointer data) //OK
{
  string temp_mask, temp_array, temp_object;
  vector< vector<unsigned> > temp_newDate;
  vector<string> temp_initMasks;
  vector< vector<string> > temp_newMasks;

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //frame_mask[0]
  GList* ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));         //box_mask_h[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));               //table_mask[0]
  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
  ppList = g_list_reverse(ppList);
  ppList = g_list_next(ppList);

  if((gtk_entry_get_text_length(GTK_ENTRY(ppList->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->data))>0) &
     (gtk_entry_get_text_length(GTK_ENTRY(ppList->next->next->next->next->data))>0))
    {
      temp_mask = gtk_entry_get_text(GTK_ENTRY(ppList->data));
      temp_array = gtk_entry_get_text(GTK_ENTRY(ppList->next->next->data));
      temp_object = gtk_entry_get_text(GTK_ENTRY(ppList->next->next->next->next->data));

      pList = g_list_next(pList);                                         //frame_mask[1]
      pList = gtk_container_get_children(GTK_CONTAINER(pList->data));     //notebook?

      if(g_list_length(pList)>0){
	temp_newDate.resize(Group.size());
	temp_newMasks.resize(Group.size());
	for(unsigned fg=0; fg<Group.size(); fg++){
	  ppList = gtk_container_get_children(GTK_CONTAINER(pList->data));  //box_h[i]
	  for(unsigned i=0; i<fg; i++){ ppList = g_list_next(ppList); }
	  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
	  GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
	  pppList = g_list_reverse(pppList);

	  if(gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0){
	    temp_initMasks.push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->data)));

	    ppList = g_list_next(ppList);						//scrollbar
	    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//viewport
	    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//pBox_v_fg
	    ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//pTable
	    unsigned temp = g_list_length(ppList);

	    if(temp>0){
	      for(unsigned i=0; i<temp; i++){
		pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
		pppList = g_list_reverse(pppList);
		if((gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0) &&
		   (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->data))>0)){
		  temp_newDate[fg].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->next->data))));
		  temp_newMasks[fg].push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->data)));
		  ppList = g_list_next(ppList);
		} else {
		  gchar* message = g_locale_to_utf8("You must fill all \nthe new required parameters !",-1,NULL,NULL,NULL);
		  errorMessage(message);
		  return;
		}
	      }
	    }

	  } else {
	    gchar* message = g_locale_to_utf8("You must fill all \nthe input required parameters !",-1,NULL,NULL,NULL);
	    errorMessage(message);
	    return;
	  }
	} //end of loop on FG
	MaskFile = temp_mask;
	ArraySavingFile = temp_array;
	ObjectSavingFile = temp_object;

	EnvsuitFile.clear();
	newEnvsuitFiles.clear();
	ChangingYearsPFG.clear();

	EnvsuitFile = temp_initMasks;
	newEnvsuitFiles = temp_newMasks;
	ChangingYearsPFG = temp_newDate;

	submit_maskPFG = TRUE;
	GtkWidget* label_tmp = gtk_label_new(NULL);
	gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
	gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
	gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);
      } else {
	gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
      }
    } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void submitMask2Params(GtkWidget* pButton, gpointer data) //OK
{
  vector< vector<unsigned> > temp_newDate;
  vector<string> temp_initMasks;
  vector< vector<string> > temp_newMasks;

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //notebook?

  if(g_list_length(pList)>0){
    temp_newDate.resize(NbDist);
    temp_newMasks.resize(NbDist);
    bool allOk = true;
    for(unsigned fg=0; fg<NbDist; fg++){

      GList* ppList = pList; //notebook
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data)); //pBox_h_fg
      for(unsigned i=0; i<fg; i++){ ppList = g_list_next(ppList); }
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
      GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      pppList = g_list_reverse(pppList);

      if(gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0){
	temp_initMasks.push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->data)));

	ppList = g_list_next(ppList);						//scrollbar
	ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//viewport
	ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//pBox_v_fg
	ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//pTable

	unsigned temp = g_list_length(ppList);
	if(temp>0){
	  for(unsigned i=0; i<temp; i++){
	    pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
	    pppList = g_list_reverse(pppList);
	    if((gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0) &&
	       (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->data))>0)){
	      temp_newDate[fg].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->next->data))));
	      temp_newMasks[fg].push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->data)));
	      ppList = g_list_next(ppList);
	    } else {
              allOk = false;
	      gchar* message = g_locale_to_utf8("You must fill all \nthe new required parameters !",-1,NULL,NULL,NULL);
	      errorMessage(message);
	      return;
	    }
	  }
	}
      } else {
        allOk = false;
	gchar* message = g_locale_to_utf8("You must fill all \nthe input required parameters !",-1,NULL,NULL,NULL);
	errorMessage(message);
	return;
      }
    }

    if(allOk){
      DistFile.clear();
      newDistFiles.clear();
      ChangingYearsDist.clear();

      DistFile = temp_initMasks;
      newDistFiles = temp_newMasks;
      ChangingYearsDist = temp_newDate;

      submit_maskDist = true;
      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}

/*############################################################################################################################*/

void submitMask3Params(GtkWidget* pButton, gpointer data) //PASOK
{
  vector< vector<unsigned> > temp_newDate;
  vector<string> temp_initMasks, temp_initDrought, temp_initSlope, temp_initElev;
  vector< vector<string> > temp_newMasks, temp_newDrought;

  GList* pList = gtk_container_get_children(GTK_CONTAINER((GtkWidget*) data));    //notebook?
  if(g_list_length(pList)>0){
    temp_newDate.resize(NbFires);
    temp_newMasks.resize(NbFires);
    bool allOk = true;
    for(unsigned fire=0; fire<NbFires; fire++){

      GList* ppList = pList;	//notebook
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data)); //pBox_h_fg
      for(unsigned i=0; i<fire; i++){ ppList = g_list_next(ppList); }
      ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));   //pTable
      GList* pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
      pppList = g_list_reverse(pppList);

      cout << gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->data)) << endl;
      cout << gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->next->next->data)) << endl;
      cout << gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->next->next->next->next->data)) << endl;
      cout << gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->next->next->next->next->next->next->data)) << endl;

      if( (gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->data))) || //activated
	  ((gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->next->next->data))) &&
	   (gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->next->next->next->next->data))) &&
	   (gtk_widget_get_sensitive(GTK_WIDGET(pppList->next->next->next->next->next->next->next->data))))
	  ){
	cout << "youhouu" << endl;
	if( (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0) || //filled
	    ((gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->data))>0) &&
	     (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->next->next->data))>0) &&
	     (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->next->next->next->next->data))>0))
	    ){
	  temp_initMasks.push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->data)));
	  temp_initDrought.push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->data)));
	  temp_initSlope.push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->next->next->data)));
	  temp_initElev.push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->next->next->next->next->data)));

	  ppList = g_list_next(ppList);						//scrollbar
	  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//viewport
	  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//pBox_v_fg
	  ppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));		//pTable

	  unsigned temp = g_list_length(ppList);
	  if(temp>0){
	    for(unsigned i=0; i<temp; i++){
	      pppList = gtk_container_get_children(GTK_CONTAINER(ppList->data));
	      pppList = g_list_reverse(pppList);
	      if((gtk_entry_get_text_length(GTK_ENTRY(pppList->next->data))>0) &&
		 (gtk_entry_get_text_length(GTK_ENTRY(pppList->next->next->next->data))>0)){
		temp_newDate[fire].push_back(atoi(gtk_entry_get_text(GTK_ENTRY(pppList->next->data))));
		temp_newMasks[fire].push_back(gtk_entry_get_text(GTK_ENTRY(pppList->next->next->next->data)));
		if((i+1)<temp) { ppList = g_list_next(ppList); }
	      } else {
		allOk = false;
		gchar* message = g_locale_to_utf8("You must fill all \nthe new required parameters !",-1,NULL,NULL,NULL);
		errorMessage(message);
		return;
	      }
	    }
	  }
	} else {
	  allOk = false;
	  gchar* message = g_locale_to_utf8("You must fill all \nthe input required parameters !",-1,NULL,NULL,NULL);
	  errorMessage(message);
	  return;
	}
      }
    }
    if(allOk){
      FireFile.clear();
      newFireFiles.clear();
      ChangingYearsFire.clear();

      FireFile = temp_initMasks;
      newFireFiles = temp_newMasks;
      ChangingYearsFire = temp_newDate;
      if(temp_initDrought.size()>0){ DroughtMap = temp_initDrought[0]; }
      if(temp_initSlope.size()>0){ SlopeMap = temp_initSlope[0]; }
      if(temp_initElev.size()>0){ ElevationMap = temp_initElev[0]; }

      submit_maskFire = TRUE;
      GtkWidget* label_tmp = gtk_label_new(NULL);
      gchar* txt_col = (gchar*)"<span foreground=\"#86B404\"><b>Submit parameters</b></span>"; //background=\"#A00000\"
      gtk_label_set_markup(GTK_LABEL(label_tmp),txt_col);
      gtk_button_set_image(GTK_BUTTON(pButton),label_tmp);
    }
  } else {
    gchar* message = g_locale_to_utf8("You must fill all \nthe required parameters !",-1,NULL,NULL,NULL);
    errorMessage(message);
  }
}


